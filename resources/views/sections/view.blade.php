@extends('layouts.app')
@section('title','View Sections')

	@section('content')
	
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Sets</h5>
                <p class="card-category">View Sets</p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">

                    <form>

                      <div class="col-lg-6">
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Code</label>
                          <input type="text" name="code" class="form-control" maxlength="10" readonly value="{{$section->code}}">
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Name</label>
                          <input type="text" name="name" class="form-control" readonly value="{{$section->name}}">
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10 mg-top">
                                
                                <a href="{{ url('sections') }}" class="btn btn-danger">Back</a>
                            </div>
                        </div>
                      </div>
                      
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Set Maintenance
                </div>
              </div>
            </div>
          </div>
      </div>
    
	@endsection

