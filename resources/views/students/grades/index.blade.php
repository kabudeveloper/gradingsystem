@extends('layouts.student')
@section('title','Grade Breakdown')

	@section('content')
	<div class="content">
        <div class="row">
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Quick Information</h5>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">
                    <form>
                      <input type="hidden" id="semester_grade" value="{{$grade->id}}">
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                              <label class="col-sm-4 control-label">Subject</label>
                              <input type="text" class="form-control" value="{{ $grade->subject->name }}" readonly>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-4 control-label">Year</label>
                              <input type="text" class="form-control" value="{{ $grade->year->code }}" readonly>
                          </div>
                          
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group">
                              <label class="col-sm-4 control-label">Semester</label>
                              <input type="text" class="form-control" value="{{ $grade->_semester }}" readonly>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-4 control-label">Grade</label>
                              <input type="text" class="form-control" value="{{ $finalgrade->semester_grade }}" readonly>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Information
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div id="accordion">
          <div class="card">
            <div class="card-header" id="headingOne">
              <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Attendance
                </button>
              </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
              <div class="card-body">
                <div class="card ">
                     <div class="card-header ">
                        <h4 class="card-title">Attendance</h4>
                      <div class="card-body ">
                          <div class="table-responsive">
                            <div class="col-lg-12">
                              <table class="table table-bordered table-striped" width="100%" id="attendance-details-table">
                                <thead class="text-primary">
                                  <tr>
                                    <th>Term</th>
                                    <th>Title</th>
                                    <th>Perfect Score</th>
                                    <th>Score</th>

                                  </tr>
                                </thead>
                              </table>
                            </div>
                          </div>
                        </div>
                        <div class="card-footer ">
                          <hr>
                          <div class="stats">
                            <i class="fa fa-history"></i> Student Grade Details
                          </div>
                        </div>
                      </div>
                    </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingTwo">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  Term Exam
                </button>
              </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
              <div class="card-body">
               @if ($courseDetails->term_exam > 0 || $courseDetails->term_exam != NULL) 
                    <div class="card ">
                     <div class="card-header ">
                        <h4 class="card-title">Term Exam</h4>
                      <div class="card-body ">
                          <div class="table-responsive">
                            <div class="col-lg-12">
                              <table class="table table-bordered table-striped" width="100%" id="term-exam-details-table">
                                <thead class="text-primary">
                                  <tr>
                                    <th>Term</th>
                                    <th>Title</th>
                                    <th>Perfect Score</th>
                                    <th>Score</th>
                                  </tr>
                                </thead>
                              </table>
                            </div>
                          </div>
                        </div>
                        <div class="card-footer ">
                          <hr>
                          <div class="stats">
                            <i class="fa fa-history"></i> Student Grade Details
                          </div>
                        </div>
                      </div>
                    </div>
                  @endif
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingThree">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  Quiz
                </button>
              </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
              <div class="card-body">
               @if ($courseDetails->quiz > 0 || $courseDetails->quiz != NULL) 
                    <div class="card ">
                     <div class="card-header ">
                        <h4 class="card-title">Quiz</h4>
                      <div class="card-body ">
                          <div class="table-responsive">
                            <div class="col-lg-12">
                              <table class="table table-bordered table-striped" width="100%" id="quiz-details-table">
                                <thead class="text-primary">
                                  <tr>
                                    <th>Term</th>
                                    <th>Title</th>
                                    <th>Perfect Score</th>
                                    <th>Score</th>
                                  </tr>
                                </thead>
                              </table>
                            </div>
                          </div>
                        </div>
                        <div class="card-footer ">
                          <hr>
                          <div class="stats">
                            <i class="fa fa-history"></i> Student Grade Details
                          </div>
                        </div>
                      </div>
                    </div>
                  @endif
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="heading4">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                  Assignment
                </button>
              </h5>
            </div>
            <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
              <div class="card-body">
                @if ($courseDetails->assignment > 0 || $courseDetails->assignment != NULL) 
                    <div class="card ">
                     <div class="card-header ">
                        <h4 class="card-title">Assignment</h4>
                      <div class="card-body ">
                          <div class="table-responsive">
                            <div class="col-lg-12">
                              <table class="table table-bordered table-striped" width="100%" id="assignment-details-table">
                                <thead class="text-primary">
                                  <tr>
                                    <th>Term</th>
                                    <th>Title</th>
                                    <th>Perfect Score</th>
                                    <th>Score</th>
                                  </tr>
                                </thead>
                              </table>
                            </div>
                          </div>
                        </div>
                        <div class="card-footer ">
                          <hr>
                          <div class="stats">
                            <i class="fa fa-history"></i> Student Grade Details
                          </div>
                        </div>
                      </div>
                    </div>
                  @endif

              </div>
            </div>
          </div>
        </div>
        
      </div>
	@endsection

@section('custom_css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}">
@endsection()

@section('custom_js')
  <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{asset('js/students/grade_breakdown.js')}}"></script>

@endsection()
