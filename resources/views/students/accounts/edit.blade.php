@extends('layouts.app')
@section('title','Edit Student Account')

	@section('content')
	
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Student Account</h5>
                <p class="card-category">Edit Student Account</p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">

                    <form method="POST" action="{{ URL('students/'.$account->student_id.'/account/'.$account->id.'/update') }}">
                      @csrf
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Username</label>
                            <input type="text" name="username" class="form-control" value="{{ $account->username }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Email</label>
                            <input type="email" name="email" class="form-control" value="{{ $account->email }}" required>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Password</label>
                            <input type="password" name="password" class="form-control" required>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Position</label>
                                  @if($account->position_id == 0)
                                   <input value="Parent" class="form-control" readonly="">
                                  @else($account->position_id == 1)
                                    <input value="Student" class="form-control" readonly="">
                                  @endif
                          </div>
                          <div class="form-group">
                              <div class="col-sm-offset-4 col-sm-10 mg-top">
                                  <button id="summary-form-button" type="submit" class="btn btn-primary btn-round">Update</button>
                                  <a href="{{ url('students/'.$account->student_id.'/edit') }}" class="btn btn-danger btn-round">Back</a>
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          @if($account->position_id == 0)
                          <div class="form-group" id="p_fname">
                            <label class="col-sm-4 control-label">First Name</label>
                            <input type="text" name="first_name" class="form-control" value="{{ $account->first_name }}">
                          </div>
                          <div class="form-group" id="p_mname">
                            <label class="col-sm-4 control-label">Middle Name</label>
                            <input type="text" name="middle_name" class="form-control" value="{{ $account->middle_name }}">
                          </div>
                           <div class="form-group" id="p_lname">
                            <label class="col-sm-4 control-label">Last Name</label>
                            <input type="text" name="last_name" class="form-control" value="{{ $account->last_name }}">
                          </div>
                          @else
                          @endif
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Student Account Maintenance
                </div>
              </div>
            </div>
          </div>
      </div>
    
	@endsection

@section('custom_css')

@section('custom_js')

