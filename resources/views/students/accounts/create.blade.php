@extends('layouts.app')
@section('title','Add Student Account')

	@section('content')
	
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Student Account</h5>
                <p class="card-category">Add Student Account</p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">

                    <form method="POST" action="{{ URL('students/'.$studentid.'/account/store') }}">
                      @csrf
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Username</label>
                            <input type="text" name="username" class="form-control" required>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Email</label>
                            <input type="email" name="email" class="form-control" required>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Password</label>
                            <input type="password" name="password" class="form-control" required>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Position</label>
                             <select name="position_id" class="form-control">
                                
                                  <option></option>
                                  <option value="0">Parent</option>
                                  <option value="{{ $position->id }}">{{ $position->name }}</option>
                              </select>
                          </div>
                          
                          <div class="form-group">
                              <div class="col-sm-offset-4 col-sm-10 mg-top">
                                  <button id="summary-form-button" type="submit" class="btn btn-primary btn-round">Save</button>
                                  <a href="{{ url('students/'.$studentid.'/edit') }}" class="btn btn-danger btn-round">Back</a>
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group" id="p_fname">
                            
                          </div>
                          <div class="form-group" id="p_mname">
                            
                          </div>
                           <div class="form-group" id="p_lname">
                            
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Student Account Maintenance
                </div>
              </div>
            </div>
          </div>
      </div>
    
	@endsection

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}">
  @endsection()
  @section('custom_js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{asset('js/student_details/student_details.js')}}"></script>
  @endsection()
