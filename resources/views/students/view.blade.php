@extends('layouts.app')
@section('title','View Students')

	@section('content')
	
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Students</h5>
                <p class="card-category">View Students</p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">

                    <form>

                      <div class="row">
                         <div class="col-lg-12">
                           <div class="form-group">
                            <label class="col-sm-4 control-label">ID Number</label>
                            <input type="number" name="id_number" class="form-control"value="{{ $student->id_number }}" readonly>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">First Name</label>
                            <input type="text" readonly name="first_name" class="form-control" value="{{ $student->first_name }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Middle Name</label>
                            <input type="text" readonly name="middle_name" class="form-control" value="{{ $student->middle_name }}">
                          </div>
                          <div class="form-group">
                              <div class="col-sm-offset-4 col-sm-10 mg-top">
                                  <a href="{{ url('students') }}" class="btn btn-danger">Back</a>
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Last Name</label>
                            <input type="text" readonly name="last_name" class="form-control" value="{{ $student->last_name }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Suffix</label>
                            <input type="text" readonly name="suffix" class="form-control" value="{{ $student->suffix }}">
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Student Maintenance
                </div>
              </div>
            </div>
          </div>
      </div>
    
	@endsection

