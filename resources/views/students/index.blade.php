@extends('layouts.app')
@section('title','Student')

  @section('content')
  
    <div class="row">
      <div class="col-md-12">
        <div class="card ">
          <div class="card-header ">
            <h4 class="card-title">Student Lists</h4>
            <button type="button" class="btn btn-round btn-success btn-sm" style="float: right;" data-toggle="modal" data-target="#uploadModal">Upload Student Files</button>
            <div class="card-header">
               <a href="students/create" class="btn btn-primary btn-round">Add Students</a>
               
            </div>
          </div>
          <div class="card-body ">
           
            <div class="table-responsive">

              <div class="col-lg-12">
                <table class="table table-striped" width="100%" id="students-table">
                  <thead class="text-primary">
                    <tr>
                      <th>Last Name</th>
                      <th>First Name</th>
                      <th>Middle Name</th>
                      <th>Suffix</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
          <div class="card-footer ">
            <hr>
            <div class="stats">
              <i class="fa fa-history"></i> Student Maintenance
            </div>
          </div>
        </div>
      </div>
      @include('alerts.confirm')
  </div>
    
        <!-- Modal -->

    <div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadmodaltitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="">Upload Files</h5>
                      <button type="button" class="close" data-dismiss="modal" id="close-button"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form action="#" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                        <div class="form-group">
                                            <label lass="col-sm-4 control-label">Upload Sales File</label>
                                            <input type="file" name="sds" class="form-control" required style="opacity: 1; position: relative;">
                                        </div>
                                        <div class="col-sm-2">
                                                <button type="button" id="uploadBtn" class="btn btn-primary btn-sm btn-round"> SAVE</button>
                                                
                                        </div>
                                    
                                </div>       
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <div class="loader">
        <div class="loader-img">
            <div>
                <img src="{{URL::asset('img/loading.gif')}}">
                <h5 class="loader-text">Uploading File please wait...</h5>
            </div>
            
        </div>
    </div>

  @endsection

@section('custom_css')
  
  <link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}">

    <style type="text/css">
        .loader{
            position: fixed;
            display: none;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(255,255,255.0.46);
            cursor: no-drop;

        }
        .loader-img{
            position: absolute;
            top: 50%;
            left:55%;
            height: 120px;
            color: white;
            transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);
            cursor: no-drop;
        }
        .loader-text{
            color: black;
            text-align: center;
        }
    </style>
  
@endsection()
@section('custom_js')
  <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{asset('js/students/students.js')}}"></script>
@endsection()
