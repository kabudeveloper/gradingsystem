
@extends('layouts.app')
@section('title','Edit Students')

	@section('content')
	
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Students</h5>
                <p class="card-category">Edit Students</p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">

                    <form method="POST" action="{{ URL('students/'.$id) }}">
                      @csrf
                      @method('PATCH')
                      
                      <div class="row">
                        <div class="col-lg-12">
                           <div class="form-group">
                            <label class="col-sm-4 control-label">ID Number</label>
                            <input type="number" name="id_number" class="form-control"value="{{ $student->id_number }}" readonly>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">First Name</label>
                            <input type="text" name="first_name" class="form-control" value="{{ $student->first_name }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Middle Name</label>
                            <input type="text" name="middle_name" class="form-control" value="{{ $student->middle_name }}">
                          </div>
                          
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Last Name</label>
                            <input type="text" name="last_name" class="form-control" value="{{ $student->last_name }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Suffix</label>
                            <input type="text" name="suffix" class="form-control" value="{{ $student->suffix }}">
                          </div>
                        </div>
                      </div>
                    
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Student Maintenance
                </div>
              </div>
            </div>

            <br> <hr>

            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Course</label>
                  <select class="form-control" name="course_id"> 
                    @foreach($course as $c)
                      @if($c->major == "")
                        @if($c->id == $studentd->course_id)
                          <option value="{{ $c->id }}" selected>{{ $c->code }}</option>
                        @else
                          <option value="{{ $c->id }}">{{ $c->code }}</option>
                        @endif
                      @else
                        @if($c->id == $studentd->course_id)
                          <option value="{{ $c->id }}" selected>{{ $c->code }} - {{ $c->major }}</option>
                        @else
                          <option value="{{ $c->id }}">{{ $c->code }} - {{ $c->major }}</option>
                        @endif
                      @endif
                      
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Year</label>
                   <select class="form-control" name="year_id"> 
                    @foreach($year as $y)
                      @if($y->id == $studentd->year_id)
                        <option value="{{ $y->id }}" selected>{{ $y->code }} Year</option>
                      @else
                        <option value="{{ $y->id }}">{{ $y->code }} Year</option>
                      @endif
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">School year</label>
                   <select class="form-control" name="school_year_id"> 
                    @foreach($sc as $s)
                      @if($s->id == $studentd->school_year_id)
                        <option value="{{ $s->id }}" selected>{{ $s->school_year }}</option>
                      @else
                        <option value="{{ $s->id }}">{{ $s->school_year }}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Set</label>
                   <select class="form-control" name="section_id"> 
                    @foreach($section as $s)
                      @if($s->id == $studentd->section_id)
                        <option value="{{ $s->id }}" selected>{{ $s->code }}</option>
                      @else
                        <option value="{{ $s->id }}">{{ $s->code }}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Gender</label>
                   <select class="form-control" name="gender_id"> 
                    @foreach($gender as $g)
                      @if($g->id == $studentd->gender_id)
                        <option value="{{ $g->id }}" selected>{{ $g->name }}</option>
                      @else
                        <option value="{{ $g->id }}">{{ $g->name }}</option>
                      @endif
                      
                    @endforeach
                  </select>
                </div>
                
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Address</label>
                  <input type="text" name="address" class="form-control" value="{{ $studentd->address }}">
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Number</label>
                  <input type="number" name="number" class="form-control" value="{{$studentd->number}}">
                </div>
                 <div class="form-group">
                    <label class="col-sm-4 control-label">Email</label>
                    <input type="email" name="email" class="form-control" value="{{$studentd->email}}">
                  </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Contact Person</label>
                  <input type="text" name="contact_person" class="form-control" value="{{$studentd->contact_person}}">
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Contact Number</label>
                  <input type="number" name="contact_number" class="form-control" value="{{$studentd->contact_number}}">
                </div>
              </div>
            </div>

            <hr>
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Mother's First Name</label>
                  <input type="text" name="m_fname" class="form-control" value="{{ $studentd->m_fname }}">
                </div>
                <div class="form-group">
                  <label class="col-sm-6 control-label">Mother's Middle Name</label>
                  <input type="text" name="m_mname" class="form-control" value="{{ $studentd->m_mname }}">
                </div> 

                <div class="form-group">
                  <label class="col-sm-8 control-label">Father's First Name</label>
                  <input type="text" name="f_fname" class="form-control" value="{{ $studentd->f_fname }}">
                </div>
                <div class="form-group">
                  <label class="col-sm-6 control-label">Father's Middle Name</label>
                  <input type="text" name="f_mname" class="form-control" value="{{ $studentd->f_mname }}">
                </div>
                <div class="form-group">
                  <label class="col-sm-6 control-label">Father's Number</label>
                  <input type="number" name="f_number" class="form-control" value="{{ $studentd->f_number }}">
                </div>
                 
                  <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-10 mg-top">
                        <button id="summary-form-button" type="submit" class="btn btn-primary btn-round">Update</button>
                        <a href="{{ url('students') }}" class="btn btn-danger btn-round">Back</a>
                    </div>
                </div>
              </div>

               <div class="col-lg-6">

                <div class="form-group">
                  <label class="col-sm-4 control-label">Mother's Last Name</label>
                  <input type="text" name="m_lname" class="form-control" value="{{ $studentd->m_lname }}">
                </div>   
                <div class="form-group">
                  <label class="col-sm-4 control-label">Mother's Suffix</label>
                  <input type="text" name="m_suffix" class="form-control" value="{{ $studentd->m_suffix }}">
                </div>

                <div class="form-group">
                  <label class="col-sm-4 control-label">Father's Last Name</label>
                  <input type="text" name="f_lname" class="form-control" value="{{ $studentd->f_lname }}">
                </div>   
                <div class="form-group">
                  <label class="col-sm-4 control-label">Father's Suffix</label>
                  <input type="text" name="f_suffix" class="form-control" value="{{ $studentd->f_suffix }}">
                </div>
                <div class="form-group">
                  <label class="col-sm-6 control-label">Mothers's Number</label>
                  <input type="number" name="m_number" class="form-control" value="{{ $studentd->m_number }}">
                </div>
              </div>
            </div>
            </form>


            <div class="card ">
             <div class="card-header ">
                <h4 class="card-title">Student Account</h4>
                <div class="card-header">
                    <a href="account/create" class="btn btn-primary btn-round">Add Student Account</a>
                </div>
              </div>
              <div class="card-body ">
                <div class="table-responsive">
                  <div class="col-lg-12">
                    <table class="table table-bordered table-striped" width="100%" id="student-account-table">
                      <thead class="text-primary">
                        <tr>
                          <th>Username</th>
                          <th>Postion</th>
                          <th>User Type</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Student Maintenance
                </div>
              </div>
            </div>           
          </div>
          @include('alerts.confirm')
          <div class="modal fade" id="account-modal" tabindex="-1" role="dialog" aria-labelledby="accountModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="">Delete this record?</h5>
                  <button type="button" class="close" data-dismiss="modal" id="close-button">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-footer">
                  
                  <button type="button" class="btn btn-primary btn-round" id="aconfirm-button">Yes</button>
                  <button type="button" class="btn btn-warning btn-round" data-dismiss="modal" id="close-button">No</button>
                </div>
                <!-- <div class="modal-footer">
                 
                </div> -->
              </div>
            </div>
          </div>
      </div>
    
	@endsection
  @section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}">
  @endsection()
  @section('custom_js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{asset('js/student_details/student_details.js')}}"></script>
  @endsection()
