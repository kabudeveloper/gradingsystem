@extends('layouts.parent')
@section('title','Dashboard')

	@section('content')
	<div class="content">
        <div class="row">
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Quick Information</h5>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">
                    <form id="class-form" action="#">
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Last Name </label>
                            <input type="text" name="last_name" class="form-control" value="{{ $student->student->last_name }}" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Course </label>
                            @if($student->course->major == '')
                               <input type="text" name="course" class="form-control" value="{{ $student->course->code }}" readonly>
                            @else
                               <input type="text" name="course" class="form-control" value="{{ $student->course->code }} - {{ $student->course->major }}" readonly>
                            @endif
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Year</label>
                            <input type="text" name="first_name" class="form-control" value="{{ $student->year->code }}" readonly>
                          </div>
                        </div>
                        <div class="col-lg-6">

                          <div class="form-group">
                            <label class="col-sm-4 control-label">First Name</label>
                            <input type="text" name="first_name" class="form-control" value="{{ $student->student->first_name }}" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Set</label>
                            <input type="text" name="section" class="form-control" value="{{ $student->section->code }}" readonly>
                          </div>

                        </div>

                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i>
                </div>
              </div>
            </div>

            <div class="card ">
             <div class="card-header ">
                <h4 class="card-title">All Records</h4>
              <div class="card-body ">
                  <div class="table-responsive">
                    <div class="col-lg-12">
                      <table class="table table-bordered table-striped" width="100%" id="student-grade-details-table">
                        <thead class="text-primary">
                          <tr>
                            <th>Subject</th>
                            <th>Year</th>
                            <th>Semester</th>
                            <th>Grade</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="card-footer ">
                  <hr>
                  <div class="stats">
                    <i class="fa fa-history"></i> Student Grade Details
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </div>
	@endsection

@section('custom_css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}">
@endsection()

@section('custom_js')
  <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{asset('js/dashboard/student.js')}}"></script>

@endsection()
