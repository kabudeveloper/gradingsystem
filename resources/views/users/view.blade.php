@extends('layouts.app')
@section('title','View Users')

	@section('content')
	
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Users</h5>
                <p class="card-category">View Users</p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">

                    <form>
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Username</label>
                            <input type="text" name="username" class="form-control" value="{{ $user->username }}" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Email</label>
                            <input type="email" name="email" class="form-control" value="{{ $user->email }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">First Name</label>
                            <input type="text" name="first_name" class="form-control" value="{{ $user->first_name }}" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Middle Name</label>
                            <input type="text" name="middle_name" class="form-control" value="{{ $user->middle_name }}" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Position</label>
                            <input type="text" class="form-control" value="{{ $user->position->name }}" readonly>
                          </div>
                          <div class="form-group">
                              <div class="col-sm-offset-4 col-sm-10 mg-top">
                                  <a href="{{ url('users') }}" readonly class="btn btn-danger btn-round">Back</a>
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Last Name</label>
                            <input type="text" name="last_name" class="form-control" value="{{ $user->last_name }}" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Suffix</label>
                            <input type="text" name="suffix" class="form-control" value="{{ $user->suffix }}" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">User Type</label>
                              <input type="text"  class="form-control" value="{{ $user->user_type->name }}" readonly>
                            
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> User Maintenance
                </div>
              </div>
            </div>
          </div>
      </div>
    
	@endsection

