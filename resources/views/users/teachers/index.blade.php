@extends('layouts.teacher')
@section('title','Teacher Profile')

	@section('content')
    <div class="row">
      <div class="col-md-4">
        <div class="card card-user">
          <div class="image">
            <img src="{{ asset('themestyle/assets/img/damir-bosnjak.jpg') }}" alt="...">
          </div>
          <div class="card-body">
            <div class="author">
              <a href="#">
                  <img class="avatar border-gray" src="{{ asset('themestyle/assets/img/faces/joe-gardner-2.jpg') }}" alt="Photo contains boy with taas ug suwang">
                
                
                
                <h5 class="title">{{ $userDetails->first_name }} {{ $userDetails->last_name }}</h5>
              </a>
              <p class="description">
                @student_{{ $userDetails->first_name }}
              </p>
            </div>
            <p class="description text-center">
              "Enjoy now
              <br> Hagbong later"
              <!-- <br> that I want" -->
            </p>
          </div>
          <div class="card-footer">
            <hr>
            <div class="button-container">
              <div class="row">
                <div class="col-lg-3 col-md-6 col-6 ml-auto">
                  <h5>113
                    <br>
                    <small>Files</small>
                  </h5>
                </div>
                <div class="col-lg-4 col-md-6 col-6 ml-auto mr-auto">
                  <h5>0.1GB
                    <br>
                    <small>Used</small>
                  </h5>
                </div>
                <div class="col-lg-3 mr-auto">
                  <h5>77kb
                    <br>
                    <small>Spent</small>
                  </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Contacs</h4>
          </div>
          <div class="card-body">
            <ul class="list-unstyled team-members">
              <li>
                <div class="row">
                  <div class="col-md-2 col-2">
                    <div class="avatar">
                      <img src="{{ asset('themestyle/assets/img/faces/erik-lucatero-1.jpg') }}" alt="Circle Image" class="img-circle img-no-padding img-responsive">
                    </div>
                  </div>
                  <div class="col-md-7 col-7">
                    {{ $contact1->firstname }} {{ $contact1->last_name }}
                    <br />
                    <span class="text-success">
                      <small>Available</small>
                    </span>
                    <span class="text-muted">
                      <small>( {{ $contact1->position->name }} )</small>
                    </span>
                  </div>
                  <div class="col-md-3 col-3 text-right">
                    <btn class="btn btn-sm btn-outline-success btn-round btn-icon"><i class="fa fa-envelope"></i></btn>
                  </div>
                </div>
              </li>
              <li>
                @if(!is_null($contact2))
                  <div class="row">
                  <div class="col-md-2 col-2">
                    <div class="avatar">
                      <img src="{{ asset('themestyle/assets/img/faces/kaci-baum-2.jpg') }}" alt="Circle Image" class="img-circle img-no-padding img-responsive">
                      <!-- <img src="{{ asset('themestyle/assets/img/faces/joe-gardner-2.jpg') }}" alt="Circle Image" class="img-circle img-no-padding img-responsive"> -->
                    </div>
                  </div>
                  
                  <div class="col-md-7 col-7">
                    {{ $contact2->first_name }} {{ $contact2->last_name }}
                    <br />
                    <span class="text-success">
                      <small>Available</small>
                    </span>
                    <span class="text-muted">
                      <small>( {{ $contact2->position->name }} )</small>
                    </span>
                  </div>
                  <div class="col-md-3 col-3 text-right">
                    <btn class="btn btn-sm btn-outline-success btn-round btn-icon"><i class="fa fa-envelope"></i></btn>
                  </div>
                </div>
                @endif
                
              </li>
          
            </ul>
          </div>
        </div>
      </div>
      <div class="col-md-8">
        <div class="card card-user">
          <div class="card-header">
            <h5 class="card-title">Edit Profile</h5>
          </div>
          <div class="card-body">
            <form>
              <div class="row">
                <div class="col-md-5 pr-1">
                  <div class="form-group">
                    <label>School (disabled)</label>
                    <input type="text" class="form-control" disabled placeholder="School" value="Sibonga College">
                  </div>
                </div>
                <div class="col-md-3 px-1">
                  <div class="form-group">
                    <label>Username</label>
                    <input type="text" class="form-control" placeholder="Username" value="{{ $username }}">
                  </div>
                </div>
                <div class="col-md-4 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" placeholder="Email" value="{{ $username}}@mail.scc.com">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 pr-1">
                  <div class="form-group">
                    <label>First Name</label>
                  </div>
                    <input type="text" class="form-control" placeholder="Company" value="{{ $userDetails->first_name }}">
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label>Last Name</label>
                  </div>
                    <input type="text" class="form-control" placeholder="Last Name" value="{{ $userDetails->last_name }}">
                </div>
              </div>
              <!-- <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Address</label>
                    <input type="text" class="form-control" placeholder="Home Address" value="{{ $userDetails->address }}">
                  </div>
                </div>
              </div> -->
              <div class="row">
                <div class="col-md-4 pr-1">
                  <div class="form-group">
                    <label>Province</label>
                    <input type="text" class="form-control" placeholder="City" value="Cebu">
                  </div>
                </div>
                <div class="col-md-4 px-1">
                  <div class="form-group">
                    <label>Country</label>
                    <input type="text" class="form-control" placeholder="Country" value="Philippines">
                  </div>
                </div>
                <div class="col-md-4 pl-1">
                  <div class="form-group">
                    <label>Postal Code</label>
                    <input type="number" class="form-control" placeholder="ZIP Code">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>About Me</label>
                    <textarea class="form-control textarea">Teaching is my passion thats why I am your instructor right now</textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="update ml-auto mr-auto">
                  <button type="submit" class="btn btn-primary btn-round disabled">Update Profile</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>


	@endsection

@section('custom_css') 
  
@endsection()
@section('custom_js')
  
  <!-- <script src="{{asset('js/course_details/students/course_details.js')}}"></script> -->
@endsection()
