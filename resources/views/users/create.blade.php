@extends('layouts.app')
@section('title','Add Users')

	@section('content')
	
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Users</h5>
                <p class="card-category">Add Users</p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">

                    <form method="POST" action="{{ URL('users') }}">
                      @csrf
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Username</label>
                            <input type="text" name="username" class="form-control">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Email</label>
                            <input type="email" name="email" class="form-control">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">First Name</label>
                            <input type="text" name="first_name" class="form-control">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Middle Name</label>
                            <input type="text" name="middle_name" class="form-control">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Position</label>
                              <select class="form-control" name="position_id">
                                <option value="0"></option>
                                @foreach($position as $p)
                                <option value="{{$p->id}}"> {{ $p->name }} </option>
                                @endforeach
                              </select>
                          </div>
                          <div class="form-group">
                              <div class="col-sm-offset-4 col-sm-10 mg-top">
                                  <button id="summary-form-button" type="submit" class="btn btn-primary btn-round">Save</button>
                                  <a href="{{ url('users') }}" class="btn btn-danger btn-round">Back</a>
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Password</label>
                            <input type="password" name="password" class="form-control">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Last Name</label>
                            <input type="text" name="last_name" class="form-control">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Suffix</label>
                            <input type="text" name="suffix" class="form-control">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">User Type</label>
                              <select class="form-control" name="user_type_id" id="user_type">
                                @foreach($usertype as $p)
                                <option value="{{$p->id}}"> {{ $p->name }} </option>
                                @endforeach
                              </select>
                            
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> User Maintenance
                </div>
              </div>
            </div>
          </div>
      </div>
    
	@endsection
  @section('custom_js')
  <!-- <script src="{{asset('js/users/users.js')}}"></script> -->

@endsection()
