@extends('layouts.app')
@section('title','Edit Teachers')

	@section('content')
	
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Teachers</h5>
                <p class="card-category">Edit Teachers</p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">

                    <form method="POST" action="{{ URL('teachers/'.$teacher->id) }}">
                      @csrf
                      @method('PATCH')
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label class="col-sm-4 control-label">First Name</label>
                          <input type="text" name="first_name" class="form-control" value="{{ $teacher->first_name }}">
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Last Name</label>
                          <input type="text" name="last_name" class="form-control" value="{{ $teacher->last_name }}">
                        </div>

                        <div class="form-group">
                          <label class="col-sm-4 control-label">Middle Name</label>
                          <input type="text" name="middle_name" class="form-control" value="{{ $teacher->middle_name }}">
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10 mg-top">
                                <button id="summary-form-button" type="submit" class="btn btn-primary btn-round">Update</button>
                                <a href="{{ url('teachers') }}" class="btn btn-danger btn-round">Back</a>
                            </div>
                        </div>
                      </div>
                      
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Teacher Maintenance
                </div>
              </div>
            </div>

            <div class="card ">
             <div class="card-header ">
                <h4 class="card-title">Account</h4>
                <div class="card-header">
                  @if($tr == 1)
                    <a href="account/create" class="btn btn-primary btn-round disabled">Add Teacher Account</a>
                  @else
                    <a href="account/create" class="btn btn-primary btn-round">Add Teacher Account</a>
                  @endif
                  
                </div>
              <div class="card-body ">
                <div class="table-responsive">
                  <div class="col-lg-12">
                    <table class="table table-bordered table-striped" width="100%" id="teacher-account-table">
                      <thead class="text-primary">
                        <tr>
                          <th>Username</th>
                          <th>Position</th>
                          <th>User Type</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Teacher Maintenance
                </div>
              </div>
            </div>
          </div>

            <div class="card ">
             <div class="card-header ">
                <h4 class="card-title">Assigned Course and Classes</h4>
              <div class="card-body ">
                <div class="table-responsive">
                  <div class="col-lg-12">
                    <table class="table table-bordered table-striped" width="100%" id="assigned-details-table">
                      <thead class="text-primary">
                        <tr>
                          <th>Course</th>
                          <th>Major</th>
                          <th>Subject</th>
                          <th>Year</th>
                          <th>Set</th>
                          <th>Semester</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Teacher Maintenance
                </div>
              </div>
            </div>
          </div>

          <div class="card ">
             <div class="card-header ">
                <h4 class="card-title">Unassigned Course and Classes</h4>
            <div class="card-body ">
                <div class="table-responsive">
                  <div class="col-lg-12">
                    <table class="table table-bordered table-striped" width="100%" id="assign-to-details-table">
                      <thead class="text-primary">
                        <tr>
                          <th>Course</th>
                          <th>Major</th>
                          <th>Subject</th>
                          <th>Year</th>
                          <th>Set</th>
                          <th>Semester</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Teacher Maintenance
                </div>
              </div>
            </div>
          </div>
          @include('alerts.confirm')
          </div>
      </div>
    
	@endsection

@section('custom_css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}">
@endsection()
@section('custom_js')
  <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{asset('js/teacher_details/teacher_details.js')}}"></script>
@endsection()

