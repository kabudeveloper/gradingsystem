@extends('layouts.app')
@section('title','Edit Teacher Account')

	@section('content')
	
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Teacher Account</h5>
                <p class="card-category">Edit Teacher Account</p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">

                    <form method="POST" action="{{ URL('teachers/'.$teacherid.'/account/'.$account->id.'/update') }}">
                      @csrf
                      
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Username</label>
                          <input type="text" name="username" class="form-control" value="{{ $account->username }}">
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Email</label>
                          <input type="email" name="email" class="form-control" required value="{{ $account->email }}">
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Password</label>
                          <input type="password" name="password" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Position</label>
                           <select name="position_id" class="form-control">
                              @foreach($position as $p)
                                @if($p->id == $account->position_id)
                                  <option value="{{ $p->id }}" selected>{{ $p->name }}</option>
                                @else
                                  <option value="{{ $p->id }}">{{ $p->name }}</option>
                                @endif
                                
                              @endforeach
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10 mg-top">
                                <button id="summary-form-button" type="submit" class="btn btn-primary btn-round">Update</button>
                                <a href="{{ url('teachers/'.$teacherid.'/edit') }}" class="btn btn-danger btn-round">Back</a>
                            </div>
                        </div>
                      </div>
                      
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Teacher Account Maintenance
                </div>
              </div>
            </div>
          </div>
      </div>
    
	@endsection

@section('custom_css')

@section('custom_js')

