@extends('layouts.teacher')
@section('title','Subjects Assigned')

  @section('content')
  <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">My Subjects Assigned</h5>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">
                    <table id="my-subjects" class="table table-striped">
                      <thead>
                        <tr>
                          <th>Course</th>
                          <th>Subject</th>
                          <th>Year</th>
                          <th>Set</th>
                          <th>Semester</th>
                        </tr>
                        
                      </thead>
                    </table>
                  </div>
                </div>

              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Subjects
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </div>
  @endsection

@section('custom_css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}">
@endsection()

@section('custom_js')
  <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{asset('js/teacher_details/teacher_subjects.js')}}"></script>
@endsection()
