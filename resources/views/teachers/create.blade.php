@extends('layouts.app')
@section('title','Add Teachers')

	@section('content')
	
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Teachers</h5>
                <p class="card-category">Add Teachers</p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">

                    <form method="POST" action="{{ URL('teachers') }}">
                      @csrf
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label class="col-sm-4 control-label">First Name</label>
                          <input type="text" name="first_name" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Last Name</label>
                          <input type="text" name="last_name" class="form-control" required>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-4 control-label">Middle Name</label>
                          <input type="text" name="middle_name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10 mg-top">
                                <button id="summary-form-button" type="submit" class="btn btn-primary btn-round">Save</button>
                                <a href="{{ url('teachers') }}" class="btn btn-danger btn-round">Back</a>
                            </div>
                        </div>
                      </div>
                      
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Teacher Maintenance
                </div>
              </div>
            </div>
          </div>
      </div>
    
	@endsection

@section('custom_css')

@section('custom_js')

