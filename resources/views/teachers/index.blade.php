@extends('layouts.app')
@section('title','Teachers')

	@section('content')
	
    <div class="row">
      <div class="col-md-12">
        <div class="card ">
          <div class="card-header ">
            <h4 class="card-title">Teacher Lists</h4>
            <div class="card-header">
               <a href="teachers/create" class="btn btn-primary btn-round">Add Teacher</a>
            </div>
          </div>
          <div class="card-body">
           
            <div class="table-responsive">

              <div class="col-lg-12">
                <table class="table table-striped" width="100%" id="teachers-table">
                  <thead class="text-primary">
                    <tr>
                      <th>Last Name</th>
                      <th>First Name</th>
                      <th>Middle Name</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
          <div class="card-footer ">
            <hr>
            <div class="stats">
              <i class="fa fa-history"></i> Teacher Maintenance
            </div>
          </div>
        </div>
      </div>
      @include('alerts.confirm')
  </div>
    

	@endsection

@section('custom_css')
  
  <link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}">
  
@endsection()
@section('custom_js')
  <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{asset('js/teachers/teachers.js')}}"></script>
@endsection()
