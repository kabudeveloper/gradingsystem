@extends('layouts.authenticationform')

@section('content')
<div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- login area start -->
    <div class="login-area login-bg">
        <div class="container">
            @include('errormessage.errormessage')
            <div class="login-box ptb--100">
                <form method="GET" action="verified">
                    
                    
                    <div class="login-form-body">
                        <div class="form-gp">
                            <label for="username">Username</label>
                            <input type="text" id="username" value="{{$user->username}}" readonly>
                            <i class="ti-email"></i>
                                
                        </div>
                        
                        <div class="form-gp">
                            <label for="code">Verfication Code</label>
                            <input type="text" id="code" class="focused {{ $errors->has('code') ? ' is-invalid' : '' }} " name="code" required>
                            <i class="ti-email"></i>
                                
                        </div>
                        <div class="submit-btn-area">
                            <button id="form_submit" type="submit">Submit <i class="ti-arrow-right"></i></button>
                        </div>
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection