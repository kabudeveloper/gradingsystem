<!DOCTYPE html>
<html>
<head>
	<title>Register Student</title>
		<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('themestyle/assets/img/apple-icon.png') }}">
	  	<link href="{{ asset('themestyle/assets/img/favicon.png') }}" rel="icon" type="image/png" />

	  	<!-- fonts -->
	  	<link rel="stylesheet" href="{{asset('template_styles/css/font-awesome.min.css')}}">
	  	<link href="{{ asset('themestyle/assets/css/font.css') }}" rel="stylesheet" />

	  	<!-- CSS Files -->
	  	<link href="{{ asset('themestyle/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
	  	<link href="{{ asset('themestyle/assets/css/paper-dashboard.css?v=2.0.0') }}" rel="stylesheet" />
	  	<link rel="stylesheet" href="{{asset('template_styles/css/styles.css')}}">
</head>
<body>

	<div class="wrapper">
		<div class="content">
			@include('errormessage.errormessage')
		</div>
		<div class="main-panel" style="width: 100%;">
			@include('titlearea.titlearea')
			<div class="row">
	          <div class="col-md-12">
	            <div class="card ">
	              <div class="card-header ">
	                <h5 class="card-title" style="margin-top: 80px;">Register Student</h5>
	                <!-- <p class="card-category">Add Users</p> -->
	              </div>
	              <div class="card-body ">
	                <div class="row">
	                  <div class="col-lg-12">

	                    <form method="POST" action="{{ URL('registerstudent') }}">
	                      @csrf
	                      <div class="row">
	                      	<div class="col-lg-12">
		                      	<div class="form-group">
		                           <label class="col-sm-4 control-label">ID Number</label>
		                            <input type="number" name="id_number" class="form-control">
		                         </div>
	                      	</div>
	                      	
	                        <div class="col-lg-6">
	                          <div class="form-group">
	                            <label class="col-sm-4 control-label">Username</label>
	                            <input type="text" name="username" class="form-control">
	                          </div>
	                          
	                        </div>
	                        <div class="col-lg-6">
	                          <div class="form-group">
	                            <label class="col-sm-4 control-label">Password</label>
	                            <input type="password" name="password" class="form-control">
	                          </div>
	                         
	                        </div>
	                      </div>
	                     
	                      <br><hr>
	                      
	               			<div class="form-group">
                              	<div class="col-sm-offset-4 col-sm-10 mg-top">
                                  <button id="summary-form-button" type="submit" class="btn btn-primary btn-round">Save</button>
                                  <a href="{{ url('users') }}" class="btn btn-danger btn-round">Back</a>
                              	</div>
                          	</div>
	                    </form>
	                  </div>
	                </div>
	               	
	              </div>
	              <div class="card-footer ">
	                <hr>
	                <div class="stats">
	                  <i class="fa fa-history"></i> Register Student
	                </div>
	              </div>
	            </div>
	          </div>
	      	</div>
		</div>
	</div>
	<footer class="footer footer-black  footer-white">
    <div class="container-fluid">
      <div class="row">
          <div class="credits ml-auto">
          <span class="copyright">
            ©
            <script>
              document.write(new Date().getFullYear())
            </script>, made with <i class="fa fa-heart heart"></i> Kabu
          </span>
        </div>
      </div>
       
    </div>
  </footer>
</body>
</html>