@extends('layouts.app')
@section('title','Courses')

	@section('content')
	
    <div class="row">
      <div class="col-md-12">
        <div class="card ">
          <div class="card-header ">
            <h4 class="card-title">Course Lists</h4>
            <div class="card-header">
               <a href="courses/create" class="btn btn-primary btn-round">Add Course</a>
            </div>
          </div>
          <div class="card-body ">
           
            <div class="table-responsive">
              <div class="col-lg-12">
                <table class="table table-striped" width="100%" id="courses-table">
                  <thead class="text-primary">
                    <tr>
                      <th></th>
                      <th>Code</th>
                      <th>Name</th>
                      <th>Major</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
          <div class="card-footer ">
            <hr>
            <div class="stats">
              <i class="fa fa-history"></i> Course Maintenance
            </div>
          </div>
        </div>
      </div>
      @include('alerts.confirm')
  </div>
    

	@endsection

@section('custom_css')
  
  <link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}">
  
@endsection()
@section('custom_js')
  <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{asset('js/courses/courses.js')}}"></script>
@endsection()
