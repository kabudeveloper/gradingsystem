@extends('layouts.app')
@section('title','Edit Courses')

	@section('content')
	
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Courses</h5>
                <p class="card-category">Edit Courses</p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">

                    <form method="POST" action="{{ URL('courses/'.$id) }}">
                      @csrf
                      @method('PATCH')

                      <div class="col-lg-6">
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Code</label>
                          <input type="text" name="code" class="form-control" maxlength="10" required value="{{$course->code}}">
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Name</label>
                          <input type="text" name="name" class="form-control" required value="{{$course->name}}">
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Major</label>
                          <input type="text" name="major" class="form-control" required value="{{$course->major}}">
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10 mg-top">
                                <button id="summary-form-button" type="submit" class="btn btn-primary btn-round">Update</button>
                                <a href="{{ url('courses') }}" class="btn btn-danger btn-round">Back</a>
                            </div>
                        </div>
                      </div>
                      
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Course Maintenance
                </div>
              </div>
            </div>
          <div class="card ">
             <div class="card-header ">
                <h4 class="card-title">Course Details</h4>
                <div class="card-header">
                  <a href="details/create" class="btn btn-primary btn-round">Add Course Details</a>
              </div>
            <div class="card-body ">
                <div class="table-responsive">
                  <div class="col-lg-12">
                    <table class="table table-bordered table-striped" width="100%" id="course-details-table">
                      <thead class="text-primary">
                        <tr>
                          <th>Subject Code</th>
                          <th>Subject Name</th>
                          <th>Year</th>
                          <th>Set</th>
                          <th>Semester</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Student Maintenance
                </div>
              </div>
            </div>
          </div>
          @include('alerts.confirm')
      </div>
    
	@endsection

@section('custom_css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}">
@endsection()
@section('custom_js')
  <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{asset('js/course_details/course_details.js')}}"></script>
@endsection()
