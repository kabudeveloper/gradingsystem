
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title') | {{ env('APP_NAME') }}</title>

  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('themestyle/assets/img/apple-icon.png') }}">
  <link href="{{ asset('themestyle/assets/img/favicon.png') }}" rel="icon" type="image/png" />

  <!-- fonts -->
  <link rel="stylesheet" href="{{asset('template_styles/css/font-awesome.min.css')}}">
  <link href="{{ asset('themestyle/assets/css/font.css') }}" rel="stylesheet" />
<!-- 
  <link rel="stylesheet" href="{{asset('template_styles/css/font-awesome.min.css')}}">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet"> -->
 

  <!-- CSS Files -->
  <link href="{{ asset('themestyle/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('themestyle/assets/css/paper-dashboard.css?v=2.0.0') }}" rel="stylesheet" />
  <link rel="stylesheet" href="{{asset('template_styles/css/styles.css')}}">
  
  
</head>

<body class="">
  <div class="wrapper ">
    @include('sidebar.teachersidebar')
    <div class="main-panel">
      @include('titlearea.titlearea')
      <div class="content">
        @include('errormessage.errormessage')
        @yield('content')
      </div>
     
    </div>
  </div>
  <footer class="footer footer-black  footer-white">
    <div class="container-fluid">
      <div class="row">
          <div class="credits ml-auto">
          <span class="copyright">
            ©
            <script>
              document.write(new Date().getFullYear())
            </script>, made with <i class="fa fa-heart heart"></i> Kabu
          </span>
        </div>
      </div>
       
    </div>
  </footer>

  <!--   Core JS Files   -->
  <script src="{{ asset('themestyle/assets/js/core/jquery.min.js')}}"></script>
  <script src="{{ asset('themestyle/assets/js/core/popper.min.js')}}"></script>
  <script src="{{ asset('themestyle/assets/js/core/bootstrap.min.js')}}"></script>
  <script src="{{ asset('themestyle/assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>

  <!-- Chart JS -->
  <script src="{{ asset('themestyle/assets/js/plugins/chartjs.min.js')}}"></script>
  <!-- font -->
  <!-- <script src="{{ asset('js/fonts/e3b83e783b.js')}}"></script> -->

  <!--  Notifications Plugin    -->
  <script src="{{ asset('themestyle/assets/js/plugins/chartjs.min.js')}}"></script>

  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('themestyle/assets/js/paper-dashboard.min.js?v=2.0.')}}" type="javascript"></script>

 <!-- datatables -->
  
  @yield('custom_js')
  @yield('custom_css')

</body>

</html>
