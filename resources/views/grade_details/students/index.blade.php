@extends('layouts.teacher')
@section('title','Grades')

  @section('content')
  <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Grades</h5>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">
                    <form action="#">
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Last Name</label>
                            <input type="text" readonly class="form-control" value="{{ $student->student->last_name }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Course</label>
                            <input type="text" readonly class="form-control" value="{{ $student->course->code }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Year</label>
                            <input type="text" readonly class="form-control" value="{{ $student->year->code }} Year">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Term</label>
                            @if($term->grade->term_id == 1)
                            <input type="text" readonly class="form-control" value="Mid Term">

                            @else
                            <input type="text" readonly class="form-control" value="Final Term">

                            @endif
                          </div>
                          <div class="form-group">
                            <input type="hidden" name="studentid" value="{{$student->student_id}}">
                            <input type="hidden" name="subjectgradeid" value="{{ $subjectgradeid }}">
                            <a href="{{ url('gradedetails') }}" class="btn btn-danger btn-round">Back</a>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">First Name</label>
                            <input type="text" readonly class="form-control" value="{{ $student->student->first_name }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Set</label>
                            <input type="text" readonly class="form-control" value="{{ $student->section->code }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Semester</label>
                            @if($term->grade->semester_id == 1)
                            <input type="text" readonly class="form-control" value="1st">
                            @else
                              <input type="text" readonly class="form-control" value="2nd">
                            @endif
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Subject</label>
                            <input type="text" readonly class="form-control" value="{{ $term->grade->subject->name }}">
                          </div>
                          
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Grades
                </div>
              </div>
            </div>

            <!-- new card for table -->
            <div class="card ">
             <div class="card-header ">
                <h4 class="card-title">Term Exam</h4>
              <div class="card-body ">
                  <div class="table-responsive">
                    <div class="col-lg-12">
                      <table class="table table-bordered table-striped" width="100%" id="term-details-table">
                        <thead class="text-primary">
                          <tr>
                            <th>Title</th>
                            <th>Total Score</th>
                            <th>Score</th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="card-footer ">
                  <hr>
                  <div class="stats">
                    <i class="fa fa-history"></i> Term Exam Details
                  </div>
                </div>
              </div>
            </div>
            <div class="card ">
             <div class="card-header ">
                <h4 class="card-title">Quiz</h4>
              <div class="card-body ">
                  <div class="table-responsive">
                    <div class="col-lg-12">
                      <table class="table table-bordered table-striped" width="100%" id="quiz-details-table">
                        <thead class="text-primary">
                          <tr>
                            <th>Title</th>
                            <th>Total Score</th>
                            <th>Score</th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="card-footer ">
                  <hr>
                  <div class="stats">
                    <i class="fa fa-history"></i> Quiz Details
                  </div>
                </div>
              </div>
            </div>
            <div class="card ">
             <div class="card-header ">
                <h4 class="card-title">Assignments</h4>
              <div class="card-body ">
                  <div class="table-responsive">
                    <div class="col-lg-12">
                      <table class="table table-bordered table-striped" width="100%" id="ass-details-table">
                        <thead class="text-primary">
                          <tr>
                            <th>Title</th>
                            <th>Total Score</th>
                            <th>Score</th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="card-footer ">
                  <hr>
                  <div class="stats">
                    <i class="fa fa-history"></i> Assignments Details
                  </div>
                </div>
              </div>
            </div>
            <div class="card ">
             <div class="card-header ">
                <h4 class="card-title">Attendance</h4>
              <div class="card-body ">
                  <div class="table-responsive">
                    <div class="col-lg-12">
                      <table class="table table-bordered table-striped" width="100%" id="attendance-details-table">
                        <thead class="text-primary">
                          <tr>
                            <th>Title</th>
                            <th>Totalscore</th>
                            <th>Score</th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="card-footer ">
                  <hr>
                  <div class="stats">
                    <i class="fa fa-history"></i> Attendance Details
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  @endsection

@section('custom_css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}">
@endsection()

@section('custom_js')
  <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{asset('js/grade_details/students/grade_details.js')}}"></script>

@endsection()
