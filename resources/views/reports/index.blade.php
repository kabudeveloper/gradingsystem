@extends('layouts.app')
@section('title','Reports')

  @section('content')
  <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Reports</h5>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">
                    <form id="report-form">
                      <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                              <label class="col-sm-4 control-label">Course</label>
                              <select id="course" name="course" class="form-control" required>
                                  <option></option>
                                  <option value="0">All</option>
                                  @foreach($course as $c)
                                  <option value="{{ $c->id }}">{{ $c->code }}</option>

                                  @endforeach

                                
                              </select>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-4 control-label">Subject</label>
                              <select id="subject" name="subject" class="form-control" required>

                              </select>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-4 control-label">Semester</label>
                              <select id="semester" name="semester" class="form-control" required>
                                  <option value="0">All</option>
                                  <option value="1">First Sem</option>
                                  <option value="2">Second Sem</option>
                              </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                              <label class="col-sm-4 control-label">Year</label>
                              <select id="year" name="year" class="form-control" required>
                                <option value="0">All</option>
                                @foreach($year as $y)
                                  <option value="{{ $y->id }}"> {{ $y->code }}</option>
                                @endforeach
                              </select>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-4 control-label">Set</label>
                              <select id="section" name="section" class="form-control" required>
                                
                              </select>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <button class="btn btn-primary btn-rounded" type="submit">Go</button>
                      </div>
                    </form>
                  </div>
                </div> <br>
                <div class="row">
                  <div class="table-responsive">
                    <div class="col-lg-12">
                      <div class="report-button">
                        
                      </div>
                        <!-- <a href="attendancegrade/create" class="btn btn-success btn-round">Add new</a> -->
                        
                        <table class="table" width="100%" id="report-table">
                          <thead class="text-primary">
                            <tr>
                              <th>Name</th>
                              <th>Course</th>
                              <th>Subject</th>
                              <th>Year</th>
                              <th>Set</th>
                              <th>Semester</th>
                            </tr>
                          </thead>
                        </table>
                    </div>
                  </div>
                </div>

              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Quiz
                </div>
              </div>
            </div>
          </div>
        </div>
        @include('alerts.confirm')
      </div>
  @endsection

@section('custom_css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}">
@endsection()

@section('custom_js')
  <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{asset('js/reports/reports.js')}}"></script>

@endsection()
