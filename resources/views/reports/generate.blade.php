<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<title></title>
</head>
<body>
	<h2 align="center">Grade Details Report</h2>
	<br>
	<table class="table table-stripped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Course</th>
				<th>Subject</th>
				<th>Year</th>
				<th>Set</th>
				<th>Semester</th>
			</tr>
		</thead>
		<tbody>
			
			@foreach($datas as $d)
			<tr>
				<td>{{ $d->last_name }} {{ $d->name }}</td>
				<td>{{ $d->course }}</td>
				<td>{{ $d->subject }}</td>
				<td>{{ $d->year }}</td>
				<td>{{ $d->section }}</td>
				@if($d->_semester == 1)
				<td>1st</td>
				@else
				<td>2nd</td>
				@endif
				
			</tr>
			@endforeach

		</tbody>
		
	</table>
</body>
</html>