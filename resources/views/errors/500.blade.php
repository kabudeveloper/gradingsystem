<!DOCTYPE html>
<html>
<head>
	<title>Authentication Failed</title>
		<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('themestyle/assets/img/apple-icon.png') }}">
	  	<link href="{{ asset('themestyle/assets/img/favicon.png') }}" rel="icon" type="image/png" />

	  	<!-- fonts -->
	  	<link rel="stylesheet" href="{{asset('template_styles/css/font-awesome.min.css')}}">
	  	<link href="{{ asset('themestyle/assets/css/font.css') }}" rel="stylesheet" />
	
	  	<!-- CSS Files -->
	  	<link href="{{ asset('themestyle/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
	  	<link href="{{ asset('themestyle/assets/css/paper-dashboard.css?v=2.0.0') }}" rel="stylesheet" />
	  	<link rel="stylesheet" href="{{asset('template_styles/css/styles.css')}}">
	
</head>
<body style="background-color: #ecf0f1">

	<div class="container">
		<div style="text-align: center;">
			<p style="color: #404040; font-size: 180px;">500</p>
		</div>
		<div style="text-align: center;">
			<h2 style="color: #404040">{{ $exception->getMessage() }}</h2>
			<p>Please go back to Login Page.</p>
		</div>
		<div style="text-align: center;">

			<a href="{{ url('/relogout') }}" class="btn btn-danger">BACK</a>
		</div>
	</div>
	
</body>
</html>