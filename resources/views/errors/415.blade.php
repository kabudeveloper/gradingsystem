@extends('layouts.teacher')
@section('title','415')

@section('content')
		<div class="row">
		    <div class="col-md-12">
		        <br><br><br><br>
		        <div>
		            <div style="background: white;padding:20px;border-radius:10px;padding-bottom:25px;">
		                <h2>  {{ $exception->getMessage() }} </h2>
		            </div>
		        </div>
		    </div>
		</div>
	@section('customjs')
    <script type="text/javascript">
      var APP_URL = {!! json_encode(url('/')) !!};
    </script>
    
    @endsection
@endsection()