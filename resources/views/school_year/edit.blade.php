@extends('layouts.app')
@section('title','Edit School Year')

	@section('content')
	
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">School Year</h5>
                <p class="card-category">Edit School Year</p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">

                    <form method="POST" action="{{ URL('schoolyear/'.$id) }}">
                      @csrf
                      @method('PATCH')
                      <div class="row">
                        <div class="col-lg-2">
                            <input type="number" name="from" class="form-control" required value="{{ $from }}">
                        </div>
                        <div class="col-lg-1">
                            -
                        </div>
                        <div class="col-lg-2"> 
                            <input type="number" name="to" class="form-control" required value="{{ $to }}">
                          
                        </div>
                      </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10 mg-top">
                                <button id="summary-form-button" type="submit" class="btn btn-primary btn-round">Save</button>
                                <a href="{{ url('schoolyear') }}" class="btn btn-danger btn-round">Back</a>
                            </div>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">

                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> School Year Maintenance
                </div>
              </div>
            </div>
          </div>
      </div>
    
	@endsection

@section('custom_css')

@section('custom_js')

