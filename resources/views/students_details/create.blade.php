@extends('layouts.app')
@section('title','Add Student Details')

	@section('content')
	
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Students</h5>
                <p class="card-category">Add Students Details</p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">

                    <form method="POST" action="{{ URL('students/details/'.$id) }}">
                      @csrf
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label control-label1">Course</label>
                            <select class="form-control" name="course_id"> 
                              @foreach($course as $c)
                                @if($c->major == "")
                                  <option value="{{ $c->id }}">{{ $c->code }}</option>
                                @else
                                  <option value="{{ $c->id }}">{{ $c->code }} - {{ $c->major }}</option>
                                @endif
                                
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Year</label>
                             <select class="form-control" name="year_id"> 
                              @foreach($year as $y)
                                <option value="{{ $y->id }}">{{ $y->code }} Year</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Set</label>
                             <select class="form-control" name="section_id"> 
                              @foreach($section as $s)
                                <option value="{{ $s->id }}">{{ $s->code }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Gender</label>
                             <select class="form-control" name="gender_id"> 
                              @foreach($gender as $s)
                                <option value="{{ $s->id }}">{{ $s->name }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">School Year</label>
                             <select class="form-control" name="sc_year"> 
                              @foreach($sc as $s)
                                <option value="{{ $s->id }}">{{ $s->school_year }}</option>
                              @endforeach
                            </select>
                          </div>
                          
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Address</label>
                            <input type="text" name="address" class="form-control">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Number</label>
                            <input type="number" name="number" class="form-control">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Contact Person</label>
                            <input type="text" name="contact_person" class="form-control">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Contact Number</label>
                            <input type="number" name="contact_number" class="form-control">
                          </div>  
                        </div>
                      </div>
                      <hr>

                      <div class="row">
                        <div class="col-lg-6">

                          <div class="form-group">
                            <label class="col-sm-4 control-label">Mother's First Name</label>
                            <input type="text" name="m_fname" class="form-control">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-6 control-label">Mother's Middle Name</label>
                            <input type="text" name="m_mname" class="form-control">
                          </div> 

                          <div class="form-group">
                            <label class="col-sm-4 control-label">Father's First Name</label>
                            <input type="text" name="f_fname" class="form-control">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-6 control-label">Father's Middle Name</label>
                            <input type="text" name="f_mname" class="form-control">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-6 control-label">Father's Number</label>
                            <input type="number" name="f_number" class="form-control">
                          </div>
                          <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10 mg-top">
                                <button id="summary-form-button" type="submit" class="btn btn-primary btn-round">Save</button>
                                <a href="{{ url('students') }}" class="btn btn-danger btn-round">Back</a>
                            </div>
                          </div>
                        </div>

                         <div class="col-lg-6">

                          <div class="form-group">
                            <label class="col-sm-4 control-label">Mother's Last Name</label>
                            <input type="text" name="m_lname" class="form-control">
                          </div>   
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Mother's Suffix</label>
                            <input type="text" name="m_suffix" class="form-control">
                          </div>

                          <div class="form-group">
                            <label class="col-sm-4 control-label">Father's Last Name</label>
                            <input type="text" name="f_lname" class="form-control">
                          </div>   
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Father's Suffix</label>
                            <input type="text" name="f_suffix" class="form-control">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-6 control-label">Mothers's Number</label>
                            <input type="number" name="m_number" class="form-control">
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Student Details Maintenance
                </div>
              </div>
            </div>
          </div>
      </div>
    
	@endsection


