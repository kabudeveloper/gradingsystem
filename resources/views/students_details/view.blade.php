@extends('layouts.app')
@section('title','Edit Student Details')

  @section('content')
  
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Students</h5>
                <p class="card-category">Edit Student Details</p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">

                    <form>
                      @csrf
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Course</label>
                            @if($student->course->major == "")
                              <input type="text" value="{{$student->course->code}}" class="form-control" readonly>
                            @else
                              <input type="text" value="{{$student->course->code}} - {{ $student->course->major }}" class="form-control" readonly>
                            @endif
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Year</label>
                             <input type="text" value="{{$student->year->code}} Year" class="form-control" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Set</label>
                             <input type="text" class="form-control" value="{{ $student->section->code}}" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Gender</label>
                             <input type="text" class="form-control" value="{{ $student->gender->name }}" readonly>
                          </div>
                          
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Address</label>
                            <input type="text" name="address" class="form-control" value="{{ $student->address }}" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Number</label>
                            <input type="number" name="number" class="form-control" value="{{$student->number}}" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Contact Person</label>
                            <input type="text" name="contact_person" class="form-control" value="{{$student->contact_person}}" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Contact Number</label>
                            <input type="number" name="contact_number" class="form-control" value="{{$student->contact_number}}" readonly>
                          </div>
                        </div>
                      </div>

                      <hr>
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Mother's First Name</label>
                            <input type="text" name="m_fname" class="form-control" value="{{ $student->m_fname }}" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-6 control-label">Mother's Middle Name</label>
                            <input type="text" name="m_mname" class="form-control" value="{{ $student->m_mname }}" readonly>
                          </div> 

                          <div class="form-group">
                            <label class="col-sm-4 control-label">Father's First Name</label>
                            <input type="text" name="f_fname" class="form-control" value="{{ $student->f_fname }}" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-6 control-label">Father's Middle Name</label>
                            <input type="text" name="f_mname" class="form-control" value="{{ $student->f_mname }}" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-6 control-label">Father's Number</label>
                            <input type="number" name="f_number" class="form-control" value="{{ $student->f_number }}" readonly>
                          </div>
                         <!--  <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10 mg-top">
                                <button id="summary-form-button" type="submit" class="btn btn-primary btn-round">Save</button>
                                <a href="{{ url('students') }}" class="btn btn-danger btn-round">Back</a>
                            </div>
                          </div> -->
                            <div class="form-group">
                              <div class="col-sm-offset-4 col-sm-10 mg-top">
                                 
                                  <a href="{{ url('students/'.$student->student_id.'/edit') }}" class="btn btn-danger btn-round">Back</a>
                              </div>
                            </div>
                        </div>

                         <div class="col-lg-6">

                          <div class="form-group">
                            <label class="col-sm-4 control-label">Mother's Last Name</label>
                            <input type="text" name="m_lname" class="form-control" value="{{ $student->m_lname }}" readonly>
                          </div>   
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Mother's Suffix</label>
                            <input type="text" name="m_suffix" class="form-control" value="{{ $student->m_suffix }}" readonly>
                          </div>

                          <div class="form-group">
                            <label class="col-sm-4 control-label">Father's Last Name</label>
                            <input type="text" name="f_lname" class="form-control" value="{{ $student->f_lname }}" readonly>
                          </div>   
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Father's Suffix</label>
                            <input type="text" name="f_suffix" class="form-control" value="{{ $student->f_suffix }}" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-6 control-label">Mothers's Number</label>
                            <input type="number" name="m_number" class="form-control" value="{{ $student->m_number }}" readonly>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Student Details Maintenance
                </div>
              </div>
            </div>
          </div>
      </div>
    
  @endsection
  @section('custom_js')
  

  @endsection()

