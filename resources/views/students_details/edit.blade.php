@extends('layouts.app')
@section('title','Edit Student Details')

	@section('content')
	
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Students</h5>
                <p class="card-category">Edit Student Details</p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">

                    <form method="POST" action="{{ URL('students/details/'.$stud_id.'/'.$id.'/update') }}">
                      @csrf
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Course</label>
                            <select class="form-control" name="course_id"> 
                              @foreach($course as $c)
                                @if($c->major == "")
                                  @if($c->id == $student->course_id)
                                    <option value="{{ $c->id }}" selected>{{ $c->code }}</option>
                                  @else
                                    <option value="{{ $c->id }}">{{ $c->code }}</option>
                                  @endif
                                @else
                                  @if($c->id == $student->course_id)
                                    <option value="{{ $c->id }}" selected>{{ $c->code }} - {{ $c->major }}</option>
                                  @else
                                    <option value="{{ $c->id }}">{{ $c->code }} - {{ $c->major }}</option>
                                  @endif
                                @endif
                                
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Year</label>
                             <select class="form-control" name="year_id"> 
                              @foreach($year as $y)
                                @if($y->id == $student->year_id)
                                  <option value="{{ $y->id }}" selected>{{ $y->code }} Year</option>
                                @else
                                  <option value="{{ $y->id }}">{{ $y->code }} Year</option>
                                @endif
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">School year</label>
                             <select class="form-control" name="school_year_id"> 
                              @foreach($sc as $s)
                                @if($s->id == $student->school_year_id)
                                  <option value="{{ $s->id }}" selected>{{ $s->school_year }}</option>
                                @else
                                  <option value="{{ $s->id }}">{{ $s->school_year }}</option>
                                @endif
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Set</label>
                             <select class="form-control" name="section_id"> 
                              @foreach($section as $s)
                                @if($s->id == $student->section_id)
                                  <option value="{{ $s->id }}" selected>{{ $s->code }}</option>
                                @else
                                  <option value="{{ $s->id }}">{{ $s->code }}</option>
                                @endif
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Gender</label>
                             <select class="form-control" name="gender_id"> 
                              @foreach($gender as $g)
                                @if($g->id == $student->gender_id)
                                  <option value="{{ $g->id }}" selected>{{ $g->name }}</option>
                                @else
                                  <option value="{{ $g->id }}">{{ $g->name }}</option>
                                @endif
                                
                              @endforeach
                            </select>
                          </div>
                          
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Address</label>
                            <input type="text" name="address" class="form-control" value="{{ $student->address }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Number</label>
                            <input type="number" name="number" class="form-control" value="{{$student->number}}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Contact Person</label>
                            <input type="text" name="contact_person" class="form-control" value="{{$student->contact_person}}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Contact Number</label>
                            <input type="number" name="contact_number" class="form-control" value="{{$student->contact_number}}">
                          </div>
                        </div>
                      </div>

                      <hr>
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Mother's First Name</label>
                            <input type="text" name="m_fname" class="form-control" value="{{ $student->m_fname }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-6 control-label">Mother's Middle Name</label>
                            <input type="text" name="m_mname" class="form-control" value="{{ $student->m_mname }}">
                          </div> 

                          <div class="form-group">
                            <label class="col-sm-4 control-label">Father's First Name</label>
                            <input type="text" name="f_fname" class="form-control" value="{{ $student->f_fname }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-6 control-label">Father's Middle Name</label>
                            <input type="text" name="f_mname" class="form-control" value="{{ $student->f_mname }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-6 control-label">Father's Number</label>
                            <input type="number" name="f_number" class="form-control" value="{{ $student->f_number }}">
                          </div>
                         <!--  <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10 mg-top">
                                <button id="summary-form-button" type="submit" class="btn btn-primary btn-round">Save</button>
                                <a href="{{ url('students') }}" class="btn btn-danger btn-round">Back</a>
                            </div>
                          </div> -->
                            <div class="form-group">
                              <div class="col-sm-offset-4 col-sm-10 mg-top">
                                  <button id="summary-form-button" type="submit" class="btn btn-primary btn-round">Update</button>
                                  <a href="{{ url('students/'.$student->student_id.'/edit') }}" class="btn btn-danger btn-round">Back</a>
                              </div>
                            </div>
                        </div>

                         <div class="col-lg-6">

                          <div class="form-group">
                            <label class="col-sm-4 control-label">Mother's Last Name</label>
                            <input type="text" name="m_lname" class="form-control" value="{{ $student->m_lname }}">
                          </div>   
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Mother's Suffix</label>
                            <input type="text" name="m_suffix" class="form-control" value="{{ $student->m_suffix }}">
                          </div>

                          <div class="form-group">
                            <label class="col-sm-4 control-label">Father's Last Name</label>
                            <input type="text" name="f_lname" class="form-control" value="{{ $student->f_lname }}">
                          </div>   
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Father's Suffix</label>
                            <input type="text" name="f_suffix" class="form-control" value="{{ $student->f_suffix }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-6 control-label">Mothers's Number</label>
                            <input type="number" name="m_number" class="form-control" value="{{ $student->m_number }}">
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Student Details Maintenance
                </div>
              </div>
            </div>
            @include('alerts.confirm')
          </div>
      </div>
    
	@endsection
  @section('custom_js')
  

  @endsection()

