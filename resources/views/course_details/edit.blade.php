@extends('layouts.app')
@section('title','Edit Course Details')

	@section('content')
	
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Course Details</h5>
                <p class="card-category">Edit Course Details</p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">

                    <form method="POST" action="{{ URL('courses/'.$courseid.'/details/'.$courseDetails->id.'/update') }}">
                      @csrf
                      <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Course</label>
                          @if($course->major == "")
                            <input type="text" value="{{ $course->code }}" class="form-control" readonly>
                          @else
                            <input type="text" value="{{ $course->code }} - {{ $course->major }}" class="form-control" readonly>
                          @endif
                          
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Subject</label>
                          <select class="form-control" name="subject_id">
                            <option value="0"> </option> 
                            @foreach($subject as $s)
                              @if($courseDetails->subject_id == $s->id)
                                <option value="{{ $s->id }}" selected>{{ $s->code }} - {{ $s->name }}</option>
                              @else
                                <option value="{{ $s->id }}">{{ $s->code }} - {{ $s->name }}</option>
                              @endif
                              
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group">

                          <label class="col-sm-4 control-label">Year</label>
                          <select class="form-control" name="year_id">
                            @foreach($year as $y)
                              @if($courseDetails->year_id == $y->id)
                                <option value="{{ $y->id }}" selected>{{ $y->code }} Year</option>
                              @else
                                <option value="{{ $y->id }}">{{ $y->code }} Year</option>
                              @endif
                              
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Set</label>
                          <select class="form-control" name="section_id">
                              @foreach($section as $s)
                                @if($courseDetails->section_id == $s->id)
                                  <option value="{{ $s->id }}" selected> {{ $s->code }}</option>
                                @else
                                  <option value="{{ $s->id }}"> {{ $s->code }}</option>
                                @endif
                              @endforeach
                          </select>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Semester</label>
                          <select class="form-control" name="_semester">
                            @if($courseDetails->_semester == 1)
                              <option value="1" selected> 1st </option>
                              <option value="2"> 2nd </option>
                            @else
                              <option value="1"> 1st</option>
                              <option value="2" selected> 2nd</option>
                            @endif
                          </select>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Units</label>
                          <input type="number" name="units" value="{{ $courseDetails->units }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10 mg-top">
                                <button id="summary-form-button" type="submit" class="btn btn-primary btn-round">Update</button>
                                <a href="{{ url('courses/'.$courseid.'/edit') }}" class="btn btn-danger btn-round">Back</a>
                            </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                            <!--<div class="form-group">
                            <input type="hidden" name="">
                          </div>
                            -->
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Term Exam</label>
                            <input type="number" name="term_exam" class="form-control" placeholder="%" value="{{ $courseDetails->term_exam }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Quiz</label>
                            <input type="number" name="quiz" class="form-control" placeholder="%" value="{{ $courseDetails->quiz }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Assignment</label>
                            <input type="number" name="assignment" class="form-control" placeholder="%" value="{{ $courseDetails->assignment }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Attendance</label>
                            <input type="number" name="attendance" class="form-control" placeholder="%" value="{{ $courseDetails->attendance }}">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Performance</label>
                            <input type="number" name="participation" class="form-control" placeholder="%" value="{{ $courseDetails->participation }}">
                          </div>
                          

                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Course Details Maintenance
                </div>
              </div>
            </div>
          </div>
          @include('alerts.confirm')
      </div>
    
	@endsection

@section('custom_css')

@section('custom_js')

