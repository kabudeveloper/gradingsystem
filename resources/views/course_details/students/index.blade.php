@extends('layouts.student')
@section('title','Course Details')

	@section('content')
	
    <div class="row">
      <div class="col-md-12">
        <div class="card ">
          <div class="card-header ">
            <h4 class="card-title">My Course</h4>
          </div>
          <div class="card-body ">
            <form>
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Course Code</label>
                    <input type="text" name="course" class="form-control" readonly value="{{ $course->course->code }}">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Major</label>
                    <input type="text" name="major" class="form-control" readonly value="{{ $course->course->major }}">
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Course Name</label>
                    <input type="text" name="name" class="form-control" readonly value="{{ $course->course->name }}">
                  </div>
                </div>
              </div>
              <input type="hidden" name="courseid" value="{{$course->course->id}}">
            </form>
          </div>
          <div class="card-footer ">
            <hr>
            <div class="stats">
              <i class="fa fa-history"></i> Course
            </div>
          </div>
        </div>

        <div class="card ">
          <div class="card-header ">
            <h4 class="card-title">Course Details</h4>
            <!-- <div class="card-header">
               <a href="courses/create" class="btn btn-primary btn-round">Add Course</a>
            </div> -->
          </div>
          <div class="card-body ">
           
            <div class="table-responsive">
              <div class="col-lg-12">
                <table class="table table-striped" width="100%" id="course-details-table">
                  <thead class="text-primary">
                    <tr>
                      <th>Code</th>
                      <th>Name</th>
                      <th>Term Exam</th>
                      <th>Quiz</th>
                      <th>Assignment</th>
                      <th>Attendance</th>
                      <th>Performance</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
          <div class="card-footer ">
            <hr>
            <div class="stats">
              <i class="fa fa-history"></i> Course Maintenance
            </div>
          </div>
        </div>
      </div>
      @include('alerts.confirm')
  </div>
    

	@endsection

@section('custom_css')
  
  <link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}">
  
@endsection()
@section('custom_js')
  <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{asset('js/course_details/students/course_details.js')}}"></script>
@endsection()
