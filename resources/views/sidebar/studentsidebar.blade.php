<div class="sidebar" data-color="white" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="#" class="simple-text logo-mini">
          <div class="logo-image-small">
           
            <img src="{{ asset('themestyle/assets/img/logo-small.png')}}">
          </div>
        </a>
        <a href="#" class="simple-text logo-normal">{{ Auth::user()->first_name }}</a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="active ">
            <a href="{{url('home')}}">
              <i class="nc-icon nc-bank"></i>
              <p>Grade Details</p>
            </a>
          </li>

          <li>
            <a href="{{ url('coursedetails') }}">
              <i class="nc-icon nc-pin-3"></i>
              <p>Course Details</p>
            </a>  
          </li>
          <li>
            <a href="{{ url('studentuserprofile') }}">
              <i class="nc-icon nc-single-02"></i>
              <p>User Profile</p>
            </a>
          </li>
          
        </ul>
      </div>
    </div>