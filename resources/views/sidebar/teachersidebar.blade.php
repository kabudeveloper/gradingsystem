<div class="sidebar" data-color="white" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="#" class="simple-text logo-mini">
          <div class="logo-image-small">
           
            <img src="{{ asset('themestyle/assets/img/logo-small.png')}}">
          </div>
        </a>
        <a href="#" class="simple-text logo-normal">{{ Auth::user()->first_name }}</a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="active ">
            <a href="{{url('home')}}">
              <i class="nc-icon nc-bank"></i>
              <p>Attendance</p>
            </a>
          </li>
          <li id="class-record-header">
            <a href="#">
              <i class="nc-icon nc-diamond"></i>
               <p class="collapsed dropdown-toggle" data-toggle="collapse" data-target="#classRecordDropdown" aria-expanded="false" aria-controls="collapseThree"> Class Records </p>
             </a>
                <ul class="collapse" aria-labelledby="class-record-header" id="classRecordDropdown">
                  <li class="dropdown-item" >
                    <a href="{{ url('quizes') }}" >Quiz</a>
                  </li> 
                  <li class="dropdown-item">
                    <a href="{{ url('assignment') }}">Assignment</a>
                  </li>
                   <li class="dropdown-item">
                    <a href="{{ url('termexam') }}">Term Exam</a>
                  </li>
                  <li class="dropdown-item">
                    <a href="{{ url('attendancegrade') }}">Attendance</a>
                  </li>
                  
                </ul>
           
          </li>

          <!-- <li>
            <a href="{{ url('gradedetails') }}">
              <i class="nc-icon nc-pin-3"></i>
              <p>Grade Details</p>
            </a>
          </li> -->
           <li id="class-record-header">
            <a href="#">
              <i class="nc-icon nc-pin-3"></i>
               <p class="collapsed dropdown-toggle" data-toggle="collapse" data-target="#gradeDetailsDropdown" aria-expanded="false" aria-controls="collapseThree"> Grade Details </p>
             </a>
                <ul class="collapse" aria-labelledby="class-record-header" id="gradeDetailsDropdown">
                  <li class="dropdown-item" >
                    <a href="{{ url('gradedetails') }}" >Term Grades</a>
                  </li> 
                  <li class="dropdown-item" >
                    <a href="{{ url('semestergrades') }}" >Semester Grades</a>
                  </li>
                  <!-- <li class="dropdown-item" >
                    <a href="{{ url('gradedetails') }}" >Comput</a>
                  </li> -->

                </ul>
          </li>
          <li>
            <a href="{{ url('teachersubjects') }}">
              <i class="nc-icon nc-bell-55"></i>
              <p>My Subjects</p>
            </a>
          </li>
          <li>
            <a href="{{ url('teacheruserprofile') }}">
              <i class="nc-icon nc-single-02"></i>
              <p>User Profile</p>
            </a>
          </li>
          
        </ul>
      </div>
    </div>