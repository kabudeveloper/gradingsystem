<div class="sidebar" data-color="white" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="#" class="simple-text logo-mini">
          <div class="logo-image-small">
           
            <img src="{{ asset('themestyle/assets/img/logo-small.png')}}">
          </div>
        </a>
        <a href="#" class="simple-text logo-normal">
         {{ Auth::user()->first_name }}
          <!-- <div class="logo-image-big">
            <img src="../assets/img/logo-big.png">
          </div> -->
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="active ">
            <a href="{{url('home')}}">
              <i class="nc-icon nc-bank"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li id="maintenance-header">
            <a href="#">
              <i class="nc-icon nc-diamond"></i>
               <p class="collapsed dropdown-toggle" data-toggle="collapse" data-target="#maintenanceDropdown" aria-expanded="false" aria-controls="collapseThree"> Maintenance </p>
            </a>
                <ul class="collapse" aria-labelledby="maintenance-header" id="maintenanceDropdown">
                  <li class="dropdown-item">
                    <a href="{{ url('courses') }}">Courses</a>
                  </li> 
                  <li class="dropdown-item">
                    <a href="{{ url('students') }}">Students</a>
                  </li>
                   <li class="dropdown-item">
                    <a href="{{ url('teachers') }}">Teachers</a>
                  </li>
                   <li class="dropdown-item">
                    <a href="{{ url('sections') }}">Sets</a>
                  </li>
                  <li class="dropdown-item">
                    <a href="{{ url('subjects') }}">Subjects</a>
                  </li>
                  <li class="dropdown-item">
                    <a href="{{ url('schoolyear') }}">School Year</a>
                  </li>
                  <li class="dropdown-item">
                    <a href="{{ url('users') }}">Users</a>
                  </li>
                </ul>
            
          </li>
          <li>
            <a href="#">
              <i class="nc-icon nc-pin-3"></i>
              <p>Grade Details</p>
            </a>
          </li>
          <li>
            <a href="{{ url('adminuserprofile') }}">
              <i class="nc-icon nc-single-02"></i>
              <p>User Profile</p>
            </a>
          </li>
          <li>
            <a href="{{ url('reports') }}">
              <i class="nc-icon nc-paper"></i>
              <p>Reports</p>
            </a>
          </li>
        </ul>
      </div>
    </div>