@extends('layouts.teacher')
@section('title','Add Total Attendance')

  @section('content')
  <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Add Total Attendance</h5>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">
                    <form id="add-quiz-form" action="{{ url('attendancegrade/store') }}" method="POST">
                      @csrf
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Title</label>
                            <input type="text" name="title" class="form-control">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Course</label>
                            <select id="course" name="course_id" class="form-control" required>
                                <option value="0"></option>

                              @foreach($course as $c)
                                <option value="{{ $c->course_id }}"> {{ $c->course->code }} </option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Subject</label>
                            <select id="subject" name="subject_id" class="form-control" required>

                            </select>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Year</label>
                            <select id="year" name="year_id" class="form-control" required>
                            </select>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">School Year</label>
                            <select id="sc_year" name="sc_year" class="form-control" required>
                              @foreach($sc as $s)
                                <option value="{{ $s->id }}">{{ $s->school_year }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <button class="btn btn-primary btn-rounded" type="submit">Save</button>
                            <a href="{{ url('quizes') }}" class="btn btn-danger btn-round">Back</a>
                          </div> 
                        </div>

                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Perfect Score</label>
                            <input type="number" name="totalscore" class="form-control">
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Set</label>
                            <select id="section" name="section_id" class="form-control" required>
                              
                            </select>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Semester</label>
                            <select id="semester" name="_semester" class="form-control" required>
                            </select>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Term</label>
                            <select id="semester" name="term_id" class="form-control" required>
                              <option value="1">Prelim</option>
                              <option value="2">Mid Term</option>
                              <option value="3">Semi-Final Term</option>
                              <option value="4">Final Term</option>
                            </select>
                          </div>
                           
                        </div>
                        
                      </div>
                    </form>
                  </div>
                </div>

              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Attendance Score
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </div>
  @endsection

@section('custom_css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}">
@endsection()

@section('custom_js')
  <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{asset('js/attendance/attendance_score.js')}}"></script>
@endsection()
