@extends('layouts.teacher')
@section('title','Assignment')

  @section('content')
  <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Assignment</h5>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-6">
                    <form id="quiz-form" action="#">
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Course</label>
                        <select id="course" name="course" class="form-control" required>
                            <option value="0"></option>

                          @foreach($course as $c)
                            <option value="{{ $c->course_id }}"> {{ $c->course->code }} </option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Subject</label>
                        <select id="subject" name="subject" class="form-control" required>

                        </select>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Year</label>
                        <select id="year" name="year" class="form-control" required>
                          
                        </select>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Set</label>
                        <select id="section" name="section" class="form-control" required>
                          
                        </select>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Semester</label>
                        <select id="semester" name="semester" class="form-control" required>
                        </select>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">School Year</label>
                        <select id="sc_year" name="sc_year" class="form-control" required>
                          @foreach($sc as $s)
                            <option value="{{ $s->id }}">{{ $s->school_year }}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group">
                        <button class="btn btn-primary btn-rounded" type="submit">Go</button>
                      </div>
                    </form>
                  </div>
                </div> <br>
                <div class="row">
                  <div class="table-responsive">
                    <div class="col-lg-12">
                      <form id="score-form" action="#">
                        @csrf
                        <a href="assignment/create" class="btn btn-success btn-round">Add new</a>
                        
                        <table class="table" width="100%" id="assignment-class-table">
                          <thead class="text-primary">
                            <tr>
                              <th>Title</th>
                              <th>Date</th>
                              <th>Term</th>
                              <th>Total Score</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                        </table>
                        
                      </form>
                    </div>
                  </div>
                </div>

              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Assignment
                </div>
              </div>
            </div>
          </div>
        </div>
        @include('alerts.confirm')
      </div>
  @endsection

@section('custom_css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}">
@endsection()

@section('custom_js')
  <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{asset('js/assignment/ass.js')}}"></script>

@endsection()
