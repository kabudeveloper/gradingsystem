@extends('layouts.teacher')
@section('title','Edit Assignment')

  @section('content')
  <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Edit Assignment</h5>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-lg-12">
                    <form id="add-quiz-form" action="#" method="POST">
                      @csrf
                      @method('PATCH')
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Title</label>
                            <input type="text" name="title" class="form-control" value="{{ $quiz->title }}" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Course</label>
                            <input type="text" value="{{ $quiz->course->code }}" class="form-control" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Subject</label>
                            <input type="text" value="{{ $quiz->subject->name }}" class="form-control" readonly>
                            
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Year</label>
                            <input type="text" value="{{ $quiz->year->name }} Year" class="form-control" readonly>
                            
                          </div>
                        </div>

                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Perfect Score</label>
                            <input type="number" name="totalscore" class="form-control" value="{{ $quiz->totalscore }}" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Set</label>
                            <input type="text" class="form-control" value="{{ $quiz->section->code }}" readonly>
                            
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Semester</label>
                             <input type="text" class="form-control" value="{{ $quiz->_semester }}" readonly>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label">Term</label>
                            @if($quiz->term_id == 1)
                              <input type="text" class="form-control" value="Mid Term" readonly>
                            @else
                              <input type="text" class="form-control" value="Final Term" readonly>
                            @endif
                             
                           
                          </div>
                           
                        </div>
                        
                      </div>
                    </form>
                  </div>
                </div>

              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Assignment
                </div>
              </div>
            </div>

            <div class="card ">
             <div class="card-header ">
                <h4 class="card-title">Students</h4>
                <div class="card-header">
              </div>
            <div class="card-body ">
                <div class="table-responsive">
                  <div class="col-lg-12">
                    <form id="student-score-form" action="#">
                      <!--  <button type="button" class="btn btn-success btn-sm btn-round" data-toggle="modal" data-target="#uploadModal">Upload Quiz Score</button> -->
                      <input type="hidden" name="" id="assid" value="{{ $id }}">
                      <table class="table table-bordered table-striped" width="100%" id="assignment-details-table">
                        <thead class="text-primary">
                          <tr>
                            <th>Last Name</th>
                            <th>First Name</th>
                            <th>Score</th>
                          </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <!-- <td> -->
                            <td colspan="3"><button type="submit" class="btn btn-success btn-sm btn-round" style="float: right;">Save</button></td>
                          </tr>
                        </tfoot>
                      </table>
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Student
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
       
      </div>
  @endsection

@section('custom_css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}">
@endsection()

@section('custom_js')
  <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{asset('js/assignment/assignment.js')}}"></script>
@endsection()
