<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermExamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('term_exam', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->integer('course_id');
            $table->integer('subject_id');
            $table->integer('year_id');
            $table->integer('section_id');
            $table->integer('teacher_id');
            $table->integer('_semester');
            $table->integer('term_id');
            $table->integer('school_year_id');
            $table->integer('totalscore');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('term_exam');
    }
}
