<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectFinalGradeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subject_final_grade', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('grade_id');
            $table->integer('student_id');
            $table->decimal('subject_grade',8,4);
            $table->integer('subject_equivalent');
            $table->decimal('final_equivalent',2,1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subject_final_grade');
    }
}
