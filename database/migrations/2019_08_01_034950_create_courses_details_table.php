<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('course_id');
            $table->integer('subject_id');
            $table->integer('year_id');
            $table->integer('section_id');
            $table->integer('_semester');
            $table->integer('units');
            $table->float('term_exam',5,2)->nullable();
            $table->float('quiz',5,2)->nullable();
            $table->float('assignment',5,2)->nullable();
            $table->float('attendance',5,2)->nullable();
            $table->float('participation',5,2)->nullable();
            $table->integer('teacher_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses_details');
    }
}
