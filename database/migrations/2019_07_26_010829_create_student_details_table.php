<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->string('email');
            $table->integer('course_id');
            $table->integer('year_id');
            $table->integer('section_id');
            $table->integer('gender_id');
            $table->integer('school_year_id');
            $table->string('address');
            $table->string('number');
            $table->string('contact_person');
            $table->string('contact_number');
            $table->string('m_fname')->nullable();
            $table->string('m_lname')->nullable();
            $table->string('m_mname')->nullable();
            $table->string('m_suffix')->nullable();
            $table->string('f_fname')->nullable();
            $table->string('f_lname')->nullable();
            $table->string('f_mname')->nullable();
            $table->string('f_suffix')->nullable();
            $table->string('m_number')->nullable();
            $table->string('f_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students_details');
    }
}
