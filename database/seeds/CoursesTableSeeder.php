<?php

use Illuminate\Database\Seeder;
use App\Courses;
class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Courses::insert([
        	[
        		'code' => 'BSICT',
        	    'name' => 'Bachelor of Science in Information and Communication Technology',
                'major' => '',
        	],
        	[
        		'code' => 'BSIT-CT',
        		'name' => 'Bachelor of Science in Industrial Technology',
                'major' => 'Computer Technology',

        	],
        	[
        		'code' => 'BSA',
        		'name' => 'Bachelor of Science in Agriculture',
                'major' => 'Fishery',

        	]
        ]);
    }
}
