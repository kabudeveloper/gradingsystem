<?php

use App\Teachers;
use Illuminate\Database\Seeder;

class TeacherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Teachers::insert([
        	[
        		'first_name' => 'Isabella',
		        'last_name' => 'Rabanes',
		        'middle_name' => 'Geagonia',
		    ],

        ]);
    }
}
