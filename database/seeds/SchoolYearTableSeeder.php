<?php

use App\SchoolYear;
use Illuminate\Database\Seeder;

class SchoolYearTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SchoolYear::insert([
        	[
                'school_year' => '2018-2019'
        		
        	],
        	[
                'school_year' => '2019-2020'
        		
        	],
        	[
                'school_year' => '2020-2021'
        		
        	],
        	[
                'school_year' => '2021-2022'
        		
        	],
        	[
                'school_year' => '2022-2023'
        		
        	],
        ]);
    }
}
