<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	User::insert([
        	[
                'username' => 'kabu',
        		'email' => 'rodgeagonia@gmail.com',
        		'first_name' => 'kabu',
        		'middle_name' => 'kabu',
        		'last_name' => 'kabu',
	            'password' => Hash::make('kabu'),
	           	'position_id' => 1,
                'user_type_id' => 1,
                'teacher_id' => 0,
                'student_id' => 0,
	           	'parent_id' => 0,
        	],
            [
                'username' => 'bella',
                'email' => 'bellabells328@gmail.com',
                'first_name' => 'Isabella',
                'middle_name' => 'Geagonia',
                'last_name' => 'Rabanes',
                'password' => Hash::make('bella'),
                'position_id' => 4,
                'user_type_id' => 2,
                'teacher_id' => 1,
                'student_id' => 0,
                'parent_id' => 0,
            ],
        ]);
    }
}
