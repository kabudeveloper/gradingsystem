<?php

use App\CourseDetails;
use Illuminate\Database\Seeder;

class CourseDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CourseDetails::insert([
        	[
	    		'course_id' => 1,
		    	'subject_id' =>1,
		    	'year_id' => 1,
		        'section_id' =>1,
		        '_semester' => 1,
                'units' => 3,
		        'term_exam' => 40.00,
		        'quiz' => 30.00,
		        'assignment' => 10.00,
		        'attendance' => 20.00,
		        'participation' => 0.00,
		        'teacher_id' => 1,
        	],
            [
                'course_id' => 1,
                'subject_id' =>2,
                'year_id' => 1,
                'section_id' =>2,
                '_semester' => 1,
                'units' => 3,
                'term_exam' => 40.00,
                'quiz' => 30.00,
                'assignment' => 10.00,
                'attendance' => 20.00,
                'participation' => 0.00,
                'teacher_id' => 1,
            ],
            [
                'course_id' => 1,
                'subject_id' =>3,
                'year_id' => 1,
                'section_id' =>1,
                '_semester' => 1,
                'units' => 3,
                'term_exam' => 40.00,
                'quiz' => 30.00,
                'assignment' => 10.00,
                'attendance' => 20.00,
                'participation' => 0.00,
                'teacher_id' => 0,
            ],
            [
                'course_id' => 2,
                'subject_id' =>1,
                'year_id' => 1,
                'section_id' =>1,
                '_semester' => 1,
                'units' => 3,
                'term_exam' => 40.00,
                'quiz' => 30.00,
                'assignment' => 10.00,
                'attendance' => 20.00,
                'participation' => 0.00,
                'teacher_id' => 0,
            ],
            [
                'course_id' => 2,
                'subject_id' =>2,
                'year_id' => 1,
                'section_id' =>1,
                '_semester' => 1,
                'units' => 3,
                'term_exam' => 40.00,
                'quiz' => 30.00,
                'assignment' => 10.00,
                'attendance' => 20.00,
                'participation' => 0.00,
                'teacher_id' => 0,
            ],
            [
                'course_id' => 2,
                'subject_id' =>2,
                'year_id' => 1,
                'section_id' =>2,
                '_semester' => 1,
                'units' => 3,
                'term_exam' => 40.00,
                'quiz' => 30.00,
                'assignment' => 10.00,
                'attendance' => 20.00,
                'participation' => 0.00,
                'teacher_id' => 0,
            ],
        ]);
    }
}
