<?php

use Illuminate\Database\Seeder;
use App\Sections;
class SectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sections::insert([
        	[
        		'code' => '1B',
        	    'name' => 'First Year Block section',
        	],
        	[
        		'code' => '1A',
        		'name' => 'First Year Day section',
        	],
        	[
        		'code' => '2B',
        		'name' => 'Second Year Block Section',
        	],
        ]);
    }
}
