<?php

use Illuminate\Database\Seeder;
use App\Year;
class YearTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Year::insert([
        	[
        		'code' => '1st',
        		'name' => 'First Year'
        	],
        	[
        		'code' => '2nd',
        		'name' => 'Second Year'
        	],
        	[
        		'code' => '3rd',
        		'name' => 'Third Year'
        	],
        	[
        		'code' => '4th',
        		'name' => 'Fourth Year'
        	],
        	[
        		'code' => '5th',
        		'name' => 'Fifth Year'
        	],
        	[
        		'code' => '6th',
        		'name' => 'Sixth Year'
        	],
        	[
        		'code' => '7th',
        		'name' => 'Seventh Year'
        	],
        	[
        		'code' => '8th',
        		'name' => 'Eight Year'
        	],
        	[
        		'code' => '9th',
        		'name' => 'Ninth Year'
        	],
        	[
        		'code' => '10th',
        		'name' => 'Tenth Year'
        	],
        ]);
    }
}
