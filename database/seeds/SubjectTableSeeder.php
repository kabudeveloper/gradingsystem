<?php

use Illuminate\Database\Seeder;
use App\Subjects;
class SubjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Subjects::insert([
       		[
       			'code' => 'TECH111',
       			'name' => 'Digital Electronics',
            'units' => 5
       		],
       		[
       			'code' => 'TECH121',
       			'name' => 'Web Development',
            'units' => 5
       		],
       		[
       			'code' => 'PE111',
       			'name' => 'Contemporary  Dance',
            'units' => 1
       		],
       		[
       			'code' => 'FIL221',
       			'name' => 'Wika at Pag-basa',
            'units' => 2
       		],
       ]);
    }
}
