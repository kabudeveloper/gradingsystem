<?php

use App\Positions;
use Illuminate\Database\Seeder;

class PositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Positions::insert([
        	[
        		'code' => 'CD',
        		'name' => 'Campus Director'
        	],
        	[
        		'code' => 'ACD',
        		'name' => 'Assistant Campus Director'
        	],
        	[
        		'code' => 'ACCNTG',
        		'name' => 'Accounting Staff'
        	],
        	[
        		'code' => 'TCHR',
        		'name' => 'Teacher'
        	],
        	[
        		'code' => 'STNDT',
        		'name' => 'Student'
        	]
        ]);
    }
}
