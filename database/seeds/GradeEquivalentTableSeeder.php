<?php

use App\GradeEquivalent;
use Illuminate\Database\Seeder;

class GradeEquivalentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GradeEquivalent::insert([
        	[
        		'grade' => 95,
        		'equivalent' => 1.0,
        	],
        	[
        		'grade' => 94,
        		'equivalent' => 1.1,
        	],
        	[
        		'grade' => 93,
        		'equivalent' => 1.2
        	],
        	[
        		'grade' => 92,
        		'equivalent' => 1.3,
        	],
        	[
        		'grade' => 91,
        		'equivalent' => 1.4,
        	],
        	[
        		'grade' => 90,
        		'equivalent' => 1.5,
        	],
        	[
        		'grade' => 89,
        		'equivalent' => 1.6,
        	],
        	[
        		'grade' => 88,
        		'equivalent' => 1.7,
        	],
        	[
        		'grade' => 87,
        		'equivalent' => 1.8,
        	],
        	[
        		'grade' => 86,
        		'equivalent' => 1.9,
        	],
        	[
        		'grade' => 85,
        		'equivalent' => 2.0,
        	],
        	[
        		'grade' => 84,
        		'equivalent' => 2.1,
        	],
        	[
        		'grade' => 83,
        		'equivalent' => 2.2,
        	],
        	[
        		'grade' => 82,
        		'equivalent' => 2.3,
        	],
        	[
        		'grade' => 81,
        		'equivalent' => 2.4,
        	],
        	[
        		'grade' => 80,
        		'equivalent' => 2.5,
        	],
        	[
        		'grade' => 79,
        		'equivalent' => 2.6,
        	],
        	[
        		'grade' => 78,
        		'equivalent' => 2.7,
        	],
        	[
        		'grade' => 77,
        		'equivalent' => 2.8,
        	],
            [
                'grade' => 76,
                'equivalent' => 2.9,
            ],
        	[
        		'grade' => 75,
        		'equivalent' => 3.0,
        	],
            [
                'grade' => 74,
                'equivalent' => 3.1,
            ],
            [
                'grade' => 73,
                'equivalent' => 3.2,
            ],
            [
                'grade' => 72,
                'equivalent' => 3.3,
            ],
            [
                'grade' => 71,
                'equivalent' => 3.4,
            ],
            [
                'grade' => 70,
                'equivalent' => 3.5,
            ],
        	[
        		'grade' => 69,
        		'equivalent' => 3.6,
        	],
        	[
        		'grade' => 68,
        		'equivalent' => 3.7,
        	],
        	[
        		'grade' => 67,
        		'equivalent' => 3.8,
        	],
        	[
        		'grade' => 66,
        		'equivalent' => 3.9,
        	],
        	[
        		'grade' => 65,
        		'equivalent' => 4.0,
        	],
        	[
        		'grade' => 64,
        		'equivalent' => 4.1,
        	],
        	[
        		'grade' => 63,
        		'equivalent' => 4.2,
        	],
        	[
        		'grade' => 62,
        		'equivalent' => 4.3,
        	],
        	[
        		'grade' => 61,
        		'equivalent' => 4.4,
        	],
        	[
        		'grade' => 60,
        		'equivalent' => 4.5,
        	],
        	[
        		'grade' => 59,
        		'equivalent' => 4.6,
        	],
        	[
        		'grade' => 58,
        		'equivalent' => 4.7,
        	],
        	[
        		'grade' => 57,
        		'equivalent' => 4.8,
        	],
        	[
        		'grade' => 56,
        		'equivalent' => 4.9,
        	],
        	[
        		'grade' => 55,
        		'equivalent' => 5.0,
        	],
        	
        	

        ]);
    }
}
