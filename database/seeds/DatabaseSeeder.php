<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CoursesTableSeeder::class);
        $this->call(CourseDetailsTableSeeder::class);
        $this->call(SectionTableSeeder::class);
        $this->call(YearTableSeeder::class);
        $this->call(StudentTableSeeder::class);
        $this->call(StudentDetailsTableSeeder::class);
        $this->call(GenderTableSeeder::class);
        $this->call(SubjectTableSeeder::class);
        $this->call(PositionTableSeeder::class);
        $this->call(UserTypeTableSeeder::class);
        $this->call(TeacherTableSeeder::class);
        $this->call(GradeEquivalentTableSeeder::class);
        $this->call(SchoolYearTableSeeder::class);
    }
}
