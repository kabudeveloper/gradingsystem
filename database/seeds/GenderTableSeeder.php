<?php

use Illuminate\Database\Seeder;
use App\Genders;
class GenderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Genders::insert([
        	[
        		'code' => 'M',
        		'name' => 'Male',
        	],
        	[
        		'code' => 'F',
        		'name' => 'Female',
        	],
        	
        ]);
    }
}
