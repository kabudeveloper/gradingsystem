<?php

use Illuminate\Database\Seeder;
use App\Students;
class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Students::insert([
        	[
                'id_number' => 11101,
        		'first_name' => 'Rod',
        	    'last_name' => 'Geagonia',
        	    'middle_name' => 'Pantinople',
        	    'suffix' => '',
        	],
        	[
                'id_number' => 11102,
        		'first_name' => 'Kabu',
        	    'last_name' => 'Kabu',
        	    'middle_name' => 'Kabu',
        	    'suffix' => 'III',
        	],
        	[
                'id_number' => 11103,
        		'first_name' => 'SonRhey',
        	    'last_name' => 'Deparin',
        	    'middle_name' => 'Sevilla',
        	    'suffix' => '',
        	],
        	[
                'id_number' => 11104,
        		'first_name' => 'Terence',
        	    'last_name' => 'Shu',
        	    'middle_name' => 'Amatong',
        	    'suffix' => '',
        	],
            [
                'id_number' => 11105,
                'first_name' => 'Thania',
                'last_name' => 'Geagonia',
                'middle_name' => 'M.',
                'suffix' => '',
            ],

        ]);
    }
}
