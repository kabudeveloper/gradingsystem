<?php

use App\UserTypes;
use Illuminate\Database\Seeder;

class UserTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserTypes::insert([
        	[
        		'name' => 'Admin',
        	],
        	[
        		'name' => 'Teachers',
        	],
        	[
        		'name' => 'Students',
        	],
        	[
        		'name' => 'Parents',  
        	]
        ]);
    }
}
