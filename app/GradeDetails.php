<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GradeDetails extends Model
{
   protected $table = 'grade_details';
   protected $fillable = [
   		'grade_id',
   	 	'student_id',
   	 	'category',
        'grade',
   ];
}
