<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $table = 'attendance';
    protected $fillable = [
    	'student_id',
    	'subject_id',
        'year_id',
        '_semester',
    	'term_id',
    	'date',
    	'_present',
        'school_year_id',
    	'in',
    	'out',
    ];
}
