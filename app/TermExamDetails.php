<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TermExamDetails extends Model
{
    protected $table = 'term_exam_details';
    protected $fillable = [
    				'term_id',
		            'student_id',
		            'score',
    		];
}
