<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Verification extends Model
{
    protected $table = 'verification';
    protected $fillable = [
    	'user_id',
    	'date',
    	'code',
    	'_verified',
    ];
}
