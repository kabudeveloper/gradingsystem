<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SemesterGradeDetails extends Model
{
    protected $table = 'semester_grade_details';
    protected $fillable = [
    	'sem_grade_id',
        'student_id',
        'semester_grade',
    ];
}
