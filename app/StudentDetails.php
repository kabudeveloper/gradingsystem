<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentDetails extends Model
{
    protected $table = 'student_details'; 
    protected $fillable = [
    	'student_id',
        'email',
        'course_id',
        'year_id',
        'section_id',
        'gender_id',
        'school_year_id',
        'address',
        'number',
        'contact_person',
        'contact_number',
        'm_fname',
        'm_lname',
        'm_mname',
        'm_suffix',
        'f_fname',
        'f_lname',
        'f_mname',
        'f_suffix',
        'f_number',
        'm_number'
    ];
    public function student(){
        return $this->belongsTo(Students::class,'student_id');
    }
    public function course(){
     return $this->belongsTo(Courses::class,'course_id');
    }
    public function year(){
        return $this->belongsTo(Year::class,'year_id');
    }
    public function section(){
        return $this->belongsTo(Sections::class,'section_id');

    }
    public function gender(){
        return $this->belongsTo(Genders::class,'gender_id');

    }
}
