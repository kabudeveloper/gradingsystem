<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjectFinalGrade extends Model
{
    protected $table = 'subject_final_grade';
   	protected $fillable = [
   		'grade_id',
   	 	'student_id',
   	 	'subject_grade',
   	 	'subject_equivalent',
   	 	'final_equivalent',
   	 	
   	];
   	public function grade(){
   		return $this->belongsTo(Grades::class,'grade_id');
   	}
}
