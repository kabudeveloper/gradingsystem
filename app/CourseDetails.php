<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseDetails extends Model
{
    protected $table = 'courses_details';
    protected $fillable = [
        'course_id',
    	'subject_id',
    	'year_id',
        'section_id',
        '_semester',
        'units',
        'term_exam',
        'quiz',
        'assignment',
        'attendance',
        'participation',
        'teacher_id',
        
    ];
    public function course(){
        return $this->belongsTo(Courses::class,'course_id');
    }
    public function subject(){
    	return $this->belongsTo(Subjects::class,'subject_id');
    }
    public function year(){
    	return $this->belongsTo(Year::class,'year_id');
    }
     public function section(){
        return $this->belongsTo(Sections::class,'section_id');
    }
    public function teacher(){
        return $this->belongsTo(Teachers::class,'teacher_id');
    }

}
