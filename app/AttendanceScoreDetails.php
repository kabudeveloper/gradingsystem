<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceScoreDetails extends Model
{
    protected $table = 'attendance_score_details';
    protected $fillable = [
    				'attendance_score_id',
		            'student_id',
		            'score',
    		];
}
