<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignmentDetails extends Model
{
    protected $table = 'assignment_details';
    protected $fillable = [
    				'assignment_id',
		            'student_id',
		            'score',
    		];
}
