<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    protected $table = 'students'; 
    protected $fillable = [
        'id_number',
        'first_name',
        'last_name',
        'middle_name',
        'suffix',
       
    ];
    // public function client(){
    // 	return $this->hasOne(Client::class);
    // }
    
}
