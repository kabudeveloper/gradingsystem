<?php

namespace App\Http\Middleware;

use App\Verification;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userid = Auth::user()->id;
        $verify = Verification::where('user_id',$userid)->latest('id')->first(); 
     
        if (is_null($verify)) {
            abort(500, 'Internal Error');
        }else if ($verify->_verified == 0) {
            abort(402, 'Loin not verified'); 
        }
        
        return $next($request);
    }
}
