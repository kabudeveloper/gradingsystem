<?php

namespace App\Http\Controllers;

use App\CourseDetails;
use App\StudentDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\Datatables;

class CourseDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(1);

        $student_id = Auth::user()->student_id;
        $user_type = Auth::user()->user_type_id;
         if ($user_type == 3) {
            $course = StudentDetails::where('student_id',$student_id)->first();
            return view('course_details.students.index',compact('course'));
        }else if($user_type == 4){
            $course = StudentDetails::where('student_id',$student_id)->first();
            return view('course_details.students.index',compact('course'));
        }else{
           abort(416,"Forbidden Access!"); 
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getCourseSubjects(){
        $student_id = Auth::user()->student_id;
        $getDetails = StudentDetails::where('student_id',$student_id)->first();
        $getSubjects = CourseDetails::with('subject')->where('course_id',$getDetails->course_id)
            ->where('section_id',$getDetails->section_id)
            ->where('year_id',$getDetails->year_id);
        return Datatables::of($getSubjects)
            ->editColumn('term_exam',function($sub){
                
                return $sub->term_exam."%";
            })
            ->editColumn('quiz',function($sub){
                return $sub->quiz."%";
            })
            ->editColumn('assignment',function($sub){
                return $sub->assignment."%";
            })
            ->editColumn('attendance',function($sub){
                return $sub->attendance."%";
            })
            ->editColumn('participation',function($sub){
                return $sub->participation."%";
            })
            ->make(true);
    }
}
