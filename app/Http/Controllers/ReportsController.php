<?php

namespace App\Http\Controllers;

use App\Courses;
use App\Year;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\Datatables;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $course = Courses::all();
        $year = Year::all();

        return view('reports.index',compact('course','year'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getall(){
        $subject = DB::table('courses_details as cd')
            ->select('s.id as subjectid','s.name as subjectname')
            ->leftjoin('subjects as s','cd.subject_id','=','s.id')
            ->groupby('s.id','s.name')
            ->get();
        $section = DB::table('courses_details as cd')
            ->select('s.id as sectionid','s.code as sectioncode')
            ->leftjoin('sections as s','cd.section_id','=','s.id')
            ->groupby('s.id','s.code')
            ->get();
        return [$subject,$section];
    }
    public function getwhere($id){
        $subject = DB::table('courses_details as cd')
            ->select('s.id as subjectid','s.name as subjectname')
            ->leftjoin('subjects as s','cd.subject_id','=','s.id')
            ->where('cd.course_id',$id)
            ->groupby('s.id','s.name')
            ->get();
        $section = DB::table('courses_details as cd')
            ->select('s.id as sectionid','s.code as sectioncode')
            ->leftjoin('sections as s','cd.section_id','=','s.id')
            ->where('cd.course_id',$id)
            ->groupby('s.id','s.code')
            ->get();
        return [$subject,$section];
    }
    public function filterreport ($course,$subject,$year,$section,$sem){
        $data = DB::table('semester_grade as sg')
            ->select('s.first_name as name','s.last_name','c.code as course','sub.name as subject','y.code as year','se.code as section','_semester')
            ->leftjoin('semester_grade_details as sgd','sg.id','=','sgd.sem_grade_id')
            ->leftjoin('students as s','sgd.student_id','=','s.id')
            ->leftjoin('courses as c','sg.course_id','=','c.id')
            ->leftjoin('subjects as sub','sg.subject_id','=','sub.id')
            ->leftjoin('years as y','sg.year_id','=','y.id')
            ->leftjoin('sections as se','sg.section_id','=','se.id');
        if($course !=0) $data->where('sg.course_id',$course);
        if ($subject != 0)$data->where('sg.subject_id',$subject);
        if ($year != 0)$data->where('sg.year_id',$year);
        if ($section != 0)$data->where('sg.section_id',$section);
        if ($sem != 0)$data->where('sg._semester',$sem);
        $datas = $data;
        return Datatables::of($datas)
            ->editColumn('name',function($datas){
                return $datas->last_name." ".$datas->name;
            })

            ->editColumn('_semester',function($datas){
                $sem = ($datas->_semester == 1) ? "1st" : "2nd";
                return $sem;
            })            
            ->make(true);

    }
    public function generatenow (Request $request){
        $data = DB::table('semester_grade as sg')
            ->select('s.first_name as name','s.last_name','c.code as course','sub.name as subject','y.code as year','se.code as section','_semester')
            ->leftjoin('semester_grade_details as sgd','sg.id','=','sgd.sem_grade_id')
            ->leftjoin('students as s','sgd.student_id','=','s.id')
            ->leftjoin('courses as c','sg.course_id','=','c.id')
            ->leftjoin('subjects as sub','sg.subject_id','=','sub.id')
            ->leftjoin('years as y','sg.year_id','=','y.id')
            ->leftjoin('sections as se','sg.section_id','=','se.id');
        if($request->course !=0) $data->where('sg.course_id',$request->course);
        if ($request->subject != 0)$data->where('sg.subject_id',$request->subject);
        if ($request->year != 0)$data->where('sg.year_id',$request->year);
        if ($request->section != 0)$data->where('sg.section_id',$request->section);
        if ($request->sem != 0)$data->where('sg._semester',$request->sem);
        $datas = $data->get();
            
            // $pdf = PDF::loadView('pdfview');
            //     return $pdf->download('pdfview.pdf');
        $pdf = PDF::loadView('reports.generate', compact('datas'));
        return $pdf->download('report.pdf');
       

    }
    
}
