<?php

namespace App\Http\Controllers;

use App\CourseDetails;
use App\Subjects;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\Datatables;

class SubjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('subjects.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subjects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'code' =>'required|unique:subjects',
            'name' =>'required',
            'units' =>'required',
        ]);

        $subject = new Subjects($request->all());
        $subject->created_at = Carbon::now();
        $subject->save();

        return redirect('subjects/'.$subject->id.'/edit')->with('message','New Subject has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subject = Subjects::find($id);
        return view('subjects.view',compact('subject'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subject = Subjects::find($id);
        return view('subjects.edit',compact('subject'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request,[
            'code' =>'required',
            'name' =>'required',
            'units' =>'required',
        ]);
        // $find = Subjects::where('code',$request->code)->first();
        // if (!is_null($find)) {
        //     return redirect('subjects/'.$find->id.'/edit')->with('error_message','Code has been taken already');
        // }else{
            $subject = Subjects::find($id);
            $subject->code = $request->code;
            $subject->name = $request->name;
            $subject->units = $request->units;
            $subject->updated_at = Carbon::now();
            $subject->update();

            return redirect('subjects/'.$subject->id.'/edit')->with('message','New Subject has been updated successfully');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subject = Subjects::find($id);
        $subject->delete();
    }
    public function teachersubjects(){
        // dd(1);
        return view('teachers.subjects.index');
    }
    public function getteachersubjects(){
        $teacherid = Auth::user()->teacher_id;
        $details = CourseDetails::with('course')->with('subject')->with('year')->with('section')->where('teacher_id',$teacherid);
        return Datatables::of($details)
            ->editColumn('_semester',function($details){
                $sem = ($details->_semester == 1) ? "1st" : "2nd";
                return $sem;
            })
            ->make(true);
    }
    public function getSubjects(){
        $subject = Subjects::all();
        return Datatables::of($subject)
            ->addColumn('action',function($subject){
                return'<a class="btn btn-round btn-success btn-sm" href="subjects/'.$subject->id.'"><i class="fa fa-eye"></i>View</a> <a class="btn btn-round btn-info btn-sm" href="subjects/'.$subject->id.'/edit"><i class="fa fa-edit"></i>Edit</a> <a class="btn btn-round btn-danger btn-sm" href="#" id="delete" data-id="'.$subject->id.'"><i class="fa fa-trash"></i>Delete</a>';
            })
            ->make(true);
    }
}
