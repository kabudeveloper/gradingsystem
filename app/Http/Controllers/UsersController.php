<?php

namespace App\Http\Controllers;

use App\Positions;
use App\StudentDetails;
use App\Teachers;
use App\User;
use App\UserTypes;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\Datatables;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(1);
        return view('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $position = Positions::all();
        $usertype = UserTypes::all();

        return view('users.create',compact('position','usertype'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'username' => 'required|unique:users',
            'email' => 'required',
            'password' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'middle_name' => 'required',
            'position_id' => 'required',
            'user_type_id' => 'required',

        ]);
        // dd($request->all());
        $user = new User;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->middle_name = $request->middle_name;
        $user->position_id = $request->position_id;
        $user->user_type_id = $request->user_type_id;
        $user->password = Hash::make($request->password);
        $user->created_at = Carbon::now();
        $user->save();

        return redirect('users/'.$user->id.'/edit')->with('message','New user has been successfuly added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user = User::find($id);

        return view('users.view',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $position = Positions::all();
        $usertype = UserTypes::all();
        
        return view('users.edit',compact('user','position','usertype'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'password' => 'required',
            'email' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'middle_name' => 'required',
            'position_id' => 'required',
            'user_type_id' => 'required',

        ]);
        // dd($request->all());
        $user = User::find($id);
        $user->email = $request->email;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->middle_name = $request->middle_name;
        $user->position_id = $request->position_id;
        $user->user_type_id = $request->user_type_id;
        $user->password = Hash::make($request->password);
        $user->updated_at = Carbon::now();
        $user->update();

        return redirect('users/'.$user->id.'/edit')->with('message','New user has been successfuly updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
    }
    public function getUsers(){
        $users = User::with('position')->with('user_type');
        return Datatables::of($users)
            ->addColumn('action',function($user){
                return'<a class="btn btn-rounded btn-success btn-sm" href="users/'.$user->id.'"><i class="fa fa-eye"></i>View</a> <a class="btn btn-rounded btn-info btn-sm" href="users/'.$user->id.'/edit"><i class="fa fa-edit"></i>Edit</a> <a class="btn btn-rounded btn-danger btn-sm" href="#" id="delete" data-id="'.$user->id.'"><i class="fa fa-trash"></i>Delete</a>';
            })
            ->editColumn('position.name',function($user){
                $name = ($user->position_id == 0) ? "Parent" : $user->position->name;
                return $name;
            })
            ->make(true);

    }
    public function studentsProfile(){
        $userid = Auth::user()->id;
        $username = Auth::user()->username;
        $studentid = Auth::user()->student_id;
        $user_type = Auth::user()->user_type_id;
         if ($user_type == 3) {
            $userDetails = StudentDetails::where('student_id',$studentid)->first();
            return view('users.students.index',compact('userid','studentid','userDetails','username'));
         }else if ($user_type == 4) {
            $user = Auth::user();
            $userDetails = StudentDetails::where('student_id',$studentid)->first();return view('users.parents.index',compact('userid','user','studentid','userDetails','username'));
         }
        
        
    }
    public function teachersProfile(){
        $userid = Auth::user()->id;
        $username = Auth::user()->username;
        $teacherid = Auth::user()->teacher_id;
        $userDetails = User::where('teacher_id',$teacherid)->first();
        $contact1 = User::where('position_id',1)->first();
        $contact2 = User::where('position_id',2)->first();
       
            return view('users.teachers.index',compact('userid','teacherid','userDetails','username','contact1','contact2'));
        
        
    }
    public function adminProfile(){
        $usertype = Auth::user()->user_type_id;
        $userid = Auth::user()->id;
        $username = Auth::user()->username;
        $teacherid = Auth::user()->teacher_id;
        $userDetails = Auth::user();
        if ($usertype != 1) {
            abort(411,"Forbidden Access");
        }
       
            return view('users.admin.index',compact('userid','teacherid','userDetails','username'));
    
    }
    public function  updateAdminProfile(Request $request){
        $userid = $request->id;
        $user = User::find($userid);
        $user->username = $request->username;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->update();

        return redirect('adminuserprofile')->with('message', 'Admin profile has been updated successfuly');
        
    
    }
   
}
