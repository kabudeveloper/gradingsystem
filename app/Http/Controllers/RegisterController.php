<?php

namespace App\Http\Controllers;

use App\Mail\VerificationMail;
use App\Positions;
use App\StudentDetails;
use App\Students;
use App\User;
use App\UserTypes;
use App\Verification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
     protected function registerStudents(Request $request){
        $this->validate($request, [
        	
        	'username' => 'required|unique:users',
            'id_number' => 'required',
            'password' => 'required',

        ]);
        $position_id = Positions::select('id')->where('name','Student')->orWhere('name','Students')->first();
        $user_type_id = UserTypes::select('id')->where('name','Students')->orWhere('name','Student')->first();
        
        $findStudent = Students::where('id_number',$request->id_number)->first();

        if (!is_null($findStudent)) {
            $findStudentDetails = StudentDetails::where('student_id',$findStudent->id)->first();
            $user = new User;
            $user->username = $request->username;
            $user->email = $findStudentDetails->email;
            $user->first_name = $findStudent->first_name;
            $user->middle_name = $findStudent->middle_name;
            $user->last_name = $findStudent->last_name;
            $user->password = Hash::make($request->password);
            $user->position_id = $position_id->id;
            $user->user_type_id = $user_type_id->id;
            $user->student_id = $findStudent->id;
            $user->created_at = Carbon::now();
            $user->save();
        }else{
            return redirect('/register')->with('error_message','ID number not registered!');
        }
        // $addstudent = new Students; 
        // $addstudent->id_number = $request->id_number;
        // $addstudent->first_name = $request->first_name;
        // $addstudent->last_name = $request->last_name;
        // $addstudent->middle_name = $request->middle_name;
        // $addstudent->suffix = $request->suffix;
        // $addstudent->save();
        
        // $addstudentdetails = new StudentDetails;
        // $addstudentdetails->student_id = $addstudent->id;
        // $addstudentdetails->course_id = $request->course;
        // $addstudentdetails->year_id = $request->year;
        // $addstudentdetails->section_id = $request->section;
        // $addstudentdetails->gender_id = $request->gender;
        // $addstudentdetails->school_year_id = $request->sc_id;
        // $addstudentdetails->address = $request->address;
        // $addstudentdetails->number = $request->contact_number;
        // $addstudentdetails->contact_person = $request->contact_person;
        // $addstudentdetails->contact_number = $request->contact_person_number;	
        // $addstudentdetails->save();

        

        // $array = [1, 2, 3, 4, 5, 6,7,8,9,0];
        // $array1 = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
        // $array2 = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        // $array3 = [1, 2, 3, 4, 5, 6,7,8,9,0];
        // $array4 = [1, 2, 3, 4, 5, 6,7,8,9,0];
        // $array5 = [1, 2, 3, 4, 5, 6,7,8,9,0];

        // $random = Arr::random($array);
        // $random1 = Arr::random($array1);
        // $random2 = Arr::random($array2);
        // $random3 = Arr::random($array3);
        // $random4 = Arr::random($array4);
        // $random5 = Arr::random($array5);
        // $code = "S-".$random."".$random1."".$random2."".$random3."".$random4."".$random5;
        // $data = ["user"=>$user->first_name." ".$user->last_name, "code"=>$code];;

        // Mail::to($user->email)->send(new VerificationMail($data));
        
        // $add = new Verification;
        // $add->user_id = $user->id;
        // $add->date = Carbon::today();
        // $add->code = $code;
        // $add->_verified = 0;
        // $add->save();

        // return view('auth.verify',compact('user'));
        return redirect('login');
	}            

    
}
