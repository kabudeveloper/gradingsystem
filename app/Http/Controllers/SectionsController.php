<?php

namespace App\Http\Controllers;

use Yajra\DataTables\Facades\Datatables;
use Illuminate\Http\Request;
use App\Sections;
use Carbon\Carbon;

class SectionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sections.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sections.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'code' => 'required|unique:sections',
            'name' => 'required|unique:sections',
        ]);
        $section = new Sections($request->all());
        $section->created_at = Carbon::now();
        $section->save();
        return redirect('sections/'.$section->id.'/edit')->with('message','New Section has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $section = Sections::find($id);
        return view('sections.view',compact('section'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $section = Sections::find($id);
        return view('sections.edit',compact('section','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'code' => 'required',
            'name' => 'required',
        ]);
        $section = Sections::find($id);
        $section->code = $request->code;
        $section->name = $request->name;
        $section->updated_at = Carbon::now();
        $section->save();
        return redirect('sections/'.$id.'/edit')->with('message','New Section has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $section = Sections::find($id);
        $section->delete();
    }

    public function getSections(){

        $section = Sections::all();
        return Datatables::of($section)
            ->addColumn('action',function($section){
                return '<a class="btn btn-rounded btn-success btn-sm" href="sections/'.$section->id.'"><i class="fa fa-eye"></i>View</a> <a class="btn btn-rounded btn-info btn-sm" href="sections/'.$section->id.'/edit"><i class="fa fa-edit"></i>Edit</a> <a class="btn btn-rounded btn-danger btn-sm" href="#" id="delete" data-id="'.$section->id.'" data-name="'.$section->name.'"><i class="fa fa-trash"></i>Delete</a>';
            })
            ->make(true);
    }
}
