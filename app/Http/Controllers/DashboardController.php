<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\CourseDetails;
use App\SchoolYear;
use App\Sections;
use App\StudentDetails;
use App\Subjects;
use App\Year;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\Datatables;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
        $usertype = Auth::user()->user_type_id;
        $user = Auth::user();
        // dd($user);   
        if ($usertype == 1) {
            // $input = [0=> 5, 1=>7, 2=>127 ];
            // $count =0;
            // $text=[];
            // $addw=0;
            // for ($i=0; $i < count($input); $i++) { 
            //     $input_weight = $input[$i];
            //     $s= [];
            //     $gnw=[];
            //     $b=0;
            //     $sbs=0;
            //     for ($a=1; $a <=$input[$i] ; $a++) {

            //         if ($a==1) {
            //             $s[$a]=2*$a;
            //         }else{
            //             $b=0;
            //             $b=$a-1;
            //             // $a++;
            //             if ($b==0) {
            //                $b=1;
            //             }else{
            //                 $b=$b;
            //             }
            //             $nw=$s[$b]*2;
            //             $gnw[$a]=$nw;
            //             $s[$a]=$nw;
            //         }
            //         $addw=1+$s[$a];

            //         // dd($addw);
            //         if ($addw==$input_weight) {
            //             $text[$a]=$addw;
            //             echo "1 ".$text[$a]."<br>";
            //             break;
            //         }elseif ($addw>$input_weight ) {
            //             $sbs=$addw-$input_weight;

            //             $text[$a]=$gnw[$a]+$sbs;
            //             echo "1 ".$text[$a]."<br>";
            //             break;

            //         }
                    
                    
            //     }

            // }
            // DEFGHIJKLMNOPQRSTUVWXYZABC
            return view('dashboard.index');

        }else if ($usertype == 2) {
            
            $course = CourseDetails::select('course_id')
                ->with('course')
                ->where('teacher_id',$user->teacher_id)
                ->groupby('course_id')
                ->get();
            $sc = SchoolYear::all();

            return view('dashboard.teacher',compact('course','sc'));
        }else if ($usertype == 3) {
            $student = StudentDetails::where('student_id',Auth::user()->student_id)->first();
            // dd($student);
            return view('dashboard.student',compact('student'));
        }else{
            $student = StudentDetails::where('student_id',Auth::user()->student_id)->first();
            return view('dashboard.parent',compact('student'));

        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getSubjects($courseid){
        $subjects = [];
        $count = 0;
        $teacherid = Auth::user()->teacher_id;
        $courseDetails = CourseDetails::select('subject_id')
            ->where('course_id',$courseid)
            ->where('teacher_id',$teacherid)
            ->groupby('subject_id')
            ->get();

        foreach ($courseDetails as $cd) {

            $subjectDetails = Subjects::find($cd->subject_id);
            $subjects[$count] = ["id"=>$cd->subject_id, "name"=>$subjectDetails->name, "courseid"=>$courseid];
            $count++;

        }
        return $subjects;
    }
    public function getYear($courseid,$subjectid){
        $year = [];
        $count = 0;
        $teacherid = Auth::user()->teacher_id;
        $courseDetails = CourseDetails::select('year_id')
            ->where('course_id',$courseid)
            ->where('subject_id',$subjectid)
            ->where('teacher_id',$teacherid)
            ->groupby('year_id')
            ->get();
        foreach ($courseDetails as $cd) {

            $yearDetails = Year::find($cd->year_id);
            $year[$count] = ["id"=>$cd->year_id, "name"=>$yearDetails->code." Year"," courseid"=>$courseid, "subjectid"=>$subjectid];
            $count++;

        }
        return $year;
    }
    public function getSection($courseid,$subjectid,$yearid){
        
        $section = [];
        $count = 0;
        $teacherid = Auth::user()->teacher_id;
        $courseDetails = CourseDetails::select('courses_details.section_id')
            ->join('student_details as sd','courses_details.section_id','=','sd.section_id')
            ->where('sd.course_id',$courseid)
            ->where('courses_details.subject_id',$subjectid)
            ->where('courses_details.teacher_id',$teacherid)
            ->where('courses_details.year_id',$yearid)
            ->groupby('courses_details.section_id')
            ->get();

        foreach ($courseDetails as $cd) {

            $sectionDetails = Sections::find($cd->section_id);
            $section[$count] = ["id"=>$cd->section_id, "name"=>$sectionDetails->code," courseid"=>$courseid, "subjectid"=>$subjectid, "yearid"=>$yearid];
            $count++;

        }
        return $section;
    }
    
    public function getClasses($courseid,$subjectid,$yearid,$sectionid,$semesterid,$schoolyearid){
        $teacherid = Auth::user()->teacher_id;
        $class = DB::table('student_details as sd')
            ->select('sd.student_id','s.first_name','s.last_name','s.middle_name')
            ->join('courses as c','sd.course_id','=','c.id')
            ->join('courses_details as cd','c.id','=','cd.course_id')
            ->join('students as s','sd.student_id','s.id')
            ->where('sd.course_id',$courseid)
            ->where('cd.subject_id',$subjectid)
            ->where('sd.year_id',$yearid)
            ->where('sd.section_id',$sectionid)
            ->where('cd._semester',$semesterid)
            ->where('sd.school_year_id',$schoolyearid)
            ->where('cd.teacher_id',$teacherid)
            ->groupby('sd.student_id','s.first_name','s.last_name','s.middle_name');
            
        return Datatables::of($class)
            ->addColumn('action',function($class)use($semesterid,$subjectid){
                $date = Carbon::today();
                $attendance = Attendance::where('student_id',$class->student_id)
                    ->where('subject_id',$subjectid)
                    ->where('_present',1)
                    ->where('date',$date)
                    ->first();
                $attendance1 = Attendance::where('student_id',$class->student_id)
                    ->where('subject_id',$subjectid)
                    ->where('_present',0)
                    ->where('date',$date)
                    ->first();

                    if (!is_null($attendance)) {
                        if (!is_null($attendance->out)) {
                            return '<a class="btn btn-round btn-success btn-sm p'.$class->student_id.' disabled" href="#" data-id="'.$class->student_id.'" id="present" data-sec="'.$semesterid.'"><i class="fa fa-edit"></i>Present</a> <a class="btn btn-round btn-danger btn-sm p'.$class->student_id.'" href="#" data-id="'.$class->student_id.'" id="absent" data-sec="'.$semesterid.'"><i class="fa fa-edit"></i>Absent</a> <a class="btn btn-round btn-warning btn-sm p'.$class->student_id.' disabled" href="#" data-id="'.$class->student_id.'" id="timeout" data-sec="'.$semesterid.'"><i class="fa fa-edit"></i>Time Out</a>';
                        }else{
                            return '<a class="btn btn-round btn-success btn-sm p'.$class->student_id.' disabled" href="#" data-id="'.$class->student_id.'" id="present" data-sec="'.$semesterid.'"><i class="fa fa-edit"></i>Present</a> <a class="btn btn-round btn-danger btn-sm p'.$class->student_id.'" href="#" data-id="'.$class->student_id.'" id="absent" data-sec="'.$semesterid.'"><i class="fa fa-edit"></i>Absent</a> <a class="btn btn-round btn-warning btn-sm p'.$class->student_id.'" href="#" data-id="'.$class->student_id.'" id="timeout" data-sec="'.$semesterid.'"><i class="fa fa-edit"></i>Time Out</a>';
                        }
                        
                    }else{
                        if (!is_null($attendance1)) {
                            return '<a class="btn btn-round btn-success btn-sm p'.$class->student_id.'" href="#" data-id="'.$class->student_id.'" id="present" data-sec="'.$semesterid.'"><i class="fa fa-edit"></i>Present</a> <a class="btn btn-round btn-danger btn-sm p'.$class->student_id.' disabled" href="#" data-id="'.$class->student_id.'" id="absent" data-sec="'.$semesterid.'"><i class="fa fa-edit"></i>Absent</a> <a class="btn btn-round btn-warning btn-sm p'.$class->student_id.' disabled" href="#" data-id="'.$class->student_id.'" id="timeout" data-sec="'.$semesterid.'"><i class="fa fa-edit"></i>Time Out</a>';
                        }else{
                            return '<a class="btn btn-round btn-success btn-sm p'.$class->student_id.'" href="#" data-id="'.$class->student_id.'" id="present" data-sec="'.$semesterid.'"><i class="fa fa-edit"></i>Present</a> <a class="btn btn-round btn-danger btn-sm p'.$class->student_id.'" href="#" data-id="'.$class->student_id.'" id="absent" data-sec="'.$semesterid.'"><i class="fa fa-edit"></i>Absent</a> <a class="btn btn-round btn-warning btn-sm p'.$class->student_id.' disabled" href="#" data-id="'.$class->student_id.'" id="timeout" data-sec="'.$semesterid.'"><i class="fa fa-edit"></i>Time Out</a>';
                        }
                    }
            })
            ->make(true);


    }

}
