<?php

namespace App\Http\Controllers;

use App\CourseDetails;
use App\Positions;
use App\Teachers;
use App\User;
use App\UserTypes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\Datatables;

class TeachersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('teachers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('teachers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'first_name' => 'required',
            'last_name' => 'required',
            'middle_name' => 'required',
        ]);
        $teacher = new Teachers($request->all());
        $teacher->created_at = Carbon::now();
        $teacher->save();

        return redirect('teachers/'.$teacher->id.'/edit')->with('message','New Teacher has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = Teachers::find($id);
        $tr = 0;
        $find = User::where('teacher_id',$id)->first();
        if (!is_null($find)) {
            $tr = 1;
        }else{
            $tr = 0;
        }
        return view('teachers.edit',compact('teacher','tr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'first_name' => 'required',
            'last_name' => 'required',
            'middle_name' => 'required',
        ]);
        $teacher = Teachers::find($id);
        $teacher->first_name = $request->first_name;
        $teacher->last_name = $request->last_name;
        $teacher->middle_name = $request->middle_name;
        $teacher->updated_at = Carbon::now();
        $teacher->update();

        return redirect('teachers/'.$id.'/edit')->with('message','New Teacher has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function createAccount($teacherid){
        $position = Positions::all();
        
        return view('teachers.account.create',compact('position','teacherid','tr'));
    }
    public function storeAccount(Request $request,$teacherid){
        // $position = Positions::all();
        $this->validate($request,[
            'username' => 'required|unique:users',
            'password' => 'required',
            'email' => 'required',
        ]);
        $find = User::where('teacher_id',$teacherid)->first();
        if (!is_null($find)) {
            abort(411,'Sorry this teacher kay naa nay account!');
        }else{

            $teacher = Teachers::find($teacherid);
            $user = new User;
            $user->username = $request->username;
            $user->email = $request->email;
            $user->first_name = $teacher->first_name;
            $user->middle_name = $teacher->middle_name;
            $user->last_name = $teacher->last_name;
            $user->password = Hash::make($request->password);
            $user->position_id = $request->position_id;
            $user->user_type_id = 2;
            $user->teacher_id = $teacherid;
            $user->created_at = Carbon::now();
            $user->save();

            return redirect('teachers/'.$teacherid.'/account/'.$user->id.'/edit')->with('New Teacher account has been added successfully');  
        }
       
        
    }
    public function editAccount($teacherid,$accountid){
        $position = Positions::all();
        $account = User::find($accountid);
        return view('teachers.account.edit',compact('position','account','teacherid'));
    }
    public function updateAccount(Request $request,$teacherid,$accountid){
         $this->validate($request,[
            'username' => 'required',
            'password' => 'required',
            'email' => 'required',
        ]);

        $teacher = Teachers::find($teacherid);
        $user = User::find($accountid);
        $user->username = $request->username;
        $user->email = $request->email;
        $user->first_name = $teacher->first_name;
        $user->middle_name = $teacher->middle_name;
        $user->last_name = $teacher->last_name;
        $user->password = Hash::make($request->password);
        $user->position_id = $request->position_id;
        $user->updated_at = Carbon::now();
        $user->save();

        return redirect('teachers/'.$teacherid.'/account/'.$user->id.'/edit')->with('New Teacher account has been added successfully');  
        
    }
    public function destroyAccount($teacherid,$accountid){
        $account = User::find($accountid);
        $account->delete();
    }
    public function assigned($teacherid,$d_id){
        $courseDetails = CourseDetails::find($d_id);
        $courseDetails->teacher_id = $teacherid;
        $courseDetails->updated_at = Carbon::now();
        $courseDetails->update();
    }
    public function cancel($teacherid,$d_id){
        $courseDetails = CourseDetails::find($d_id);
        $courseDetails->teacher_id = 0;
        $courseDetails->updated_at = Carbon::now();
        $courseDetails->update();
    }
    public function getTeachers(){
        $teacher = Teachers::all();
         return Datatables::of($teacher)
            ->addColumn('action',function($teacher){
                return'<a class="btn btn-round btn-info btn-sm" href="teachers/'.$teacher->id.'/edit"><i class="fa fa-edit"></i>Edit</a> <a class="btn btn-round btn-danger btn-sm" href="#" id="delete" data-id="'.$teacher->id.'"><i class="fa fa-trash"></i>Delete</a>';
            })
            ->make(true);

    }
    public function getassigned($id){
        // $assigned = CourseDetails::with('subject')->with('year')->with('section')->with('course')->where('teacher_id',$id);
        $assigned = DB::table('courses_details as cd')
            ->select('c.code as code','c.major as major','sb.name as subject','s.code as section','y.code as year','s.code as section','cd._semester','cd.id')
            ->join('years as y','y.id','=','cd.year_id')
            ->join('subjects as sb','sb.id','=','cd.subject_id')
            ->join('sections as s','s.id','=','cd.section_id')
            ->join('courses as c','c.id','=','cd.course_id')
            ->where('cd.teacher_id',$id);
        return Datatables::of($assigned)
            ->addColumn('action',function($assigned){
                return '<a class="btn btn-round btn-danger btn-sm" data-id="'.$assigned->id.'" id="cancel" href="#">Cancel</a>';
            })
            ->make(true);
    }
    public function getnotassigned($id){
        // $assigned = CourseDetails::with('subject')->with('year')->with('section')->with('course')->where('teacher_id',0);

        $assigned = DB::table('courses_details as cd')
            ->select('c.code as code','c.major as major','sb.name as subject','s.code as section','y.code as year','s.code as section','cd._semester','cd.id')
            ->join('years as y','y.id','=','cd.year_id')
            ->join('subjects as sb','sb.id','=','cd.subject_id')
            ->join('sections as s','s.id','=','cd.section_id')
            ->join('courses as c','c.id','=','cd.course_id')
            ->where('cd.teacher_id',0);
        return Datatables::of($assigned)
            ->addColumn('action',function($assigned){
                return '<a class="btn btn-round btn-success btn-sm" data-id="'.$assigned->id.'" id="assign" href="#">Assign</a>';
            })
            ->make(true);
    }
    public function getteacheraccount($id){
        $account = User::with('position')->with('user_type')->where('teacher_id',$id);
        return DataTables::of($account)
            ->addColumn('action',function($account){
                return'<a class="btn btn-round btn-info btn-sm" href="account/'.$account->id.'/edit"><i class="fa fa-edit"></i>Edit</a> <a class="btn btn-round btn-danger btn-sm" href="#" id="delete" data-id="'.$account->id.'"><i class="fa fa-trash"></i>Delete</a>';
            })
            ->make(true);
    }
}
