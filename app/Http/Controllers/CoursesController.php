<?php

namespace App\Http\Controllers;

use App\CourseDetails;
use App\Courses;
use App\Sections;
use App\Subjects;
use App\Year;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\Datatables;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function verify(){
        $user = Auth::user();
        
        return view('auth.verify',compact('user'));
    }
    public function index()
    {
        // dd(1);
        return view('courses.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'code' => 'required|unique:courses',
            'name' => 'required|unique:courses',
            'major' => 'required',
        ]);
        $course = new Courses($request->all());
        $course->created_at = Carbon::now();
        $course->save();
        return redirect('/courses/'.$course->id.'/edit')->with('message','New Course has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Courses::find($id);
        return view('courses.view',compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        $course = Courses::find($id);
        return view('courses.edit',compact('id','course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($id);
        $this->validate($request, [
            'code' => 'required',
            'name' => 'required',
            'major' => 'required',
        ]);

        $course = Courses::find($id);
        $course->code = $request->code;
        $course->name = $request->name;
        $course->updated_at = Carbon::now();
        $course->save();
        return redirect('/courses/'.$course->id.'/edit')->with('message','New Course has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Courses::find($id);
        $course->delete();
    }
    
    public function createCourseDetails($courseid){
        $course = Courses::find($courseid);
        $subject = Subjects::all();
        $year = Year::all();
        $section = Sections::all();
        return view('course_details.create',compact('courseid','course','subject','year','section'));
    }
    public function storeCourseDetails(Request $request, $courseid){
      
        $this->validate($request,[
            'subject_id' => 'required',
            'year_id' => 'required',
            '_semester' => 'required',
            'units' => 'required',
        ]);
        $courseDetails = new CourseDetails;
        $courseDetails->course_id = $courseid;
        $courseDetails->subject_id = $request->subject_id;
        $courseDetails->year_id = $request->year_id;
        $courseDetails->section_id = $request->section_id;
        $courseDetails->_semester = $request->_semester;
        $courseDetails->units = $request->units;
        $courseDetails->term_exam = $request->term_exam;
        $courseDetails->quiz = $request->quiz;
        $courseDetails->assignment = $request->assignment;
        $courseDetails->attendance = $request->attendance;
        $courseDetails->participation = $request->participation;
        $courseDetails->teacher_id = 0;
        $courseDetails->created_at = Carbon::now();
        $courseDetails->save();
        return redirect('courses/'.$courseid.'/details/'.$courseDetails->id.'/edit')->with('message','New Course subject has been added Succesfully');
    }
    public function editCourseDetails($courseid,$id){
        $subject = Subjects::all();
        $year = Year::all();
        $section = Sections::all();
        $course = Courses::find($courseid);
        $courseDetails = CourseDetails::find($id);

        return view('course_details.edit',compact('subject','year','course','courseDetails','courseid','section'));
    }
    public function updateCourseDetails(Request $request, $courseid,$id){
        $this->validate($request,[
            'subject_id' => 'required',
            'year_id' => 'required',
            '_semester' => 'required',
            'units' => 'required',
        ]);
        $courseDetails = CourseDetails::find($id);
        $courseDetails->subject_id = $request->subject_id;
        $courseDetails->year_id = $request->year_id;
        $courseDetails->section_id = $request->section_id;
        $courseDetails->_semester = $request->_semester;
        $courseDetails->units = $request->units;
        $courseDetails->term_exam = $request->term_exam;
        $courseDetails->quiz = $request->quiz;
        $courseDetails->assignment = $request->assignment;
        $courseDetails->attendance = $request->attendance;
        $courseDetails->participation = $request->participation;
        $courseDetails->updated_at = Carbon::now();
        $courseDetails->update();

        return redirect('courses/'.$courseid.'/details/'.$id.'/edit')->with('message','Details has been updated successfully');
    }
    public function destroyDetails($courseId,$id){
        $courseDetails = CourseDetails::find($id);
        $courseDetails->delete();
    }
    public function getCourses(){   
        $courses = Courses::all();
        return Datatables::of($courses)
            ->addColumn('action',function($course){
                return'<a class="btn btn-rounded btn-success btn-sm" href="courses/'.$course->id.'"><i class="fa fa-eye"></i>View</a> <a class="btn btn-rounded btn-info btn-sm" href="courses/'.$course->id.'/edit"><i class="fa fa-edit"></i>Edit</a> <a class="btn btn-rounded btn-danger btn-sm" href="#" id="delete" data-id="'.$course->id.'" data-name="'.$course->name.'"><i class="fa fa-trash"></i>Delete</a>';
            })
            ->make(true);
    }
    public function getCoursesDetails($courseid){
        $courseDetails = CourseDetails::with('subject')->with('year')->where('course_id',$courseid)->with('section')->get();
        
        return DataTables::of($courseDetails)
            ->addColumn('action',function($details){
                $id = $details->id;

                return'<a class="btn btn-round btn-info btn-sm" href="details/'.$id.'/edit"><i class="fa fa-edit"></i>Edit</a> <a class="btn btn-round btn-danger btn-sm" href="#" id="delete" data-id="'.$id.'"><i class="fa fa-trash"></i>Delete</a>';
            })
            ->editColumn('_semester',function($courseDetails){
                if ($courseDetails->_semester == 1) {
                    return "1st";
                }else{
                    return "2nd";
                }

            })
            ->make(true);
    }
}
