<?php

namespace App\Http\Controllers;

use App\Courses;
use App\Genders;
use App\Positions;
use App\SchoolYear;
use App\Sections;
use App\StudentDetails;
use App\Students;
use App\User;
use App\Year;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\Datatables;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('students.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $course = Courses::all();
        $section = Sections::all();
        $year = Year::all();
        $gender = Genders::all();
        $sc = SchoolYear::all();
        return view('students.create',compact('course','year','section','gender','sc'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request,[
            'id_number'=>'required|unique:students',
            'first_name' => 'required',
            'last_name' => 'required',
            'middle_name' => 'required',
            'email'=>'required',
            'course_id' => 'required',
            'year_id' => 'required',
            'section_id' => 'required',
            'gender_id' => 'required',
            'address' => 'required',
            'number' => 'required',
            'contact_person' => 'required',
            'contact_number' => 'required',
        ]);
       
        
        $student = new Students;
        $student->id_number = $request->id_number;
        $student->first_name =  $request->first_name;
        $student->last_name =  $request->last_name;
        $student->middle_name =  $request->middle_name;
        $student->suffix =  $request->suffix;
        $student->created_at = Carbon::now();
        $student->save();

        $studentDetail = new StudentDetails; 
        $studentDetail->student_id = $student->id;
        $studentDetail->email = $request->email;
        $studentDetail->course_id = $request->course_id;
        $studentDetail->year_id = $request->year_id;
        $studentDetail->section_id = $request->section_id;
        $studentDetail->gender_id = $request->gender_id;
        $studentDetail->school_year_id = $request->sc_year;
        $studentDetail->address = $request->address;
        $studentDetail->number = $request->number;
        $studentDetail->contact_person = $request->contact_person;
        $studentDetail->contact_number = $request->contact_number;
        $studentDetail->m_fname = $request->m_fname;
        $studentDetail->m_lname = $request->m_lname;
        $studentDetail->m_mname = $request->m_mname;
        $studentDetail->m_suffix = $request->m_suffix;
        $studentDetail->f_fname = $request->f_fname;
        $studentDetail->f_lname = $request->f_lname;
        $studentDetail->f_mname = $request->f_mname;
        $studentDetail->f_suffix = $request->f_suffix;    
        $studentDetail->f_number = $request->f_number;
        $studentDetail->m_number = $request->m_number;
        $studentDetail->created_at = Carbon::now();
        $studentDetail->save();

        return redirect('students/'.$student->id.'/edit')->with('message','New Student has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Students::find($id);

        return view('students.view',compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Students::find($id);
        $studentd = StudentDetails::where('student_id',$id)->first();
        $findacc = User::where('student_id',$id)->first();
      
    
        $course = Courses::all();
        $section = Sections::all();
        $year = Year::all();
        $gender = Genders::all();
        $sc = SchoolYear::all();
        // dd($gender);
        return view('students.edit',compact('id','student','studentd','course','year','section','gender','sc'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'middle_name' => 'required',
            'email'=>'required',
            'course_id' => 'required',
            'year_id' => 'required',
            'section_id' => 'required',
            'address' => 'required',
            'number' => 'required',
            'contact_person' => 'required',
            'contact_number' => 'required',
        ]);
        $studentDetailId = StudentDetails::select('id')->where('student_id',$id)->first();
        $student = Students::find($id);
        $student->first_name = $request->first_name;
        $student->last_name = $request->last_name;
        $student->middle_name = $request->middle_name;
        $student->suffix = $request->suffix;
        $student->updated_at = Carbon::now();
        $student->update();

        $studentDetail = StudentDetails::find($studentDetailId->id);
        $studentDetail->course_id = $request->course_id;
        $studentDetail->email = $request->email;
        $studentDetail->year_id = $request->year_id;
        $studentDetail->section_id = $request->section_id;
        $studentDetail->gender_id = $request->gender_id;
        $studentDetail->school_year_id = $request->school_year_id;
        $studentDetail->address = $request->address;
        $studentDetail->number = $request->number;
        $studentDetail->contact_person = $request->contact_person;
        $studentDetail->contact_number = $request->contact_number;
        $studentDetail->m_fname = $request->m_fname;
        $studentDetail->m_lname = $request->m_lname;
        $studentDetail->m_mname = $request->m_mname;
        $studentDetail->m_suffix = $request->m_suffix;
        $studentDetail->f_fname = $request->f_fname;
        $studentDetail->f_lname = $request->f_lname;
        $studentDetail->f_mname = $request->f_mname;
        $studentDetail->f_suffix = $request->f_suffix;    
        $studentDetail->f_number = $request->f_number;
        $studentDetail->m_number = $request->m_number;
        $studentDetail->updated_at = Carbon::now();
        $studentDetail->update();

        
        return redirect('students/'.$student->id.'/edit')->with('message','New Student has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Students::find($id);
        $studentDetail = StudentDetails::where('student_id',$id)->first();
        $studentDetailDelete = StudentDetails::find($studentDetail->id);
        $studentDetailDelete->delete();
        $student->delete();
    }
    public function getStudents(){
        $student = Students::all();
        return Datatables::of($student)
            ->addColumn('action',function($student){
                return '<a class="btn btn-round btn-success btn-sm" href="students/'.$student->id.'"><i class="fa fa-eye"></i>View</a> <a class="btn btn-round btn-info btn-sm" href="students/'.$student->id.'/edit"><i class="fa fa-edit"></i>Edit</a> <a class="btn btn-round btn-danger btn-sm" href="#" id="delete" data-id="'.$student->id.'"><i class="fa fa-trash"></i>Delete</a>';
            })
            ->make(true);
    }
    public function showDetails($studid,$id){
        $student = StudentDetails::find($id);
        return view('students_details.view',compact('student'));
    }
    public function createDetails($id){
        $course = Courses::all();
        $section = Sections::all();
        $year = Year::all();
        $gender = Genders::all();
        $sc = SchoolYear::all();
        //add trapping here
        $student = Students::find($id);
        $studentDetail = StudentDetails::where('student_id',$id)->first();
        if (!is_null($studentDetail)) {
            abort(411,'Opps...You cannot add on more details to '.$student->first_name.' '.$student->last_name);
        }else{
            return view('students_details.create',compact('course','section','year','gender','id','sc'));

        }
    }
    public function storeDetails(Request $request, $id){
       
        $validatedData = $request->validate([
            'course_id' => 'required',
            'year_id' => 'required',
            'section_id' => 'required',
            'gender_id' => 'required',
            'address' => 'required',
            'number' => 'required',
            'contact_person' => 'required',
            'contact_number' => 'required',
            
        ]);  

        $studentDetail = new StudentDetails; 
        $studentDetail->student_id = $request->id;
        $studentDetail->course_id = $request->course_id;
        $studentDetail->year_id = $request->year_id;
        $studentDetail->section_id = $request->section_id;
        $studentDetail->gender_id = $request->gender_id;
        $studentDetail->school_year_id = $request->sc_year;
        $studentDetail->address = $request->address;
        $studentDetail->number = $request->number;
        $studentDetail->contact_person = $request->contact_person;
        $studentDetail->contact_number = $request->contact_number;
        $studentDetail->m_fname = $request->m_fname;
        $studentDetail->m_lname = $request->m_lname;
        $studentDetail->m_mname = $request->m_mname;
        $studentDetail->m_suffix = $request->m_suffix;
        $studentDetail->f_fname = $request->f_fname;
        $studentDetail->f_lname = $request->f_lname;
        $studentDetail->f_mname = $request->f_mname;
        $studentDetail->f_suffix = $request->f_suffix;    
        $studentDetail->f_number = $request->f_number;
        $studentDetail->m_number = $request->m_number;
        $studentDetail->created_at = Carbon::now();
        $studentDetail->save();
        return redirect('students/'.$id.'/details/'.$studentDetail->id.'/edit')->with('message','New Student Detail has been added successfully');

    }
    public function editDetails($stud_id,$id){
        $student = StudentDetails::find($id);
        $course = Courses::all();
        $section = Sections::all();
        $year = Year::all();
        $gender = Genders::all();
        $sc = SchoolYear::all();
        // dd($gender);
        return view('students_details.edit',compact('stud_id','id','student','course','year','section','gender','sc'));
    }
    public function updateDetails(Request $request,$stud_id,$id){
        $validatedData = $request->validate([
            'course_id' => 'required',
            'year_id' => 'required',
            'section_id' => 'required',
            'address' => 'required',
            'number' => 'required',
            'contact_person' => 'required',
            'contact_number' => 'required',
        ]);  

        $studentDetail = StudentDetails::find($id);
        $studentDetail->course_id = $request->course_id;
        $studentDetail->year_id = $request->year_id;
        $studentDetail->section_id = $request->section_id;
        $studentDetail->gender_id = $request->gender_id;
        $studentDetail->school_year_id = $request->school_year_id;
        $studentDetail->address = $request->address;
        $studentDetail->number = $request->number;
        $studentDetail->contact_person = $request->contact_person;
        $studentDetail->contact_number = $request->contact_number;
        $studentDetail->m_fname = $request->m_fname;
        $studentDetail->m_lname = $request->m_lname;
        $studentDetail->m_mname = $request->m_mname;
        $studentDetail->m_suffix = $request->m_suffix;
        $studentDetail->f_fname = $request->f_fname;
        $studentDetail->f_lname = $request->f_lname;
        $studentDetail->f_mname = $request->f_mname;
        $studentDetail->f_suffix = $request->f_suffix;    
        $studentDetail->f_number = $request->f_number;
        $studentDetail->m_number = $request->m_number;
        $studentDetail->updated_at = Carbon::now();
        $studentDetail->update();

        return redirect('students/'.$stud_id.'/details/'.$studentDetail->id.'/edit')->with('message','New Student Detail has been updated successfully');        
    }
    public function createAccount($studentid){
        $position = Positions::where('name','=','Student')->first();   
        return view('students.accounts.create',compact('position','studentid'));
    }
    public function storeAccount(Request $request,$studentid){
        $position_id = $request->position_id;
        if ($position_id == 0) {
            $this->validate($request,[
                'username' => 'required|unique:users',
                'email' => 'required',
                'password' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
            ]);
            $find = User::where('student_id',$studentid)->where('parent_id',1)->count('parent_id');
            
            if ($find >= 2) {
                abort(411,'Sorry you have reach the limit of number of parents account');
            }else{

                $student = Students::find($studentid);
                $user = new User;
                $user->username = $request->username;
                $user->email = $request->email;
                $user->first_name = $request->first_name;
                $user->middle_name = $request->middle_name;
                $user->last_name = $request->last_name;
                $user->password = Hash::make($request->password);
                $user->position_id = $request->position_id;
                $user->user_type_id = 4;
                $user->student_id = $studentid;
                $user->parent_id = 1;
                $user->created_at = Carbon::now();
                $user->save();

                return redirect('students/'.$studentid.'/account/'.$user->id.'/edit')->with('New Student account has been added successfully');
            }
        }else if ($position_id >0) {
            $this->validate($request,[
                'username' => 'required|unique:users',
                'password' => 'required',
            ]);
            $find = User::where('student_id',$studentid)->first();
            if (!is_null($find)) {
                abort(411,'Sorry this student kay naa nay account!');
            }else{

                $student = Students::find($studentid);
                $user = new User;
                $user->username = $request->username;
                $user->email = $request->email;
                $user->first_name = $student->first_name;
                $user->middle_name = $student->middle_name;
                $user->last_name = $student->last_name;
                $user->password = Hash::make($request->password);
                $user->position_id = $request->position_id;
                $user->user_type_id = 3;
                $user->student_id = $studentid;
                $user->created_at = Carbon::now();
                $user->save();

                return redirect('students/'.$studentid.'/account/'.$user->id.'/edit')->with('New Student account has been added successfully');
            }
        }else{

        }
          
        
    }
    public function updateAccount(Request $request,$studentid,$accountid){
        $findu = User::find($accountid);
        if ($findu->parent_id == 1) {
            $this->validate($request,[
                'username' => 'required',
                'email' => 'required',
                'password' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
            ]);
                $user = User::find($accountid);
                $user->username = $request->username;
                $user->email = $request->email;
                $user->first_name = $request->first_name;
                $user->middle_name = $request->middle_name;
                $user->last_name = $request->last_name;
                $user->password = Hash::make($request->password);
                $user->student_id = $studentid;
                $user->created_at = Carbon::now();
                $user->save();
                return redirect('students/'.$studentid.'/account/'.$user->id.'/edit')->with('message','New Student account has been updated successfully');
        }else{
            $this->validate($request,[
                'username' => 'required',
                'password' => 'required',
            ]);
            $find = User::where('student_id',$studentid)->first();
            $student = Students::find($studentid);
            $user = User::find($accountid);
            $user->username = $request->username;
            $user->email = $request->email;
            $user->first_name = $student->first_name;
            $user->middle_name = $student->middle_name;
            $user->last_name = $student->last_name;
            $user->password = Hash::make($request->password);
            $user->position_id = $request->position_id;
            $user->updated_at = Carbon::now();
            $user->save();

            return redirect('students/'.$studentid.'/account/'.$user->id.'/edit')->with('message','New Student account has been updated successfully'); 
        }
         
        
    }
    public function editAccount($studentid,$accountid){
        $position = Positions::where('name','=','Student')->first(); 
        $account = User::find($accountid);
        
        return view('students.accounts.edit',compact('position','account'));
    }
    public function getStudentsDetails($studid,$id){
        $studentDetails = StudentDetails::with('course')->with('year')->with('section')->where('student_id',$studid);
        return Datatables::of($studentDetails)
            ->addColumn('action',function($studentDetails)use($id){
                return'<a class="btn btn-round btn-success btn-sm" href="details/'.$studentDetails->id.'/view"><i class="fa fa-eye"></i>View</a> <a class="btn btn-round btn-info btn-sm" href="details/'.$id.'/edit"><i class="fa fa-edit"></i>Edit</a> <a class="btn btn-round btn-danger btn-sm" href="#" id="delete" data-id="'.$studentDetails->id.'"><i class="fa fa-trash"></i>Delete</a>';
            })
            ->make(true);
    }
    public function destroyAccount($studentid,$accountid){  
        $account = User::find($accountid);
        $account->delete();
    }
    public function getAccount($id){
        $account = User::with('position')->with('user_type')->where('student_id',$id);
        return Datatables::of($account)
            ->editColumn('position.name',function($account){
                if ($account->position_id == 0) {
                    return "Parent";
                }else if ($account->position_id == 5) {
                    return "Student";
                }else{
                    return "";
                }
            })

            ->addColumn('action',function($account){
                return'<a class="btn btn-round btn-info btn-sm" href="account/'.$account->id.'/edit"><i class="fa fa-edit"></i>Edit</a> <a class="btn btn-round btn-danger btn-sm" href="#" id="delete" data-id="'.$account->id.'"><i class="fa fa-trash"></i>Delete</a>';
            })
            ->make(true);
    }
    public function destroyDetails($studid,$id){
        // dd(1);
        $studentDetails = StudentDetails::find($id);
        $studentDetails->delete();
        // return view('students_details.edit',compact('student','studid','id'));
    }
}
