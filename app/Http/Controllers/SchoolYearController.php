<?php

namespace App\Http\Controllers;

use App\SchoolYear;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\Datatables;

class SchoolYearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('school_year.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('school_year.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_year = $request->from."-".$request->to;
        $sc = new SchoolYear;
        $sc->school_year = $school_year;
        $sc->save();
        return redirect('schoolyear');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sc = SchoolYear::find($id);
        $schoolyear = $sc->school_year;
        $splt = (explode("-",$schoolyear));
        $from = $splt[0];
        $to = $splt[1];
        return view('school_year.edit',compact('from','to','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $school_year = $request->from."-".$request->to;
        $sc = SchoolYear::find($id);
        $sc->school_year = $school_year;
        $sc->save();
        return redirect('schoolyear');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getschoolyear(){
        $sc = SchoolYear::all();
        return Datatables::of($sc)
            ->addColumn('action',function($sc){

                return'<a class="btn btn-round btn-info btn-sm" href="schoolyear/'.$sc->id.'/edit"><i class="fa fa-edit"></i>Edit</a> <a class="btn btn-round btn-danger btn-sm" href="#" id="delete" data-id="'.$sc->id.'"><i class="fa fa-trash"></i>Delete</a>';
            })
            ->make(true);
    }
}
