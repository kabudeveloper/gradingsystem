<?php

namespace App\Http\Controllers;

use App\Verification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VerificationController extends Controller
{
    public function verified(Request $request){
    	$user = Auth::user();
    	$today = Carbon::today();
    	$find = Verification::where('date',$today)->where('user_id',$user->id)->where('code',$request->code)->first();
        if (!is_null($find)) {
            if ($find->_verified == 0) {
                if ($find->code == $request->code) {
                    $get = Verification::where('date',$today)->where('user_id',$user->id)->first();
                    $get->_verified = 1;
                    $get->save();
                    $find->_verified = 1;
                    $find->save();
                    return redirect('/home');
                }else{
                    return redirect('verify')->with('error_message','Wrong Code, Please Check you email');
                }
            }else{
                return redirect('verify')->with('error_message','This Code has expired!');
            }
        }else{
            return redirect('verify')->with('error_message','Wrong Code, Please Check you email');
        }
    	
    }
     protected function relogout(){
        auth()->logout();
        return redirect('/login');
    }
}
