<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Attendance;
use App\AttendanceScore;
use App\CourseDetails;
use App\GradeDetails;
use App\GradeEquivalent;
use App\Grades;
use App\Quiz;
use App\SchoolYear;
use App\SemesterGrade;
use App\SemesterGradeDetails;
use App\StudentDetails;
use App\SubjectFinalGrade;
use App\TermExam;
use App\TermExamDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\Datatables;

class GradeDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usertype = Auth::user()->user_type_id;
        $user = Auth::user();
        $sc = SchoolYear::all();
        if ($usertype == 2) {
            $course = CourseDetails::select('course_id')
                ->with('course')
                ->where('teacher_id',$user->teacher_id)
                ->groupby('course_id')
                ->get();
            return view('grade_details.index',compact('course','sc'));
        }else{
            abort(415, 'Forbidden Access');
        }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      

    }
    public function gstore(Request $request)
    {
        
        $teacherid = Auth::user()->teacher_id;
        if($request->term ==1){
            $find = TermExam::where('course_id',$request->course)
            ->where('subject_id',$request->subject)
            ->where('year_id',$request->year)
            ->where('section_id',$request->section)
            ->where('_semester',$request->semester)
            ->where('teacher_id',$teacherid)
            ->where('term_id',1)
            ->where('school_year_id',$request->sc_year)
            ->first();
            if (is_null($find)) {
                return "You must give prelim exam first";
            }
        }elseif ($request->term ==2) {
            $find = Grades::where('course_id',$request->course)
            ->where('subject_id',$request->subject)
            ->where('year_id',$request->year)
            ->where('section_id',$request->section)
            ->where('_semester',$request->semester)
            ->where('term_id',1)
            ->where('school_year_id',$request->sc_year)
            ->first();
            if (is_null($find)) {
                return "sorry you havent computed prelim yet";
            }
        }elseif ($request->term ==3) {
            
        }else {
            $find = Grades::where('course_id',$request->course)
            ->where('subject_id',$request->subject)
            ->where('year_id',$request->year)
            ->where('section_id',$request->section)
            ->where('_semester',$request->semester)
            ->where('term_id',3)
            ->where('school_year_id',$request->sc_year)
            ->first();
            if (is_null($find)) {
                return "sorry you havent computed semi-final yet";
            }
        }
        if ($request->term == 1 || $request->term == 3) {
            return $this->semisGradeComputation($request->course,$request->subject,$request->year,$request->section,$request->semester,$request->term,$request->sc_year,$teacherid);
        }else{
            return $this->finalGradeComputation($request->course,$request->subject,$request->year,$request->section,$request->semester,$request->term,$request->sc_year,$teacherid);
        }

    }
    private function semisGradeComputation($course,$subject,$year,$section,$semester,$term,$sc_year,$teacherid){
        $termName = ($term == 1) ? 'Prelim' : 'Semi-Finals';
        $find = Grades::where('course_id',$course)
            ->where('subject_id',$subject)
            ->where('year_id',$year)
            ->where('section_id',$section)
            ->where('_semester',$semester)
            ->where('term_id',$term)
            ->where('school_year_id',$sc_year)
            ->first();
            if (!is_null($find)) {
                //category
                // 1 - term exam
                // 2 - quiz
                // 3 - assignment
                // 5 - paticipation/performance
                // 4 - attendance
                $getCourseDetails = DB::table('courses_details as cd')
                    ->select('term_exam','quiz','assignment','attendance','participation')
                    ->where('course_id',$course)
                    ->where('subject_id',$subject)
                    ->where('year_id',$year)
                    ->where('section_id',$section)
                    ->where('_semester',$semester)
                    ->where('teacher_id',$teacherid)
                    ->first();
                 if (is_null($getCourseDetails)) {
                    // abort(415,"Cannot Proceed, Please Check your inputs");
                    return "Cannot Proceed, Please Check your inputs";
                }
                
                //E X A M
                $termScore = 0;
                $quizScore = 0;
                $assScore = 0;
                $attScore = 0;
                $termDetails = 0;
                $quizDetails = 0;
                $assDetails = 0;
                $attDetails = 0;

                if ($getCourseDetails->term_exam !=0 || $getCourseDetails->term_exam != NULL) {
                    $check = TermExam::where('teacher_id',$teacherid)
                        ->where('course_id',$course)
                        ->where('subject_id',$subject)
                        ->where('year_id',$year)
                        ->where('section_id',$section)
                        ->where('_semester',$semester)
                        ->where('term_id',$term)
                        ->where('school_year_id',$sc_year)
                        ->first();
                    if(is_null($check)){
                        // abort(415,"Sorry, Cannot Proceeed, Please check ur input");
                        return "Sorry, Cannot Proceeed, Empty Exam for ".$termName;
                    }
                    $getTermTotal = TermExam::select(DB::raw('SUM(totalscore) as totalscore'))
                        ->where('teacher_id',$teacherid)
                        ->where('course_id',$course)
                        ->where('subject_id',$subject)
                        ->where('year_id',$year)
                        ->where('section_id',$section)
                        ->where('_semester',$semester)
                        ->where('term_id',$term)
                        ->where('school_year_id',$sc_year)
                        ->first();

                    $getStudentsTerm = DB::table('term_exam as te')
                        ->select(DB::raw('SUM(ted.score) as score'),'ted.student_id')
                        ->leftjoin('term_exam_details as ted','te.id','=','ted.term_id')
                        ->where('te.teacher_id',$teacherid)
                        ->where('te.course_id',$course)
                        ->where('te.subject_id',$subject)
                        ->where('te.year_id',$year)
                        ->where('te.section_id',$section)
                        ->where('te._semester',$semester)
                        ->where('te.term_id',$term)
                        ->where('te.school_year_id',$sc_year)
                        ->groupby('ted.student_id')
                        ->get();
                    $termPercentage = $getCourseDetails->term_exam / 100;
                    $termPs = $getTermTotal->totalscore;

                    foreach($getStudentsTerm as $gst){
                        // $termScore = $gst->score * $termPercentage;
                        $termScore = $gst->score;
                        $computeInitial = ($termScore * 40) / $termPs;
                        $addInitial = $computeInitial + 55;
                        $finalTermGrade = $addInitial * $termPercentage;
                        
                        $addTerm = GradeDetails::where('grade_id',$find->id)
                            ->where('student_id',$gst->student_id)
                            ->where('category',1)->first();
                        if (!is_null($addTerm)) {
                            $addTerm->grade = $finalTermGrade;
                            $addTerm->update();
                        }else{
                            $addNewTerm = new GradeDetails;
                            $addNewTerm->grade_id = $find->id;
                            $addNewTerm->student_id = $gst->student_id;
                            $addNewTerm->category = 1;
                            $addNewTerm->grade = $finalTermGrade;
                            $addNewTerm->save();
                            
                        }
                        
                    }
                }


                //Q U I Z
                if ($getCourseDetails->quiz !=0 || $getCourseDetails->quiz != NULL) {
                    
                    $getQuizTotal = Quiz::select(DB::raw('SUM(totalscore) as totalscore'))
                        ->where('teacher_id',$teacherid)
                        ->where('course_id',$course)
                        ->where('subject_id',$subject)
                        ->where('year_id',$year)
                        ->where('section_id',$section)
                        ->where('_semester',$semester)
                        ->where('school_year_id',$sc_year)
                        ->where('term_id',$term)->first();

                    $getStudentsQuiz = DB::table('quizes as te')
                        ->select(DB::raw('SUM(ted.score) as score'),'ted.student_id')
                        ->leftjoin('quiz_details as ted','te.id','=','ted.quiz_id')
                        ->where('te.teacher_id',$teacherid)
                        ->where('te.course_id',$course)
                        ->where('te.subject_id',$subject)
                        ->where('te.year_id',$year)
                        ->where('te.section_id',$section)
                        ->where('te._semester',$semester)
                        ->where('te.term_id',$term)
                        ->where('te.school_year_id',$sc_year)
                        ->groupby('ted.student_id')
                        ->get();
                    $quizPercentage = $getCourseDetails->quiz / 100;
                    $quizPs = $getQuizTotal->totalscore;

                    foreach($getStudentsQuiz as $gst){
                        $quizScore = $gst->score;
                        $computeInitial = ($quizScore * 40) / $quizPs;
                        $addInitial = $computeInitial + 55;
                        $finalQuizGrade = $addInitial * $quizPercentage;

                        $addTerm = GradeDetails::where('grade_id',$find->id)
                            ->where('student_id',$gst->student_id)
                            ->where('category',2)->first();
                        if (!is_null($addTerm)) {
                            $addTerm->grade = $finalQuizGrade;
                            $addTerm->update();
                        }else{
                            $addNewTerm = new GradeDetails;
                            $addNewTerm->grade_id = $find->id;
                            $addNewTerm->student_id = $gst->student_id;
                            $addNewTerm->category = 2;
                            $addNewTerm->grade = $finalQuizGrade;
                            $addNewTerm->save();
                            
                        }
                    }
                }
                //A S S I G N M E N T 

                if ($getCourseDetails->assignment !=0 || $getCourseDetails->assignment != NULL) {
                    
                    $getAssTotal = Assignment::select(DB::raw('SUM(totalscore) as totalscore'))
                        ->where('teacher_id',$teacherid)
                        ->where('course_id',$course)
                        ->where('subject_id',$subject)
                        ->where('year_id',$year)
                        ->where('section_id',$section)
                        ->where('_semester',$semester)
                        ->where('school_year_id',$sc_year)
                        ->where('term_id',$term)->first();

                    $getStudentAss = DB::table('assignment as te')
                        ->select(DB::raw('SUM(ted.score) as score'),'ted.student_id')
                        ->leftjoin('assignment_details as ted','te.id','=','ted.assignment_id')
                        ->where('te.teacher_id',$teacherid)
                        ->where('te.course_id',$course)
                        ->where('te.subject_id',$subject)
                        ->where('te.year_id',$year)
                        ->where('te.section_id',$section)
                        ->where('te._semester',$semester)
                        ->where('te.term_id',$term)
                        ->where('te.school_year_id',$sc_year)
                        ->groupby('ted.student_id')
                        ->get();
                    $assPercentage = $getCourseDetails->assignment / 100;
                    $assPs = $getAssTotal->totalscore;

                    foreach($getStudentAss as $gst){
                        $assScore = $gst->score;
                        $computeInitial = ($assScore * 40) / $assPs;
                        $addInitial = $computeInitial + 55;
                        $finalAssGrade = $addInitial * $assPercentage;

                        $addTerm = GradeDetails::where('grade_id',$find->id)
                            ->where('student_id',$gst->student_id)
                            ->where('category',3)->first();
                        if (!is_null($addTerm)) {
                            $addTerm->grade = $finalAssGrade;
                            $addTerm->update();
                        }else{
                            $addNewTerm = new GradeDetails;
                            $addNewTerm->grade_id = $find->id;
                            $addNewTerm->student_id = $gst->student_id;
                            $addNewTerm->category = 3;
                            $addNewTerm->grade = $finalAssGrade;
                            $addNewTerm->save();
                            
                        }
                    }

                }
                // A T T E N D A N C E
                if ($getCourseDetails->attendance !=0 || $getCourseDetails->attendance != NULL) {

                    $getAttTotal = AttendanceScore::select(DB::raw('SUM(totalscore) as totalscore'))
                        ->where('teacher_id',$teacherid)
                        ->where('course_id',$course)
                        ->where('subject_id',$subject)
                        ->where('year_id',$year)
                        ->where('section_id',$section)
                        ->where('_semester',$semester)
                        ->where('school_year_id',$sc_year)
                        ->where('term_id',$term)->first();

                    $getStudentAtt = DB::table('attendance_score as te')
                        ->select(DB::raw('SUM(ted.score) as score'),'ted.student_id')
                        ->leftjoin('attendance_score_details as ted','te.id','=','ted.attendance_score_id')
                        ->where('te.teacher_id',$teacherid)
                        ->where('te.course_id',$course)
                        ->where('te.subject_id',$subject)
                        ->where('te.year_id',$year)
                        ->where('te.section_id',$section)
                        ->where('te._semester',$semester)
                        ->where('te.term_id',$term)
                        ->where('te.school_year_id',$sc_year)
                        ->groupby('ted.student_id')
                        ->get();
                    $attPercentage = $getCourseDetails->attendance / 100;
                    $attPs = $getAttTotal->totalscore;
                        
                    foreach($getStudentAtt as $gst){
                        $attScore = $gst->score;
                        $computeInitial = ($attScore * 40) / $attPs;
                        $addInitial = $computeInitial + 55;
                        $finalAttGrade = $addInitial * $attPercentage;

                        $addTerm = GradeDetails::where('grade_id',$find->id)
                            ->where('student_id',$gst->student_id)
                            ->where('category',4)->first();
                        if (!is_null($addTerm)) {
                            $addTerm->grade = $finalAttGrade;
                            $addTerm->update();
                        }else{
                            $addTerm = new GradeDetails;
                            $addTerm->grade_id = $find->id;
                            $addTerm->student_id = $gst->student_id;
                            $addTerm->category = 4;
                            $addTerm->grade = $finalAttGrade;
                            $addTerm->save();
                        }
                        
                    }
                    

                }
                 $getAllPartialGrades = GradeDetails::select(DB::raw('SUM(grade) as sgrade'),'student_id')
                        ->where('grade_id',$find->id)
                        ->groupby('student_id')
                        ->get();
                foreach ($getAllPartialGrades as $gapg) {
                    $fixedEquivalent = round($gapg->sgrade,0);
                    $finalGrade = 0;
                    if ($fixedEquivalent >=95) {
                        $finalGrade = 1.0;
                    }else if($fixedEquivalent <= 50){
                        $finalGrade = 5.0;
                    }else{

                        $finalEquivalent = GradeEquivalent::where('grade',$fixedEquivalent)->first();
                        $finalGrade = $finalEquivalent->equivalent;
                    }
                    
                    $findSubjectFinalGrade = SubjectFinalGrade::where('grade_id',$find->id)->where('student_id',$gapg->student_id)->first();
                    if (!is_null($findSubjectFinalGrade)) {
                        $findSubjectFinalGrade->subject_grade = $gapg->sgrade;
                        $findSubjectFinalGrade->subject_equivalent = $fixedEquivalent;
                        $findSubjectFinalGrade->final_equivalent = $finalGrade;
                        $findSubjectFinalGrade->update();
                    }else{
                        $subjectGrade = new SubjectFinalGrade;
                        $subjectGrade->grade_id = $find->id;
                        $subjectGrade->student_id = $gapg->student_id;
                        $subjectGrade->subject_grade = $gapg->sgrade;
                        $subjectGrade->subject_equivalent = $fixedEquivalent;
                        $subjectGrade->final_equivalent = $finalGrade;
                        $subjectGrade->save();
                    }
                    
                }
            
            }else{
               //category
            // 1 - term exam
            // 2 - quiz
            // 3 - assignment
            // 4 - paticipation/performance
            // 5 - attendance
                $getCourseDetails = DB::table('courses_details as cd')
                    ->select('term_exam','quiz','assignment','attendance','participation')
                    ->where('course_id',$course)
                    ->where('subject_id',$subject)
                    ->where('year_id',$year)
                    ->where('section_id',$section)
                    ->where('_semester',$semester)
                    ->where('teacher_id',$teacherid)
                    ->first();
                if (is_null($getCourseDetails)) {
                    return "Cannot Proceed, Please contact the admin for course criteria";
                }
                $termScore = 0;
                $quizScore = 0;
                $assScore = 0;
                $attScore = 0;
                $termDetails = 0;
                $quizDetails = 0;
                $assDetails = 0;
                $attDetails = 0;

                $add = new Grades;
                $add->course_id = $course;
                $add->subject_id = $subject;
                $add->year_id = $year;
                $add->section_id = $section;
                $add->_semester = $semester;
                $add->term_id = $term;
                $add->school_year_id = $sc_year;
                $add->save();
                $gradeid = $add->id;

                if ($getCourseDetails->term_exam !=0 || $getCourseDetails->term_exam != NULL) {
                    
                    $getTermTotal = TermExam::select(DB::raw('SUM(totalscore) as totalscore'))
                        ->where('teacher_id',$teacherid)
                        ->where('course_id',$course)
                        ->where('subject_id',$subject)
                        ->where('year_id',$year)
                        ->where('section_id',$section)
                        ->where('_semester',$semester)
                        ->where('school_year_id',$sc_year)
                        ->where('term_id',$term)->first();

                    $getStudentsTerm = DB::table('term_exam as te')
                        ->select(DB::raw('SUM(ted.score) as score'),'ted.student_id')
                        ->leftjoin('term_exam_details as ted','te.id','=','ted.term_id')
                        ->where('te.teacher_id',$teacherid)
                        ->where('te.course_id',$course)
                        ->where('te.subject_id',$subject)
                        ->where('te.year_id',$year)
                        ->where('te.section_id',$section)
                        ->where('te._semester',$semester)
                        ->where('te.term_id',$term)
                        ->where('te.school_year_id',$sc_year)
                        ->groupby('ted.student_id')
                        ->get();
                    $termPercentage = $getCourseDetails->term_exam / 100;
                    $termPs = $getTermTotal->totalscore;

                    foreach($getStudentsTerm as $gst){
                        $termScore = $gst->score;
                        $computeInitial = ($termScore * 40) / $termPs;
                        $addInitial = $computeInitial + 55;
                        $finalTermGrade = $addInitial * $termPercentage;

                        $addTerm = new GradeDetails;
                        $addTerm->grade_id = $gradeid;
                        $addTerm->student_id = $gst->student_id;
                        $addTerm->category = 1;
                        $addTerm->grade = $finalTermGrade;
                        $addTerm->save();
                    }
                   
                }

                // Q U I Z
                if ($getCourseDetails->quiz !=0 || $getCourseDetails->quiz != NULL) {

                    $getQuizTotal = Quiz::select(DB::raw('SUM(totalscore) as totalscore'))
                        ->where('teacher_id',$teacherid)
                        ->where('course_id',$course)
                        ->where('subject_id',$subject)
                        ->where('year_id',$year)
                        ->where('section_id',$section)
                        ->where('_semester',$semester)
                        ->where('school_year_id',$sc_year)
                        ->where('term_id',$term)->first();

                    $getStudentsQuiz = DB::table('quizes as te')
                        ->select(DB::raw('SUM(ted.score) as score'),'ted.student_id')
                        ->leftjoin('quiz_details as ted','te.id','=','ted.quiz_id')
                        ->where('te.teacher_id',$teacherid)
                        ->where('te.course_id',$course)
                        ->where('te.subject_id',$subject)
                        ->where('te.year_id',$year)
                        ->where('te.section_id',$section)
                        ->where('te._semester',$semester)
                        ->where('te.term_id',$term)
                        ->where('te.school_year_id',$sc_year)
                        ->groupby('ted.student_id')
                        ->get();
                    $quizPercentage = $getCourseDetails->quiz / 100;
                    $quizPs = $getQuizTotal->totalscore;

                    foreach($getStudentsQuiz as $gst){
                        $quizScore = $gst->score;
                        $computeInitial = ($quizScore * 40) / $quizPs;
                        $addInitial = $computeInitial + 55;
                        $finalTermGrade = $addInitial * $quizPercentage;

                        $addTerm = new GradeDetails;
                        $addTerm->grade_id = $gradeid;
                        $addTerm->student_id = $gst->student_id;
                        $addTerm->category = 2;
                        $addTerm->grade = $finalTermGrade;
                        $addTerm->save();
                        
                    }
                   
                }

                //A S S I G N M E N T
                
                if ($getCourseDetails->assignment !=0 || $getCourseDetails->assignment != NULL) {

                    $getAssTotal = Assignment::select(DB::raw('SUM(totalscore) as totalscore'))
                        ->where('teacher_id',$teacherid)
                        ->where('course_id',$course)
                        ->where('subject_id',$subject)
                        ->where('year_id',$year)
                        ->where('section_id',$section)
                        ->where('_semester',$semester)
                        ->where('school_year_id',$sc_year)
                        ->where('term_id',$term)->first();

                    $getStudentAss = DB::table('assignment as te')
                        ->select(DB::raw('SUM(ted.score) as score'),'ted.student_id')
                        ->leftjoin('assignment_details as ted','te.id','=','ted.assignment_id')
                        ->where('te.teacher_id',$teacherid)
                        ->where('te.course_id',$course)
                        ->where('te.subject_id',$subject)
                        ->where('te.year_id',$year)
                        ->where('te.section_id',$section)
                        ->where('te._semester',$semester)
                        ->where('te.term_id',$term)
                        ->where('te.school_year_id',$sc_year)
                        ->groupby('ted.student_id')
                        ->get();
                    $assPercentage = $getCourseDetails->assignment / 100;
                    $assPs = $getAssTotal->totalscore;

                    foreach($getStudentAss as $gst){
                        $assScore = $gst->score;
                        $computeInitial = ($assScore * 40) / $assPs;
                        $addInitial = $computeInitial + 55;
                        $finalTermGrade = $addInitial * $assPercentage;

                        $addTerm = new GradeDetails;
                        $addTerm->grade_id = $gradeid;
                        $addTerm->student_id = $gst->student_id;
                        $addTerm->category = 3;
                        $addTerm->grade = $finalTermGrade;
                        $addTerm->save();
                    }

                }
                // A T T E N D A N C E
                if ($getCourseDetails->attendance !=0 || $getCourseDetails->attendance != NULL) {

                    $getAttTotal = AttendanceScore::select(DB::raw('SUM(totalscore) as totalscore'))
                        ->where('teacher_id',$teacherid)
                        ->where('course_id',$course)
                        ->where('subject_id',$subject)
                        ->where('year_id',$year)
                        ->where('section_id',$section)
                        ->where('_semester',$semester)
                        ->where('school_year_id',$sc_year)
                        ->where('term_id',$term)->first();

                    $getStudentAtt = DB::table('attendance_score as te')
                        ->select(DB::raw('SUM(ted.score) as score'),'ted.student_id')
                        ->leftjoin('attendance_score_details as ted','te.id','=','ted.attendance_score_id')
                        ->where('te.teacher_id',$teacherid)
                        ->where('te.course_id',$course)
                        ->where('te.subject_id',$subject)
                        ->where('te.year_id',$year)
                        ->where('te.section_id',$section)
                        ->where('te._semester',$semester)
                        ->where('te.term_id',$term)
                        ->where('te.school_year_id',$sc_year)
                        ->groupby('ted.student_id')
                        ->get();
                    $attPercentage = $getCourseDetails->attendance / 100;
                    $attPs = $getAttTotal->totalscore;
                        
                    foreach($getStudentAtt as $gst){
                        $attScore = $gst->score;
                        $computeInitial = ($attScore * 40) / $attPs;
                        $addInitial = $computeInitial + 55;
                        $finalTermGrade = $addInitial * $attPercentage;

                        $addTerm = new GradeDetails;
                        $addTerm->grade_id = $gradeid;
                        $addTerm->student_id = $gst->student_id;
                        $addTerm->category = 4;
                        $addTerm->grade = $finalTermGrade;
                        $addTerm->save();
                    }
                    

                }
                // P A R T I C I P A T I O N   H E R E 

                $getAllPartialGrades = GradeDetails::select(DB::raw('SUM(grade) as sgrade'),'student_id')
                        ->where('grade_id',$gradeid)
                        ->groupby('student_id')
                        ->get();
                foreach ($getAllPartialGrades as $gapg) {
                    // $sEqui1 = ($gapg->sgrade / $overAll) * 100;
                    // $f = number_format($sEqui1, 0, '.', '');
                    // $fixedEquivalent = (int)$f;
                    // // $fixedEquivalent = round($sEqui1);
                    $fixedEquivalent = round($gapg->sgrade,0);
                    $finalGrade = 0;

                    if ($fixedEquivalent >=95) {
                        $finalGrade = 1.0;
                    }else if($fixedEquivalent <= 55){
                        $finalGrade = 5.0;
                    }else{

                        $finalEquivalent = GradeEquivalent::where('grade',$fixedEquivalent)->first();
                        $finalGrade = $finalEquivalent->equivalent;
                    }
                    $subjectGrade = new SubjectFinalGrade;
                    $subjectGrade->grade_id = $gradeid;
                    $subjectGrade->student_id = $gapg->student_id;
                    $subjectGrade->subject_grade = $gapg->sgrade;
                    $subjectGrade->subject_equivalent = $fixedEquivalent;
                    $subjectGrade->final_equivalent = $finalGrade;
                    $subjectGrade->save();

                    // $request->term 
            }
        }
    }
    private function finalGradeComputation($course,$subject,$year,$section,$semester,$term,$sc_year,$teacherid){
        $termName = ($term == 2) ? 'Mid Term' : 'Final Term';
        $find = Grades::where('course_id',$course)
            ->where('subject_id',$subject)
            ->where('year_id',$year)
            ->where('section_id',$section)
            ->where('_semester',$semester)
            ->where('term_id',$term)
            ->where('school_year_id',$sc_year)
            ->first();
            if (!is_null($find)) {
                //category
                // 1 - term exam
                // 2 - quiz
                // 3 - assignment
                // 5 - paticipation/performance
                // 4 - attendance
                $getCourseDetails = DB::table('courses_details as cd')
                    ->select('term_exam','quiz','assignment','attendance','participation')
                    ->where('course_id',$course)
                    ->where('subject_id',$subject)
                    ->where('year_id',$year)
                    ->where('section_id',$section)
                    ->where('_semester',$semester)
                    ->where('teacher_id',$teacherid)
                    ->first();
                 if (is_null($getCourseDetails)) {
                    // abort(415,"Cannot Proceed, Please Check your inputs");
                    return "Cannot Proceed, Please Check your inputs";
                }
                
                //E X A M
                $termScore = 0;
                $quizScore = 0;
                $assScore = 0;
                $attScore = 0;
                $termDetails = 0;
                $quizDetails = 0;
                $assDetails = 0;
                $attDetails = 0;

                if ($getCourseDetails->term_exam !=0 || $getCourseDetails->term_exam != NULL) {
                    $check = TermExam::where('teacher_id',$teacherid)
                        ->where('course_id',$course)
                        ->where('subject_id',$subject)
                        ->where('year_id',$year)
                        ->where('section_id',$section)
                        ->where('_semester',$semester)
                        ->where('term_id',$term)
                        ->where('school_year_id',$sc_year)
                        ->first();
                    if(is_null($check)){
                        // abort(415,"Sorry, Cannot Proceeed, Please check ur input");
                        return "Sorry, Cannot Proceeed, Empty Exam for ".$termName;
                    }
                    $getTermTotal = TermExam::select(DB::raw('SUM(totalscore) as totalscore'))
                        ->where('teacher_id',$teacherid)
                        ->where('course_id',$course)
                        ->where('subject_id',$subject)
                        ->where('year_id',$year)
                        ->where('section_id',$section)
                        ->where('_semester',$semester)
                        ->where('term_id',$term)
                        ->where('school_year_id',$sc_year)
                        ->first();

                    $getStudentsTerm = DB::table('term_exam as te')
                        ->select(DB::raw('SUM(ted.score) as score'),'ted.student_id')
                        ->leftjoin('term_exam_details as ted','te.id','=','ted.term_id')
                        ->where('te.teacher_id',$teacherid)
                        ->where('te.course_id',$course)
                        ->where('te.subject_id',$subject)
                        ->where('te.year_id',$year)
                        ->where('te.section_id',$section)
                        ->where('te._semester',$semester)
                        ->where('te.term_id',$term)
                        ->where('te.school_year_id',$sc_year)
                        ->groupby('ted.student_id')
                        ->get();
                    $termPercentage = $getCourseDetails->term_exam / 100;
                    $termPs = $getTermTotal->totalscore;

                    foreach($getStudentsTerm as $gst){
                        // $termScore = $gst->score * $termPercentage;
                        $termScore = $gst->score;
                        $computeInitial = ($termScore * 40) / $termPs;
                        $addInitial = $computeInitial + 55;
                        $finalTermGrade = $addInitial * $termPercentage;
                        
                        $addTerm = GradeDetails::where('grade_id',$find->id)
                            ->where('student_id',$gst->student_id)
                            ->where('category',1)->first();
                        if (!is_null($addTerm)) {
                            $addTerm->grade = $finalTermGrade;
                            $addTerm->update();
                        }else{
                            $addNewTerm = new GradeDetails;
                            $addNewTerm->grade_id = $find->id;
                            $addNewTerm->student_id = $gst->student_id;
                            $addNewTerm->category = 1;
                            $addNewTerm->grade = $finalTermGrade;
                            $addNewTerm->save();
                            
                        }
                        
                    }
                }


                //Q U I Z
                if ($getCourseDetails->quiz !=0 || $getCourseDetails->quiz != NULL) {
                    
                    $getQuizTotal = Quiz::select(DB::raw('SUM(totalscore) as totalscore'))
                        ->where('teacher_id',$teacherid)
                        ->where('course_id',$course)
                        ->where('subject_id',$subject)
                        ->where('year_id',$year)
                        ->where('section_id',$section)
                        ->where('_semester',$semester)
                        ->where('school_year_id',$sc_year)
                        ->where('term_id',$term)->first();

                    $getStudentsQuiz = DB::table('quizes as te')
                        ->select(DB::raw('SUM(ted.score) as score'),'ted.student_id')
                        ->leftjoin('quiz_details as ted','te.id','=','ted.quiz_id')
                        ->where('te.teacher_id',$teacherid)
                        ->where('te.course_id',$course)
                        ->where('te.subject_id',$subject)
                        ->where('te.year_id',$year)
                        ->where('te.section_id',$section)
                        ->where('te._semester',$semester)
                        ->where('te.term_id',$term)
                        ->where('te.school_year_id',$sc_year)
                        ->groupby('ted.student_id')
                        ->get();
                    $quizPercentage = $getCourseDetails->quiz / 100;
                    $quizPs = $getQuizTotal->totalscore;

                    foreach($getStudentsQuiz as $gst){
                        $quizScore = $gst->score;
                        $computeInitial = ($quizScore * 40) / $quizPs;
                        $addInitial = $computeInitial + 55;
                        $finalQuizGrade = $addInitial * $quizPercentage;

                        $addTerm = GradeDetails::where('grade_id',$find->id)
                            ->where('student_id',$gst->student_id)
                            ->where('category',2)->first();
                        if (!is_null($addTerm)) {
                            $addTerm->grade = $finalQuizGrade;
                            $addTerm->update();
                        }else{
                            $addNewTerm = new GradeDetails;
                            $addNewTerm->grade_id = $find->id;
                            $addNewTerm->student_id = $gst->student_id;
                            $addNewTerm->category = 2;
                            $addNewTerm->grade = $finalQuizGrade;
                            $addNewTerm->save();
                            
                        }
                    }
                }
                //A S S I G N M E N T 

                if ($getCourseDetails->assignment !=0 || $getCourseDetails->assignment != NULL) {
                    
                    $getAssTotal = Assignment::select(DB::raw('SUM(totalscore) as totalscore'))
                        ->where('teacher_id',$teacherid)
                        ->where('course_id',$course)
                        ->where('subject_id',$subject)
                        ->where('year_id',$year)
                        ->where('section_id',$section)
                        ->where('_semester',$semester)
                        ->where('school_year_id',$sc_year)
                        ->where('term_id',$term)->first();

                    $getStudentAss = DB::table('assignment as te')
                        ->select(DB::raw('SUM(ted.score) as score'),'ted.student_id')
                        ->leftjoin('assignment_details as ted','te.id','=','ted.assignment_id')
                        ->where('te.teacher_id',$teacherid)
                        ->where('te.course_id',$course)
                        ->where('te.subject_id',$subject)
                        ->where('te.year_id',$year)
                        ->where('te.section_id',$section)
                        ->where('te._semester',$semester)
                        ->where('te.term_id',$term)
                        ->where('te.school_year_id',$sc_year)
                        ->groupby('ted.student_id')
                        ->get();
                    $assPercentage = $getCourseDetails->assignment / 100;
                    $assPs = $getAssTotal->totalscore;

                    foreach($getStudentAss as $gst){
                        $assScore = $gst->score;
                        $computeInitial = ($assScore * 40) / $assPs;
                        $addInitial = $computeInitial + 55;
                        $finalAssGrade = $addInitial * $assPercentage;

                        $addTerm = GradeDetails::where('grade_id',$find->id)
                            ->where('student_id',$gst->student_id)
                            ->where('category',3)->first();
                        if (!is_null($addTerm)) {
                            $addTerm->grade = $finalAssGrade;
                            $addTerm->update();
                        }else{
                            $addNewTerm = new GradeDetails;
                            $addNewTerm->grade_id = $find->id;
                            $addNewTerm->student_id = $gst->student_id;
                            $addNewTerm->category = 3;
                            $addNewTerm->grade = $finalAssGrade;
                            $addNewTerm->save();
                            
                        }
                    }

                }
                // A T T E N D A N C E
                if ($getCourseDetails->attendance !=0 || $getCourseDetails->attendance != NULL) {

                    $getAttTotal = AttendanceScore::select(DB::raw('SUM(totalscore) as totalscore'))
                        ->where('teacher_id',$teacherid)
                        ->where('course_id',$course)
                        ->where('subject_id',$subject)
                        ->where('year_id',$year)
                        ->where('section_id',$section)
                        ->where('_semester',$semester)
                        ->where('school_year_id',$sc_year)
                        ->where('term_id',$term)->first();

                    $getStudentAtt = DB::table('attendance_score as te')
                        ->select(DB::raw('SUM(ted.score) as score'),'ted.student_id')
                        ->leftjoin('attendance_score_details as ted','te.id','=','ted.attendance_score_id')
                        ->where('te.teacher_id',$teacherid)
                        ->where('te.course_id',$course)
                        ->where('te.subject_id',$subject)
                        ->where('te.year_id',$year)
                        ->where('te.section_id',$section)
                        ->where('te._semester',$semester)
                        ->where('te.term_id',$term)
                        ->where('te.school_year_id',$sc_year)
                        ->groupby('ted.student_id')
                        ->get();
                    $attPercentage = $getCourseDetails->attendance / 100;
                    $attPs = $getAttTotal->totalscore;
                        
                    foreach($getStudentAtt as $gst){
                        $attScore = $gst->score;
                        $computeInitial = ($attScore * 40) / $attPs;
                        $addInitial = $computeInitial + 55;
                        $finalAttGrade = $addInitial * $attPercentage;

                        $addTerm = GradeDetails::where('grade_id',$find->id)
                            ->where('student_id',$gst->student_id)
                            ->where('category',4)->first();
                        if (!is_null($addTerm)) {
                            $addTerm->grade = $finalAttGrade;
                            $addTerm->update();
                        }else{
                            $addTerm = new GradeDetails;
                            $addTerm->grade_id = $find->id;
                            $addTerm->student_id = $gst->student_id;
                            $addTerm->category = 4;
                            $addTerm->grade = $finalAttGrade;
                            $addTerm->save();
                        }
                        
                    }
                    

                }
                 $getAllPartialGrades = GradeDetails::select(DB::raw('SUM(grade) as sgrade'),'student_id')
                        ->where('grade_id',$find->id)
                        ->groupby('student_id')
                        ->get();
                foreach ($getAllPartialGrades as $gapg) {
                    $fixedEquivalent = round($gapg->sgrade,0);
                    $findStudentInitial='';
                    if ($term == 2) {
                        $findStudentInitial = Grades::select('sfg.subject_equivalent as IMG')
                        ->join('subject_final_grade as sfg','sfg.grade_id','=','grades.id')
                        ->where('grades.subject_id',$subject)
                        ->where('grades.year_id',$year)
                        ->where('grades.term_id',1)
                        ->where('grades.school_year_id',$sc_year)
                        ->where('grades._semester',$semester)
                        ->where('sfg.student_id',$gapg->student_id)->first();
                    }else{
                        $findStudentInitial = Grades::select('sfg.subject_equivalent as IMG')
                        ->join('subject_final_grade as sfg','sfg.grade_id','=','grades.id')
                        ->where('grades.subject_id',$subject)
                        ->where('grades.year_id',$year)
                        ->where('grades.term_id',3)
                        ->where('grades.school_year_id',$sc_year)
                        ->where('grades._semester',$semester)
                        ->where('sfg.student_id',$gapg->student_id)->first();
                    }
                    $computeFMG = ((1/3)*$findStudentInitial->IMG) + ((2/3)*$fixedEquivalent);
                    $FMG = round($computeFMG,0);
                    $FMGEquivalent = 0;
                    if ($FMG >=95) {
                        $FMGEquivalent = 1.0;
                    }else if($FMG <= 50){
                        $FMGEquivalent = 5.0;
                    }else{

                        $finalEquivalent = GradeEquivalent::where('grade',$FMG)->first();
                        $FMGEquivalent = $finalEquivalent->equivalent;
                    }
                    
                    $findSubjectFinalGrade = SubjectFinalGrade::where('grade_id',$find->id)->where('student_id',$gapg->student_id)->first();

                    if (!is_null($findSubjectFinalGrade)) {
                        $findSubjectFinalGrade->subject_grade = $gapg->sgrade;
                        $findSubjectFinalGrade->subject_equivalent = $FMG;
                        $findSubjectFinalGrade->final_equivalent = $FMGEquivalent;
                        $findSubjectFinalGrade->update();
                    }else{
                        $subjectGrade = new SubjectFinalGrade;
                        $subjectGrade->grade_id = $find->id;
                        $subjectGrade->student_id = $gapg->student_id;
                        $subjectGrade->subject_grade = $gapg->sgrade;
                        $subjectGrade->subject_equivalent = $FMG;
                        $subjectGrade->final_equivalent = $FMGEquivalent;
                        $subjectGrade->save();
                    }
                    
                }
            
            }else{
               //category
            // 1 - term exam
            // 2 - quiz
            // 3 - assignment
            // 4 - paticipation/performance
            // 5 - attendance
                $getCourseDetails = DB::table('courses_details as cd')
                    ->select('term_exam','quiz','assignment','attendance','participation')
                    ->where('course_id',$course)
                    ->where('subject_id',$subject)
                    ->where('year_id',$year)
                    ->where('section_id',$section)
                    ->where('_semester',$semester)
                    ->where('teacher_id',$teacherid)
                    ->first();
                if (is_null($getCourseDetails)) {
                    return "Cannot Proceed, Please contact the admin for course criteria";
                }
                $termScore = 0;
                $quizScore = 0;
                $assScore = 0;
                $attScore = 0;
                $termDetails = 0;
                $quizDetails = 0;
                $assDetails = 0;
                $attDetails = 0;

                $add = new Grades;
                $add->course_id = $course;
                $add->subject_id = $subject;
                $add->year_id = $year;
                $add->section_id = $section;
                $add->_semester = $semester;
                $add->term_id = $term;
                $add->school_year_id = $sc_year;
                $add->save();
                $gradeid = $add->id;

                if ($getCourseDetails->term_exam !=0 || $getCourseDetails->term_exam != NULL) {
                    
                    $getTermTotal = TermExam::select(DB::raw('SUM(totalscore) as totalscore'))
                        ->where('teacher_id',$teacherid)
                        ->where('course_id',$course)
                        ->where('subject_id',$subject)
                        ->where('year_id',$year)
                        ->where('section_id',$section)
                        ->where('_semester',$semester)
                        ->where('school_year_id',$sc_year)
                        ->where('term_id',$term)->first();

                    $getStudentsTerm = DB::table('term_exam as te')
                        ->select(DB::raw('SUM(ted.score) as score'),'ted.student_id')
                        ->leftjoin('term_exam_details as ted','te.id','=','ted.term_id')
                        ->where('te.teacher_id',$teacherid)
                        ->where('te.course_id',$course)
                        ->where('te.subject_id',$subject)
                        ->where('te.year_id',$year)
                        ->where('te.section_id',$section)
                        ->where('te._semester',$semester)
                        ->where('te.term_id',$term)
                        ->where('te.school_year_id',$sc_year)
                        ->groupby('ted.student_id')
                        ->get();
                    $termPercentage = $getCourseDetails->term_exam / 100;
                    $termPs = $getTermTotal->totalscore;

                    foreach($getStudentsTerm as $gst){
                        $termScore = $gst->score;
                        $computeInitial = ($termScore * 40) / $termPs;
                        $addInitial = $computeInitial + 55;
                        $finalTermGrade = $addInitial * $termPercentage;

                        $addTerm = new GradeDetails;
                        $addTerm->grade_id = $gradeid;
                        $addTerm->student_id = $gst->student_id;
                        $addTerm->category = 1;
                        $addTerm->grade = $finalTermGrade;
                        $addTerm->save();
                    }
                   
                }

                // Q U I Z
                if ($getCourseDetails->quiz !=0 || $getCourseDetails->quiz != NULL) {

                    $getQuizTotal = Quiz::select(DB::raw('SUM(totalscore) as totalscore'))
                        ->where('teacher_id',$teacherid)
                        ->where('course_id',$course)
                        ->where('subject_id',$subject)
                        ->where('year_id',$year)
                        ->where('section_id',$section)
                        ->where('_semester',$semester)
                        ->where('school_year_id',$sc_year)
                        ->where('term_id',$term)->first();

                    $getStudentsQuiz = DB::table('quizes as te')
                        ->select(DB::raw('SUM(ted.score) as score'),'ted.student_id')
                        ->leftjoin('quiz_details as ted','te.id','=','ted.quiz_id')
                        ->where('te.teacher_id',$teacherid)
                        ->where('te.course_id',$course)
                        ->where('te.subject_id',$subject)
                        ->where('te.year_id',$year)
                        ->where('te.section_id',$section)
                        ->where('te._semester',$semester)
                        ->where('te.term_id',$term)
                        ->where('te.school_year_id',$sc_year)
                        ->groupby('ted.student_id')
                        ->get();
                    $quizPercentage = $getCourseDetails->quiz / 100;
                    $quizPs = $getQuizTotal->totalscore;

                    foreach($getStudentsQuiz as $gst){
                        $quizScore = $gst->score;
                        $computeInitial = ($quizScore * 40) / $quizPs;
                        $addInitial = $computeInitial + 55;
                        $finalTermGrade = $addInitial * $quizPercentage;

                        $addTerm = new GradeDetails;
                        $addTerm->grade_id = $gradeid;
                        $addTerm->student_id = $gst->student_id;
                        $addTerm->category = 2;
                        $addTerm->grade = $finalTermGrade;
                        $addTerm->save();
                        
                    }
                   
                }

                //A S S I G N M E N T
                
                if ($getCourseDetails->assignment !=0 || $getCourseDetails->assignment != NULL) {

                    $getAssTotal = Assignment::select(DB::raw('SUM(totalscore) as totalscore'))
                        ->where('teacher_id',$teacherid)
                        ->where('course_id',$course)
                        ->where('subject_id',$subject)
                        ->where('year_id',$year)
                        ->where('section_id',$section)
                        ->where('_semester',$semester)
                        ->where('school_year_id',$sc_year)
                        ->where('term_id',$term)->first();

                    $getStudentAss = DB::table('assignment as te')
                        ->select(DB::raw('SUM(ted.score) as score'),'ted.student_id')
                        ->leftjoin('assignment_details as ted','te.id','=','ted.assignment_id')
                        ->where('te.teacher_id',$teacherid)
                        ->where('te.course_id',$course)
                        ->where('te.subject_id',$subject)
                        ->where('te.year_id',$year)
                        ->where('te.section_id',$section)
                        ->where('te._semester',$semester)
                        ->where('te.term_id',$term)
                        ->where('te.school_year_id',$sc_year)
                        ->groupby('ted.student_id')
                        ->get();
                    $assPercentage = $getCourseDetails->assignment / 100;
                    $assPs = $getAssTotal->totalscore;

                    foreach($getStudentAss as $gst){
                        $assScore = $gst->score;
                        $computeInitial = ($assScore * 40) / $assPs;
                        $addInitial = $computeInitial + 55;
                        $finalTermGrade = $addInitial * $assPercentage;

                        $addTerm = new GradeDetails;
                        $addTerm->grade_id = $gradeid;
                        $addTerm->student_id = $gst->student_id;
                        $addTerm->category = 3;
                        $addTerm->grade = $finalTermGrade;
                        $addTerm->save();
                    }

                }
                // A T T E N D A N C E
                if ($getCourseDetails->attendance !=0 || $getCourseDetails->attendance != NULL) {

                    $getAttTotal = AttendanceScore::select(DB::raw('SUM(totalscore) as totalscore'))
                        ->where('teacher_id',$teacherid)
                        ->where('course_id',$course)
                        ->where('subject_id',$subject)
                        ->where('year_id',$year)
                        ->where('section_id',$section)
                        ->where('_semester',$semester)
                        ->where('school_year_id',$sc_year)
                        ->where('term_id',$term)->first();

                    $getStudentAtt = DB::table('attendance_score as te')
                        ->select(DB::raw('SUM(ted.score) as score'),'ted.student_id')
                        ->leftjoin('attendance_score_details as ted','te.id','=','ted.attendance_score_id')
                        ->where('te.teacher_id',$teacherid)
                        ->where('te.course_id',$course)
                        ->where('te.subject_id',$subject)
                        ->where('te.year_id',$year)
                        ->where('te.section_id',$section)
                        ->where('te._semester',$semester)
                        ->where('te.term_id',$term)
                        ->where('te.school_year_id',$sc_year)
                        ->groupby('ted.student_id')
                        ->get();
                    $attPercentage = $getCourseDetails->attendance / 100;
                    $attPs = $getAttTotal->totalscore;
                        
                    foreach($getStudentAtt as $gst){
                        $attScore = $gst->score;
                        $computeInitial = ($attScore * 40) / $attPs;
                        $addInitial = $computeInitial + 55;
                        $finalTermGrade = $addInitial * $attPercentage;

                        $addTerm = new GradeDetails;
                        $addTerm->grade_id = $gradeid;
                        $addTerm->student_id = $gst->student_id;
                        $addTerm->category = 4;
                        $addTerm->grade = $finalTermGrade;
                        $addTerm->save();
                    }
                    

                }
                // P A R T I C I P A T I O N   H E R E 

                $getAllPartialGrades = GradeDetails::select(DB::raw('SUM(grade) as sgrade'),'student_id')
                        ->where('grade_id',$gradeid)
                        ->groupby('student_id')
                        ->get();
 
                foreach ($getAllPartialGrades as $gapg) {
                    $fixedEquivalent = round($gapg->sgrade,0);
                    $findStudentInitial='';
                    if ($term == 2) {
                        $findStudentInitial = Grades::select('sfg.subject_equivalent as IMG')
                        ->join('subject_final_grade as sfg','sfg.grade_id','=','grades.id')
                        ->where('grades.subject_id',$subject)
                        ->where('grades.year_id',$year)
                        ->where('grades.term_id',1)
                        ->where('grades.school_year_id',$sc_year)
                        ->where('grades._semester',$semester)
                        ->where('sfg.student_id',$gapg->student_id)->first();
                    }else{
                        $findStudentInitial = Grades::select('sfg.subject_equivalent as IMG')
                        ->join('subject_final_grade as sfg','sfg.grade_id','=','grades.id')
                        ->where('grades.subject_id',$subject)
                        ->where('grades.year_id',$year)
                        ->where('grades.term_id',3)
                        ->where('grades.school_year_id',$sc_year)
                        ->where('grades._semester',$semester)
                        ->where('sfg.student_id',$gapg->student_id)->first();
                    }
                    $computeFMG = ((1/3)*$findStudentInitial->IMG) + ((2/3)*$fixedEquivalent);
                    $FMG = round($computeFMG,0);
                    $FMGEquivalent = 0;

                    if ($FMG >=95) {
                        $FMGEquivalent = 1.0;
                    }else if($FMG <= 55){
                        $FMGEquivalent = 5.0;
                    }else{

                        $finalEquivalent = GradeEquivalent::where('grade',$FMG)->first();
                        $FMGEquivalent = $finalEquivalent->equivalent;
                    }
                    $subjectGrade = new SubjectFinalGrade;
                    $subjectGrade->grade_id = $gradeid;
                    $subjectGrade->student_id = $gapg->student_id;
                    $subjectGrade->subject_grade = $gapg->sgrade;
                    $subjectGrade->subject_equivalent = $FMG;
                    $subjectGrade->final_equivalent = $FMGEquivalent;
                    $subjectGrade->save();

                    // $term 
            }
        }

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function viewStudentsGrade($studentid,$subjectgradeid){
        // dd(1);
        $student = StudentDetails::where('student_id',$studentid)->first();
        $term = SubjectFinalGrade::find($subjectgradeid);
        
        return view('grade_details.students.index',compact('student','subjectgradeid','term'));
    }
    public function getStudentsTermScore($studentid,$subjectgradeid){
        $grade = SubjectFinalGrade::find($subjectgradeid);
        $getTermGrade = Grades::find($grade->grade_id);

        $getTerm = DB::table('term_exam as te')
            ->select('te.title','te.totalscore','ted.score')
            ->leftjoin('term_exam_details as ted','te.id','ted.term_id')
            ->where('ted.student_id',$studentid)
            ->where('te.course_id',$getTermGrade->course_id)
            ->where('te.subject_id',$getTermGrade->subject_id)
            ->where('te.year_id',$getTermGrade->year_id)
            ->where('te._semester',$getTermGrade->_semester)
            ->where('te.term_id',$getTermGrade->term_id)
            ->where('te.teacher_id',Auth::user()->teacher_id);
       return DataTables::of($getTerm)
        ->make(true);
    }

    public function getStudentsQuizScore($studentid,$subjectgradeid){
        $grade = SubjectFinalGrade::find($subjectgradeid);
        $getTermGrade = Grades::find($grade->grade_id);

        $getQuiz = DB::table('quizes as te')
            ->select('te.title','te.totalscore','ted.score')
            ->leftjoin('quiz_details as ted','te.id','ted.quiz_id')
            ->where('ted.student_id',$studentid)
            ->where('te.course_id',$getTermGrade->course_id)
            ->where('te.subject_id',$getTermGrade->subject_id)
            ->where('te.year_id',$getTermGrade->year_id)
            ->where('te._semester',$getTermGrade->_semester)
            ->where('te.term_id',$getTermGrade->term_id)
            ->where('te.teacher_id',Auth::user()->teacher_id);
       return DataTables::of($getQuiz)
        ->make(true);
    }
    public function getStudentsAssScore($studentid,$subjectgradeid){
        $grade = SubjectFinalGrade::find($subjectgradeid);
        $getTermGrade = Grades::find($grade->grade_id);

        $getQuiz = DB::table('assignment as te')
            ->select('te.title','te.totalscore','ted.score')
            ->leftjoin('assignment_details as ted','te.id','ted.assignment_id')
            ->where('ted.student_id',$studentid)
            ->where('te.course_id',$getTermGrade->course_id)
            ->where('te.subject_id',$getTermGrade->subject_id)
            ->where('te.year_id',$getTermGrade->year_id)
            ->where('te._semester',$getTermGrade->_semester)
            ->where('te.term_id',$getTermGrade->term_id)
            ->where('te.teacher_id',Auth::user()->teacher_id);
       return DataTables::of($getQuiz)
        ->make(true);
    }

    public function getStudentsAttendanceScore($studentid,$subjectgradeid){
        $grade = SubjectFinalGrade::find($subjectgradeid);
        $getTermGrade = Grades::find($grade->grade_id);

        $getQuiz = DB::table('attendance_score as atts')
            ->select(
                'atts.title',
                'atts.totalscore',
                'attsd.score'
            )
            ->join('attendance_score_details as attsd','attsd.attendance_score_id','=','atts.id')
            ->where('attsd.student_id',$studentid)
            ->where('atts.course_id',$getTermGrade->course_id)
            ->where('atts.subject_id',$getTermGrade->subject_id)
            ->where('atts.year_id',$getTermGrade->year_id)
            ->where('atts._semester',$getTermGrade->_semester)
            ->where('atts.term_id',$getTermGrade->term_id)
            ->where('atts.teacher_id',Auth::user()->teacher_id);

       return DataTables::of($getQuiz)
        ->make(true);
    }
    
    public function getStudentSubjectGrade($courseid,$subjectid,$yearid,$sectionid,$semesterid,$termid,$sc_year){
        
        $getStudentGrade = DB::table('subject_final_grade as sfg')
            ->select('s.first_name','s.last_name','sfg.final_equivalent as grade','g.term_id','sfg.id','s.id as studentid')
            ->leftjoin('grades as g','sfg.grade_id','=','g.id')
            ->leftjoin('students as s','sfg.student_id','=','s.id')
            ->where('g.course_id',$courseid)
            ->where('g.subject_id',$subjectid)
            ->where('g.year_id',$yearid)
            ->where('g.section_id',$sectionid)
            ->where('g._semester',$semesterid)
            ->where('g.school_year_id',$sc_year)
            ->where('g.term_id',$termid);
        return Datatables::of($getStudentGrade)
            ->addColumn('action',function($grade){
                return '<a href="gradedetails/student/'.$grade->studentid.'/'.$grade->id.'/view" class="btn btn-success btn-round btn-sm"> View </a>';
            })
            ->editColumn('term',function($grade){
                $term = '';
                if ($grade->term_id == 1) {
                   $term = 'Prelim';
                }else if ($grade->term_id == 2) {
                    $term = 'Mid Term';
                }else if ($grade->term_id == 3) {
                    $term = 'Semi-Final';
                }else{
                    $term = 'Final';
                }
                return $term;
            })
            ->make(true);
    }
    public function getStudentSemGrade($courseid,$subjectid,$yearid,$sectionid,$semesterid,$sc_year){
        
        $getStudentGrade = DB::table('semester_grade_details as sfg')
            ->select('s.first_name','s.last_name','sfg.semester_grade as grade','sfg.id','s.id as studentid')
            ->leftjoin('semester_grade as g','sfg.sem_grade_id','=','g.id')
            ->leftjoin('students as s','sfg.student_id','=','s.id')
            ->where('g.course_id',$courseid)
            ->where('g.subject_id',$subjectid)
            ->where('g.year_id',$yearid)
            ->where('g.section_id',$sectionid)
            ->where('g._semester',$semesterid)
            ->where('g.school_year_id',$sc_year);
        return Datatables::of($getStudentGrade)
            // ->addColumn('action',function($grade){
            //     return '<a href="gradedetails/student/'.$grade->studentid.'/'.$grade->id.'/view" class="btn btn-success btn-round btn-sm"> View </a>';
            // })
            ->editColumn('studentid',function($grade)use($semesterid){
                $term = '';
                if ($semesterid == 1) {
                   $term = '1st';
                }else{
                    $term = '2nd';
                }
                return $term;
            })
            ->make(true);
    }
    public function individualStudentGrade(){
        $student_id = Auth::user()->student_id;
        $getGrade = DB::table('semester_grade as sg')
            ->select('s.name as subject','y.code as year','sg._semester as semester','semester_grade as final_equivalent','sg.id as id')
            ->leftjoin('semester_grade_details as sgd','sg.id','=','sgd.sem_grade_id')
            ->leftjoin('subjects as s','sg.subject_id','=','s.id')
            ->leftjoin('years as y','sg.year_id','=','y.id')
            ->where('sgd.student_id',$student_id);
        // dd($getGrade);
        // $getGrade = SubjectFinalGrade::with('grades')->where('student_id',$student_final)

        return Datatables::of($getGrade)
            ->addColumn('action',function($grade){
                return '<a class="btn btn-success btn-round btn-sm" href="studentsgrade/'.$grade->id.'/view"> View </a>';
            })
            ->editColumn('semester',function($grade){
                $sem = ($grade->semester == 1) ? "1st" : "2nd";
                
                return $sem;
            })
            
            ->make(true);
    }

    public function individualStudentGradeTerm(){
        $student_id = Auth::user()->student_id;

        $getGrade = DB::table('grades as sg')
            ->select('s.name as subject','y.code as year','sg._semester as semester','sgd.final_equivalent','sg.id as id','sg.term_id as term')
            ->leftjoin('subject_final_grade as sgd','sg.id','=','sgd.grade_id')
            ->leftjoin('subjects as s','sg.subject_id','=','s.id')
            ->leftjoin('years as y','sg.year_id','=','y.id')
            ->where('sgd.student_id',$student_id);
        // dd($getGrade);
        // $getGrade = SubjectFinalGrade::with('grades')->where('student_id',$student_final)

        return Datatables::of($getGrade)
            
            ->editColumn('semester',function($grade){
                $sem = ($grade->semester == 1) ? "1st" : "2nd";
                return $sem;
            })
            ->editColumn('term',function($grade){
                // $sem = ($grade->term == 1) ? "Mid" : "Final";
                // $sem = "";
                if($grade->term == 1){
                    $sem = 'Prelim';
                }elseif ($grade->term == 2) {
                    $sem = 'Mid';
                }elseif ($grade->term == 3) {
                    $sem = 'Semi-final';
                }else{
                    $sem = 'Final';
                }
                return $sem;
            })
            
            ->make(true);
    }
    public function studentGradeBreakDown ($semestergradeid){
        // dd($semestergradeid);

        $studentid = Auth::user()->student_id;
        $grade = SemesterGrade::find($semestergradeid);
        $finalgrade = SemesterGradeDetails::where('student_id',$studentid)->first();
        $courseDetails = CourseDetails::where('course_id',$grade->course_id)
            ->where('subject_id',$grade->subject_id)->first();
        return view('students.grades.index',compact('grade','finalgrade','courseDetails'));
    }
    
    public function getstudentAttendanceGradeBreakDown($gradeid){
        // dd($gradeid);
        $studentid = Auth::user()->student_id;
        $grade = SemesterGrade::find($gradeid);

        // $getScore = TermExam::select('term_exam.term_id as term','term_exam.title','term_exam.totalscore','ted.score')
        //     ->leftjoin('term_exam_details as ted','term_exam.id','=','ted.term_id')
        //     ->where('term_exam.course_id',$grade->course_id)
        //     ->where('term_exam.subject_id',$grade->subject_id)
        //     ->where('term_exam.year_id',$grade->year_id)
        //     ->where('term_exam.section_id',$grade->section_id)
        //     ->where('term_exam._semester',$grade->_semester)
        //     ->where('ted.student_id',$studentid);

        $getAttendance= DB::table('attendance_score as as')
            ->select('as.term_id as term','as.totalscore','as.title','asd.score as score')
            ->leftjoin('attendance_score_details as asd','asd.attendance_score_id','=','as.id')
            ->where('as.course_id',$grade->course_id)
            ->where('as.subject_id',$grade->subject_id)
            ->where('as.year_id',$grade->year_id)
            ->where('as.section_id',$grade->section_id)
            ->where('as._semester',$grade->_semester)
            ->where('asd.student_id',$studentid);

            return Datatables::of($getAttendance)
                ->editColumn('term',function($attendance){
                    $term = '';
                    if($attendance->term == 1){
                    $term = 'Prelim';
                    }elseif ($attendance->term == 2) {
                        $term = 'Mid';
                    }elseif ($attendance->term == 3) {
                        $term = 'Semi-final';
                    }else{
                        $term = 'Final';
                    }
                    return $term;
                })
                ->make(true);
    }
    public function getstudentTermGradeBreakDown ($gradeid){
        // dd($gradeid);
        $studentid = Auth::user()->student_id;
        $grade = SemesterGrade::find($gradeid);

        $getScore = TermExam::select('term_exam.term_id as term','term_exam.title','term_exam.totalscore','ted.score')
            ->leftjoin('term_exam_details as ted','term_exam.id','=','ted.term_id')
            ->where('term_exam.course_id',$grade->course_id)
            ->where('term_exam.subject_id',$grade->subject_id)
            ->where('term_exam.year_id',$grade->year_id)
            ->where('term_exam.section_id',$grade->section_id)
            ->where('term_exam._semester',$grade->_semester)
            ->where('ted.student_id',$studentid);

            return Datatables::of($getScore)
                ->editColumn('term',function($term_exam){
                    $term = '';
                    if($term_exam->term == 1){
                    $term = 'Prelim';
                    }elseif ($term_exam->term == 2) {
                        $term = 'Mid';
                    }elseif ($term_exam->term == 3) {
                        $term = 'Semi-final';
                    }else{
                        $term = 'Final';
                    }
                    return $term;
                })
                ->make(true);
    }
    public function getStudentQuizGradeBreakDown ($gradeid){
        // dd($gradeid);
        $studentid = Auth::user()->student_id;
        $grade = SemesterGrade::find($gradeid);
        $getScore = Quiz::select('quizes.term_id as term','quizes.title','quizes.totalscore','ted.score')
            ->leftjoin('quiz_details as ted','quizes.id','=','ted.quiz_id')
            ->where('quizes.course_id',$grade->course_id)
            ->where('quizes.subject_id',$grade->subject_id)
            ->where('quizes.year_id',$grade->year_id)
            ->where('quizes.section_id',$grade->section_id)
            ->where('quizes._semester',$grade->_semester)
            // ->where('quizes.term_id',$grade->term_id)
            ->where('ted.student_id',$studentid);

            return Datatables::of($getScore)
                ->editColumn('term',function($term_exam){
                    $term = '';
                    if($term_exam->term == 1){
                    $term = 'Prelim';
                    }elseif ($term_exam->term == 2) {
                        $term = 'Mid';
                    }elseif ($term_exam->term == 3) {
                        $term = 'Semi-final';
                    }else{
                        $term = 'Final';
                    }
                    return $term;
                })
                ->make(true);
    }
    public function getStudentAssGradeBreakDown ($gradeid){
        // dd($gradeid);
        $studentid = Auth::user()->student_id;
        $grade = SemesterGrade::find($gradeid);

        $getScore = Assignment::select('assignment.term_id as term','assignment.title','assignment.totalscore','ted.score')
            ->leftjoin('assignment_details as ted','assignment.id','=','ted.assignment_id')
            ->where('assignment.course_id',$grade->course_id)
            ->where('assignment.subject_id',$grade->subject_id)
            ->where('assignment.year_id',$grade->year_id)
            ->where('assignment.section_id',$grade->section_id)
            ->where('assignment._semester',$grade->_semester)
            // ->where('assignment.term_id',$grade->term_id)
            ->where('ted.student_id',$studentid);

            return Datatables::of($getScore)
            ->editColumn('term',function($term_exam){
                    $term = '';
                    if($term_exam->term == 1){
                    $term = 'Prelim';
                    }elseif ($term_exam->term == 2) {
                        $term = 'Mid';
                    }elseif ($term_exam->term == 3) {
                        $term = 'Semi-final';
                    }else{
                        $term = 'Final';
                    }
                    return $term;
                })
                ->make(true);
    }
    public function semestergrade (){
        $usertype = Auth::user()->user_type_id;
        $user = Auth::user();
        $sc = SchoolYear::all();
        if ($usertype == 2) {
            $course = CourseDetails::select('course_id')
                ->with('course')
                ->where('teacher_id',$user->teacher_id)
                ->groupby('course_id')
                ->get();
            return view('grade_details.bysemester',compact('course','sc'));
        }else{
            abort(415, 'Forbidden Access');
        }
    }
    public function storesemgrades(Request $request){
        /* put school year in where and in blade*/

        $findGrade = Grades::where('course_id',$request->course)
            ->join('grade_details as g','g.grade_id','=','grades.id')
            ->where('grades.subject_id',$request->subject)
            ->where('grades.year_id',$request->year)
            ->where('grades.section_id',$request->section)
            ->where('grades._semester',$request->semester)
            ->where('grades.school_year_id',$request->sc_year)
            ->where('grades.term_id',1)
            ->first();
        $findNdGrade = Grades::where('course_id',$request->course)
            ->join('grade_details as g','g.grade_id','=','grades.id')
            ->where('grades.subject_id',$request->subject)
            ->where('grades.year_id',$request->year)
            ->where('grades.section_id',$request->section)
            ->where('grades._semester',$request->semester)
            ->where('grades.school_year_id',$request->sc_year)
            ->where('grades.term_id',2)
            ->first();
        $findsemiGrade = Grades::where('course_id',$request->course)
            ->join('grade_details as g','g.grade_id','=','grades.id')
            ->where('grades.subject_id',$request->subject)
            ->where('grades.year_id',$request->year)
            ->where('grades.section_id',$request->section)
            ->where('grades._semester',$request->semester)
            ->where('grades.school_year_id',$request->sc_year)
            ->where('grades.term_id',3)
            ->first();
        $findfinalGrade = Grades::where('course_id',$request->course)
            ->join('grade_details as g','g.grade_id','=','grades.id')
            ->where('grades.subject_id',$request->subject)
            ->where('grades.year_id',$request->year)
            ->where('grades.section_id',$request->section)
            ->where('grades._semester',$request->semester)
            ->where('grades.school_year_id',$request->sc_year)
            ->where('grades.term_id',4)
            ->first();
        
        if (!is_null($findGrade)) {
            if (!is_null($findNdGrade)) {
                if (!is_null($findsemiGrade)) {
                    if (!is_null($findfinalGrade)) {
                        // $getGrade = DB::table('grades as g')
                        //     ->select('sfg.subject_equivalent','sfg.student_id','g.term_id')
                        //     ->leftjoin('subject_final_grade as sfg','g.id','=','sfg.grade_id')
                        //     ->where('g.course_id',$request->course)
                        //     ->where('subject_id',$request->subject)
                        //     ->where('year_id',$request->year)
                        //     ->where(function($q){
                        //             $q->where('term_id',2)
                        //             ->orwhere('term_id',4);
                        //         })
                        //     ->where('section_id',$request->section)
                        //     ->where('_semester',$request->semester)
                        //     ->where('g.school_year_id',$request->sc_year)
                        //     ->groupby('student_id','subject_equivalent','term_id')
                        //     ->get();
                        $getStudents =  DB::table('student_details as sd')
                            ->select('sd.student_id')
                            ->join('courses_details as cd','sd.course_id','=','cd.course_id')
                            ->where('sd.course_id',$request->course)
                            ->where('sd.school_year_id',$request->sc_year)
                            ->where('cd.subject_id',$request->subject)
                            ->where('cd.year_id',$request->year)
                            ->where('cd.section_id',$request->section)
                            ->where('cd._semester',$request->semester)
                            ->get();
                        // dd($getStudents);


                        $findSemGrade = SemesterGrade::where('course_id',$request->course)
                            ->where('subject_id',$request->subject)
                            ->where('year_id',$request->year)
                            ->where('section_id',$request->section)
                            ->where('_semester',$request->semester)
                            ->where('school_year_id',$request->sc_year)
                            ->first();
                        if (!is_null($findSemGrade)) {
                            foreach ($getStudents as $s) {

                                $findMidGrade = Grades::select('sfg.subject_equivalent')
                                    ->join('subject_final_grade as sfg','sfg.grade_id','=','grades.id')
                                    ->where('grades.course_id',$request->course)
                                    ->where('grades.school_year_id',$request->sc_year)
                                    ->where('grades.subject_id',$request->subject)
                                    ->where('grades.year_id',$request->year)
                                    ->where('grades.section_id',$request->section)
                                    ->where('grades._semester',$request->semester)
                                    ->where('grades.term_id',2)
                                    ->where('sfg.student_id',$s->student_id)->first();
                                $findMidGrade = Grades::select('sfg.subject_equivalent')
                                    ->join('subject_final_grade as sfg','sfg.grade_id','=','grades.id')
                                    ->where('grades.course_id',$request->course)
                                    ->where('grades.school_year_id',$request->sc_year)
                                    ->where('grades.subject_id',$request->subject)
                                    ->where('grades.year_id',$request->year)
                                    ->where('grades.section_id',$request->section)
                                    ->where('grades._semester',$request->semester)
                                    ->where('grades.term_id',4)
                                    ->where('sfg.student_id',$s->student_id)->first();

                                $computeFinalSemGrade = ((1/3)*$findMidGrade->subject_equivalent) + ((2/3)*$findMidGrade->subject_equivalent);
                                $semGrade = round($computeFinalSemGrade,0);
                                $semGradeEquivalent = 0;
                                if ($semGrade >=95) {
                                    $semGradeEquivalent = 1.0;
                                }else if($semGrade <= 50){
                                    $semGradeEquivalent = 5.0;
                                }else{

                                    $finalEquivalent = GradeEquivalent::where('grade',$semGrade)->first();
                                    $semGradeEquivalent = $finalEquivalent->equivalent;
                                }
                                $findFinalSemesterDetails = SemesterGradeDetails::where('sem_grade_id',$findSemGrade->id)
                                    ->where('student_id',$s->student_id)
                                    ->first();
                                if (!is_null($findFinalSemesterDetails)) {
                                    $findFinalSemesterDetails->semester_grade = $semGradeEquivalent;
                                    $findFinalSemesterDetails->save();
                                }else{
                                    $insertSemGrade = new SemesterGradeDetails;
                                    $insertSemGrade->sem_grade_id = $findSemGrade->id;
                                    $insertSemGrade->student_id = $s->student_id;
                                    $insertSemGrade->semester_grade = $semGradeEquivalent;
                                    $insertSemGrade->save();
                                }

                            }
                        }else{
                            $storeSem = new SemesterGrade;
                            $storeSem->course_id = $request->course;
                            $storeSem->subject_id = $request->subject;
                            $storeSem->year_id = $request->year;
                            $storeSem->section_id = $request->section;
                            $storeSem->school_year_id = $request->sc_year;
                            $storeSem->_semester = $request->semester;
                            $storeSem->save();

                            foreach ($getStudents as $s) {
                                 $findMidGrade = Grades::select('sfg.subject_equivalent')
                                    ->join('subject_final_grade as sfg','sfg.grade_id','=','grades.id')
                                    ->where('grades.course_id',$request->course)
                                    ->where('grades.school_year_id',$request->sc_year)
                                    ->where('grades.subject_id',$request->subject)
                                    ->where('grades.year_id',$request->year)
                                    ->where('grades.section_id',$request->section)
                                    ->where('grades._semester',$request->semester)
                                    ->where('grades.term_id',2)
                                    ->where('sfg.student_id',$s->student_id)->first();
                                $findMidGrade = Grades::select('sfg.subject_equivalent')
                                    ->join('subject_final_grade as sfg','sfg.grade_id','=','grades.id')
                                    ->where('grades.course_id',$request->course)
                                    ->where('grades.school_year_id',$request->sc_year)
                                    ->where('grades.subject_id',$request->subject)
                                    ->where('grades.year_id',$request->year)
                                    ->where('grades.section_id',$request->section)
                                    ->where('grades._semester',$request->semester)
                                    ->where('grades.term_id',4)
                                    ->where('sfg.student_id',$s->student_id)->first();

                                $computeFinalSemGrade = ((1/3)*$findMidGrade->subject_equivalent) + ((2/3)*$findMidGrade->subject_equivalent);
                                $semGrade = round($computeFinalSemGrade,0);
                                $semGradeEquivalent = 0;
                                if ($semGrade >=95) {
                                    $semGradeEquivalent = 1.0;
                                }else if($semGrade <= 50){
                                    $semGradeEquivalent = 5.0;
                                }else{

                                    $finalEquivalent = GradeEquivalent::where('grade',$semGrade)->first();
                                    $semGradeEquivalent = $finalEquivalent->equivalent;
                                }

                                $insertSemGrade = new SemesterGradeDetails;
                                $insertSemGrade->sem_grade_id = $storeSem->id;
                                $insertSemGrade->student_id = $s->student_id;
                                $insertSemGrade->semester_grade = $semGradeEquivalent;
                                $insertSemGrade->save();

                            }
                        }
                    }else{
                        abort(415, "You havent created Final Term grade yet.");
                    }
                }else{
                    abort(415, "You havent created Semi-final grade yet.");
                }
                
            }else{
                abort(415, "You havent created Mid-term grade yet.");
            }
        }else{
            abort(415, "You havent created Prelim grade yet.");
        }
    }
    
    
    
}
