<?php

namespace App\Http\Controllers;

use App\CourseDetails;
use App\Quiz;
use App\QuizDetails;
use App\SchoolYear;
use App\Sections;
use App\Subjects;
use App\Year;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\Datatables;

class QuizesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usertype = Auth::user()->user_type_id;
        $user = Auth::user();
        $sc = SchoolYear::all();
        if ($usertype == 2) {
            $course = CourseDetails::select('course_id')
                ->with('course')
                ->where('teacher_id',$user->teacher_id)
                ->where(function($q){
                    $q->where('quiz','!=', null)
                      ->orwhere('quiz', '!=', 0);
                })
                ->groupby('course_id')
                ->get();
            return view('quizes.index',compact('course','sc'));
        }else{
            abort(411,'Forbbiden Access!');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usertype = Auth::user()->user_type_id;
        $user = Auth::user();
        $sc = SchoolYear::all();
        if ($usertype == 2) {
            $course = CourseDetails::select('course_id')
                ->with('course')
                ->where('teacher_id',$user->teacher_id)
                ->where(function($q){
                    $q->where('quiz','!=', null)
                      ->orwhere('quiz', '!=', 0);
                })
                ->groupby('course_id')
                ->get();
            return view('quiz_details.create',compact('course','sc'));
        }else{
            abort(411,'Forbbiden Access!');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $teacherid = Auth::user()->teacher_id;
        $this->validate($request,[
            'title' => 'required',
            'totalscore' => 'required',
            'course_id' => 'required',
            'subject_id' => 'required',
            'year_id' => 'required',
            'section_id' => 'required',
            '_semester' => 'required',
            'term_id' => 'required',
            
        ]);
        $find = Quiz::where('course_id',$request->course_id)
            ->where('title',$request->title)
            ->where('subject_id',$request->subject_id)
            ->where('year_id',$request->year_id)
            ->where('section_id',$request->section_id)
            ->where('term_id',$request->term_id)
            ->where('teacher_id',$teacherid)
            ->where('_semester',$request->_semester)
            ->where('school_year_id',$request->sc_year)
            ->first();
        // dd($find);
        if (!is_null($find)) {
            return redirect('quizes/create')->with('error_message','Quiz Title already exist.');
        }else{ 
            $add = new Quiz;
            $add->title = $request->title;
            $add->course_id = $request->course_id;
            $add->subject_id = $request->subject_id;
            $add->year_id = $request->year_id;
            $add->section_id = $request->section_id;
            $add->term_id = $request->term_id;
            $add->teacher_id = $teacherid;
            $add->_semester = $request->_semester;
            $add->school_year_id = $request->sc_year;
            $add->totalscore = $request->totalscore;
            $add->date = Carbon::today();
            $add->save();
            return redirect('quizes/'.$add->id.'/edit')->with('message','New Quiz has been addes successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quiz = Quiz::find($id);
        // dd($quiz);
        return view('quiz_details.edit',compact('quiz','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $find = Quiz::find($id);
        $find->delete();

        $deleteDetails = QuizDetails::where('quiz_id',$id)
            ->delete();
    }
    public function getSubjects($courseid){
        $subjects = [];
        $count = 0;
        $teacherid = Auth::user()->teacher_id;
        $courseDetails = CourseDetails::select('subject_id')
            ->where('course_id',$courseid)
            ->where('teacher_id',$teacherid)
            ->groupby('subject_id')
            ->get();

        foreach ($courseDetails as $cd) {

            $subjectDetails = Subjects::find($cd->subject_id);
            $subjects[$count] = ["id"=>$cd->subject_id, "name"=>$subjectDetails->name, "courseid"=>$courseid];
            $count++;

        }
        return $subjects;
    }
    public function getYear($courseid,$subjectid){
        $year = [];
        $count = 0;
        $teacherid = Auth::user()->teacher_id;
        $courseDetails = CourseDetails::select('year_id')
            ->where('course_id',$courseid)
            ->where('subject_id',$subjectid)
            ->where('teacher_id',$teacherid)
            ->groupby('year_id')
            ->get();
        foreach ($courseDetails as $cd) {

            $yearDetails = Year::find($cd->year_id);
            $year[$count] = ["id"=>$cd->year_id, "name"=>$yearDetails->code." Year"," courseid"=>$courseid, "subjectid"=>$subjectid];
            $count++;

        }
        return $year;
    }
    public function getSection($courseid,$subjectid,$yearid){
        $section = [];
        $count = 0;
        $teacherid = Auth::user()->teacher_id;
        $courseDetails = CourseDetails::select('courses_details.section_id')
            ->join('student_details as sd','courses_details.section_id','=','sd.section_id')
            ->where('courses_details.course_id',$courseid)
            ->where('courses_details.subject_id',$subjectid)
            ->where('courses_details.teacher_id',$teacherid)
            ->where('courses_details.year_id',$yearid)
            ->groupby('courses_details.section_id')
            ->get();
        // dd($courseDetails);
        foreach ($courseDetails as $cd) {

            $sectionDetails = Sections::find($cd->section_id);
            $section[$count] = ["id"=>$cd->section_id, "name"=>$sectionDetails->code," courseid"=>$courseid, "subjectid"=>$subjectid, "yearid"=>$yearid];
            $count++;

        }
        return $section;
    }
    public function getQuizes ($courseid,$subjectid,$yearid,$sectionid,$semesterid,$scyear){
        $teacherid = Auth::user()->teacher_id;
        
        $class = Quiz::select('id','title','totalscore','date','term_id')
            ->where('course_id',$courseid)
            ->where('subject_id',$subjectid)
            ->where('year_id',$yearid)
            ->where('section_id',$sectionid)
            ->where('_semester',$semesterid)
            ->where('teacher_id',$teacherid)
            ->where('school_year_id',$scyear)
            ->groupby('id','title','totalscore','date','term_id');
            
        return Datatables::of($class)
            ->addColumn('action',function($class)use($semesterid,$subjectid){
                $date = Carbon::today();
                return '<a href="quizes/'.$class->id.'/edit" class="btn btn-success btn-round btn-sm">Edit</a> <a href=#" class="btn btn-danger btn-round btn-sm" id="delete" data-id="'.$class->id.'">Delete</a>';
            })
            ->editColumn('term_id',function($class){
                // $term = ($class->term_id == 1) ? 'Mid Term' : 'Final Term';
                $term = '';
                if ($class->term_id == 1) {
                    $term = 'Prelim';
                }else if($class->term_id == 2){
                    $term = 'Mid Term';
                }else if ($class->term_id == 3) {
                    $term = 'Semi-final Term';
                }else{
                    $term = 'Final Term';
                }
                return $term;
            })
            ->make(true);
    }
    public function scoreUpload(Request $request,$id){
       $find = QuizDetails::where('quiz_id',$id)
        ->where('student_id',$request->studentid)->first();
        if(!is_null($find)){
            $find->score = $request->score;
            $find->update();
        }else{
            $new = new QuizDetails;
            $new->quiz_id = $id;
            $new->student_id = $request->studentid;
            $new->score = $request->score;
            $new->save();
        }

    }
    public function getStudentQuiz($id){
        $quiz = Quiz::find($id);
       
        $student = DB::table('quizes as q')
            ->select('s.first_name','s.last_name','s.id as studid','q.id as quizid')
            // ->leftjoin('quiz_details as qd','q.id','=','qd.quiz_id')
            ->leftjoin('courses_details as cd','q.course_id','=','cd.course_id')
            ->leftjoin('student_details as sd','cd.course_id','=','sd.course_id')
            ->leftjoin('students as s','sd.student_id','=','s.id')
            ->where('q.id',$id)
            ->where('cd.subject_id',$quiz->subject_id)
            ->where('sd.year_id',$quiz->year_id)
            ->where('sd.section_id',$quiz->section_id)
            ->where('sd.school_year_id',$quiz->school_year_id)
            ->groupby('s.first_name','s.last_name','s.id','q.id');
            // ->where('q.section_id',$id)
            // dd($student);
            // return Datatables::of($class)
            // ->addColumn('action',function($class)use($semesterid,$subjectid){
            //     $date = Carbon::today();
            //     return '<a href="quizes/'.$class->id.'/edit" class="btn btn-success btn-round btn-sm">Edit</a>';
            // })
            // ->make(true);

            
          

            return Datatables::of($student)
                ->addColumn('action',function($student){

                    $score = QuizDetails::where('quiz_id',$student->quizid)
                        ->where('student_id',$student->studid)->first();
                    if(!is_null($score)){
                        // return $score->score;
                        return '<input type="number" id="'.$student->studid.'" name="score" class="form-control" value="'.$score->score.'">';
                    }else{
                        // return 0;
                        return '<input type="number" id="'.$student->studid.'" name="score" class="form-control" >';
                    }
                   
                })->make(true);
    }
}
