<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizDetails extends Model
{
    protected $table = 'quiz_details';
    protected $fillable = [
    				'quiz_id',
		            'student_id',
		            'score',
    		];
}
