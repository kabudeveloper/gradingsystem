<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grades extends Model
{
    protected $table = 'grades';
    protected $fillable = [
    	'course_id',
        'subject_id',
        'year_id',
        'section_id',
        '_semester',
        'school_year_id',
        'term_id',
    ];

    public function course(){
        return $this->belongsTo(Courses::class,'course_id');
    }
    public function subject(){
    	return $this->belongsTo(Subjects::class,'subject_id');
    }
    public function year(){
        return $this->belongsTo(Year::class,'year_id');
    }
    public function section(){
        return $this->belongsTo(Year::class,'section_id');
    }
    
}
