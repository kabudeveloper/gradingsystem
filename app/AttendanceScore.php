<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceScore extends Model
{
     protected $table = 'attendance_score';
    protected $fillable = [
        'title',
        'course_id',
        'subject_id',
        'year_id',
        'section_id',
        'teacher_id',
        '_semester',
        'term_id',
        'school_year_id',
        'totalscore',
        'date',
    ];
    public function course(){
        return $this->belongsTo(Courses::class,'course_id');
    }
    public function subject(){
        return $this->belongsTo(Subjects::class,'subject_id');
    }
    public function year(){
        return $this->belongsTo(Year::class,'year_id');
    }
     public function section(){
        return $this->belongsTo(Sections::class,'section_id');
    }
    public function teacher(){
        return $this->belongsTo(Teachers::class,'teacher_id');
    }
}
