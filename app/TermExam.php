<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TermExam extends Model
{
    protected $table = 'term_exam';
    protected $fillable = [
        'title',
        'course_id',
        'subject_id',
        'year_id',
        'section_id',
        'teacher_id',
        '_semester',
        'school_year_id',
        'term_id',
        'totalscore',
        'date',
    ];
    public function course(){
        return $this->belongsTo(Courses::class,'course_id');
    }
    public function subject(){
        return $this->belongsTo(Subjects::class,'subject_id');
    }
    public function year(){
        return $this->belongsTo(Year::class,'year_id');
    }
     public function section(){
        return $this->belongsTo(Sections::class,'section_id');
    }
    public function teacher(){
        return $this->belongsTo(Teachers::class,'teacher_id');
    }
}
