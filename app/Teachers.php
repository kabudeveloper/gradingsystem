<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teachers extends Model
{
   protected $table = 'teachers'; 
    protected $fillable = [
        'first_name',
        'last_name',
        'middle_name',
       
    ];
}
