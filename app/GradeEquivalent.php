<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GradeEquivalent extends Model
{
    //grade_equivalent
    protected $table = 'grade_equivalent'; 
    protected $fillable = [
    	'grade',
        'equivalent',
        
    ];
}
