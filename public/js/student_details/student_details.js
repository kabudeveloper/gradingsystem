$(document).ready(function(){
	var studid = $('#studentId').val();
	var studentDetailsId = $('#detailsId').val();

	var studenDetails = $('#student-details-table').DataTable({
		processing: true,
		serverSide: true,
		ajax: 'details/'+studentDetailsId+'/get',
		columns: [
			{data:'course.code', name:'course.code'},
			{data:'course.major', name:'course.major'},
			{data:'year.code', name:'year.code'},
			{data:'section.code', name:'section.code'},
			{data:'address', name:'address'},
			{data:'number', name:'number'},
			{data:'contact_person', name:'contact_person'},
			{data:'contact_number', name:'contact_number'},
			{data:'action', name:'action'},
		],

	});
	var del = 0;
	$('#student-details-table').on('click','#delete',function(){
		$('#confirm-modal').modal('show');

	});
	$('#confirm-button').click(function(){
		$.ajax({
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			method:'DELETE',
			url:'details/'+studentDetailsId+'/delete',
			beforeSend:function(){
				$('#confirm-button').text('Deleting...');
			},
			success: function(data){
				location.reload();
			}
		});
	});

	var accountid = 0;
	var studentacc = $('#student-account-table').DataTable({
		processing: true,
		serverSide: true,
		ajax: 'account/get',
		columns:[
			{data:'username', name:'username' },
			{data:'position.name', name:'position.name' },
			{data:'user_type.name', name:'user_type.name' },
			{data:'action', name:'action' },
		],

	});

	$('#student-account-table').on('click','#delete',function(){
		accountid = $(this).attr('data-id');
		$('#account-modal').modal('show');

	});
	$('#aconfirm-button').click(function(){
		$.ajax({
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			method:'DELETE',
			url:'account/'+accountid+'/destroy',
			beforeSend:function(){
				$('#confirm-button').text('Deleting...');
			},
			success: function(data){
				location.reload();
			}
		});
	});
	// alert(1);
	$("select[name=position_id]").change(function(){
		var pid = $(this).val();
		if (pid == 0) {
			$('#p_fname').html('<label class="col-sm-4 control-label">First Name</label><input type="text" name="first_name" class="form-control" required>');
			$('#p_mname').html('<label class="col-sm-4 control-label">Middle Name</label><input type="text" name="middle_name" class="form-control" required>');
			$('#p_lname').html('<label class="col-sm-4 control-label">Last Name</label><input type="text" name="last_name" class="form-control" required>');
		}else{
			$('#p_fname').html('');
			$('#p_mname').html('');
			$('#p_lname').html('');
		}
	});	

});

