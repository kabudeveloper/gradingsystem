$(document).ready(function(){
	var accountid = 0;
	var account = $('#teacher-account-table').DataTable({
		processing: true,
		serverSide: true,
		// "order":[[0,'dec']],
		ajax: 'account/getteacheraccount',
		columns: [
			{data:'username',name:'username'},
			{data: 'position.name', name:'position.name'},
			{data: 'user_type.name', name:'user_type.name'},
	        {data: 'action', name: 'action', orderable: false, searchable: false},

		],

	});
	$('#teacher-account-table').on('click','#delete',function(){
		accountid = $(this).attr('data-id');
		$('#confirm-modal').modal('show');
	});
	$('#confirm-button').click(function(){
		$.ajax({
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			method:'DELETE',
			url:'account/'+accountid+'/destroy',
			beforeSend:function(){
				$('#confirm-button').text('Deleting...');
			},
			success: function(data){
				location.reload();
			}
		});
	})

	var assigned = $('#assigned-details-table').DataTable({
		processing: true,
		serverSide: true,
		// "order":[[0,'dec']],
		ajax: 'details/getassigned',
		columns: [
			{data:'code',name:'code'},
			{data: 'major', name:'major'},
			{data: 'subject', name:'subject'},
			{data: 'year', name:'year'},
			{data: 'section', name:'section'},
			{data: '_semester', name:'_semester'},
	        {data: 'action', name: 'action', orderable: false, searchable: false},

		],

	});

	var notassigned = $('#assign-to-details-table').DataTable({
		processing: true,
		serverSide: true,
		// "order":[[0,'dec']],
		ajax: 'details/getnotassigned',
		columns: [
			{data:'code',name:'code'},
			{data: 'major', code:'major'},
			{data: 'subject', name:'subject'},
			{data: 'year', name:'year'},
			{data: 'section', name:'section'},
			{data: '_semester', name:'_semester'},
	        {data: 'action', name: 'action', orderable: false, searchable: false},

		],

	});

	$('#assign-to-details-table').on('click','#assign',function(){
		var id = $(this).attr('data-id');
		$.ajax({
			headers:{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			method:'PUT',
			url:'details/'+id+'/assigned',
			success: function(data){
				
				$('#assign-to-details-table').DataTable().ajax.reload();
				$('#assigned-details-table').DataTable().ajax.reload();
			}
		});
	});

	$('#assigned-details-table').on('click','#cancel',function(){
		var id = $(this).attr('data-id');
		$.ajax({
			headers:{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			method:'PUT',
			url:'details/'+id+'/cancel',
			success: function(data){
				
				$('#assign-to-details-table').DataTable().ajax.reload();
				$('#assigned-details-table').DataTable().ajax.reload();
			}
		});
	});
	
});