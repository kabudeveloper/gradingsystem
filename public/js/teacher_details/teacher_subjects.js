$(document).ready(function(){
	var subjects = $('#my-subjects').DataTable({
		processing: true,
		serverSide: true,
		"order":[[0,'dec']],
		ajax: 'teachersubjects/get',
		columns: [
			{data:'course.code',name:'course.code'},
			{data: 'subject.name', name:'subject.name'},
			{data: 'year.code', name:'year.code'},
			{data: 'section.code', name:'section.code'},
	        {data: '_semester', name: '_semester'},

		],
		// "columnDefs":[
			
		// 	{
		// 		"targets":2,
		// 		"width":"45%"
		// 	}
		// ]

	});
});