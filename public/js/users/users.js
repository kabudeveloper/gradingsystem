$(document).ready(function(){
	var userId = 0;
	var users = $('#users-table').DataTable({
		processing: true,
		serverSide: true,
		ajax: 'users/get',
		columns:[
			{data:'last_name',name:'last_name'},
			{data:'first_name',name:'first_name'},
			{data:'middle_name',name:'middle_name'},
			{data:'username',name:'username'},
			{data:'position.name',name:'position.name'},
			{data:'user_type.name',name:'user_type.name'},
			{data:'action',name:'action'}
		]
		
	});

	$('#users-table').on('click','#delete',function(){
		userId = $(this).attr('data-id');
		$('#confirm-modal').modal('show');
	});

	$('#confirm-button').click(function(){
		$.ajax({
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            method:'DELETE',
			url:'users/'+userId,
			beforeSend:function(){
				$('#confirm-button').text('Deleting...');
			},
			success:function(data){
				setTimeout(function(){
					$('#confirm-button').text('Yes');
					$('#confirm-modal').modal('hide');
					$('#users-table').DataTable().ajax.reload();
				},500);

			}

		})

	});
	// $('#user_type').change(function(){
	// 	var usertypeid = $(this).val();
	// 	if (usertypeid == 4) {
			
	// 	}
	// });

});