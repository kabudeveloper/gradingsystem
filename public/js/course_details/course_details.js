$(document).ready(function(){
	var courseDetailsId = 0;
	var courseDetails = $('#course-details-table').DataTable({
		processing:true,
		serverSide:true,
		ajax:'details/get',
		"order":[[1,'desc']],
		columns:[
			{data:'subject.code',name:'subject.code'},
			{data:'subject.name',name:'subject.name'},
			{data:'year.code',name:'year.code'},
			{data:'section.code',name:'section.code'},
			{data:'_semester',name:'_semester'},
			{data:'action',name:'action'},
		],

	});
	$('#course-details-table').on('click','#delete',function(){
		courseDetailsId = $(this).attr('data-id');
		$('#confirm-modal').modal('show');

	});
	$('#confirm-button').click(function(){
		
		$.ajax({
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			method:'DELETE',
			url:'details/'+courseDetailsId+'/delete',
			beforeSend:function(){
				$('#confirm-button').text('Deleting...');
			},
			success: function(data){
				location.reload();
			}

		});
		
	});

});
