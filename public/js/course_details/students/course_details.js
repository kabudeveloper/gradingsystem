$(document).ready(function(){
	var courseDetailsId = 0;
	var courseDetails = $('#course-details-table').DataTable({
		processing:true,
		serverSide:true,
		ajax:'coursedetails/getcoursesubjects',
		"order":[[1,'desc']],
		columns:[

			{data:'subject.code',name:'subject.code'},
			{data:'subject.name',name:'subject.name'},
			{data:'term_exam',name:'term_exam'},
			{data:'quiz',name:'quiz'},
			{data:'assignment',name:'assignment'},
			{data:'attendance',name:'attendance'},
			{data:'participation',name:'participation'},
		],

	});
	

});
