$(document).ready(function(){
	var studentid = $("input[name=studentid]").val();
	var subjectgradeid = $("input[name=subjectgradeid]").val();
	var term = $('#term-details-table').DataTable({
		processing: true,
		serverSide: true,
		// "order":[[0,'dec']],
		ajax: 'getTerm',
		columns: [
			{data:'title',name:'title'},
			{data: 'totalscore', name:'totalscore'},
			{data: 'score', name:'score'},
		],
	});

	var term = $('#quiz-details-table').DataTable({
		processing: true,
		serverSide: true,
		// "order":[[0,'dec']],
		ajax: 'getQuiz',
		columns: [
			{data:'title',name:'title'},
			{data: 'totalscore', name:'totalscore'},
			{data: 'score', name:'score'},
		],
	});

	var term = $('#ass-details-table').DataTable({
		processing: true,
		serverSide: true,
		// "order":[[0,'dec']],
		ajax: 'getAss',
		columns: [
			{data:'title',name:'title'},
			{data: 'totalscore', name:'totalscore'},
			{data: 'score', name:'score'},
		],
	});

	var term = $('#attendance-details-table').DataTable({
		processing: true,
		serverSide: true,
		// "order":[[0,'dec']],
		ajax: 'getAttendance',
		columns: [
			{data:'title',name:'title'},
			{data: 'totalscore', name:'totalscore'},
			{data: 'score', name:'score'},
		],
	});
	
});