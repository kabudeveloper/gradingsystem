$(document).ready(function(){
	var deleteId = 0;
	var s = $('#school-year-table').DataTable({
		processing: true,
		serverSide: true,
		"order":[[0,'dec']],
		ajax: 'schoolyear/get',
		columns: [
			{data: 'school_year', name:'school_year'},
			{data: 'action', name: 'action', orderable: false, searchable: false},

		],
		"columnDefs":[
			// {
			// 	"targets":[0],
			// 	"visible": false,
			// 	"searchable": false
			// },
			{
				"targets":1,
				"width":"45%"
			}
		]

	});

	$('#school-year-table').on('click','#delete',function(){
		deleteId = $(this).attr('data-id');
		$('#confirm-modal').modal('show');
		
	});
	$('#confirm-button').click(function(){
		$.ajax({
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            method:'DELETE',
			url:'schoolyear/'+deleteId,
			beforeSend:function(){
				$('#confirm-button').text('Deleting...');
			},
			success:function(data){
				setTimeout(function(){
					$('#confirm-button').text('Yes');
					$('#confirm-modal').modal('hide');
					$('#courses-table').DataTable().ajax.reload();
				},500);

			}
		})
	});

	

});