$(document).ready(function(){
	var deleteId = 0;
	var courses = $('#courses-table').DataTable({
		processing: true,
		serverSide: true,
		"order":[[0,'dec']],
		ajax: 'courses/get',
		columns: [
			{data:'id',name:'id'},
			{data: 'code', name:'code'},
			{data: 'name', name:'name'},
			{data: 'major', name:'major'},
	        {data: 'action', name: 'action', orderable: false, searchable: false},

		],
		"columnDefs":[
			{
				"targets":[0],
				"visible": false,
				"searchable": false
			},
			{
				"targets":2,
				"width":"45%"
			}
		]

	});

	$('#courses-table').on('click','#delete',function(){
		deleteId = $(this).attr('data-id');
		$('#confirm-modal').modal('show');
		
	});
	$('#confirm-button').click(function(){
		$.ajax({
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            method:'DELETE',
			url:'courses/'+deleteId,
			beforeSend:function(){
				$('#confirm-button').text('Deleting...');
			},
			success:function(data){
				setTimeout(function(){
					$('#confirm-button').text('Yes');
					$('#confirm-modal').modal('hide');
					$('#courses-table').DataTable().ajax.reload();
				},500);

			}
		})
	});

	

});