$(document).ready(function(){
	var teacherId = 0;
	var teachers = $('#teachers-table').DataTable({
		processing: true,
		serverSide: true,
		ajax: 'teachers/get',
		columns:[
			{ data:'last_name', name:'last_name' },
			{ data:'first_name', name:'first_name' },
			{ data:'middle_name', name:'middle_name' },
			{ data:'action', name:'action' },
		],
		// "columnDefs":[
		// 	{"width":"35%", "targets":3}
		// ],
	});
});