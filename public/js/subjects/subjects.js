$(document).ready(function(){
	var subjectId = 0;
	var subjects = $('#subjects-table').DataTable({
		processing: true,
		serverSide: true,
		"order":[[0,'asc']],
		ajax:'subjects/get',
		columns:[
			{ data:'code', name:'code' },
			{ data:'name', name:'name' },
			{ data:'units', name:'units' },
			{ data:'action', name:'action' },
		],
		"columnDefs":[
			{"width":"35%", "targets":3}
		],

	});

	$('#subjects-table').on('click','#delete',function(){

		subjectId = $(this).attr('data-id');
		$('#confirm-modal').modal('show');
	});
	$('#confirm-button').click(function(){
		$.ajax({
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            method:'DELETE',
			url:'subjects/'+subjectId,
			beforeSend:function(){
				$('#confirm-button').text('Deleting...');
			},
			success:function(data){
				setTimeout(function(){
					$('#confirm-button').text('Yes');
					$('#confirm-modal').modal('hide');
					$('#subjects-table').DataTable().ajax.reload();
				},500);

			}
		});
		
	});
});