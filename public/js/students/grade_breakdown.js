$(document).ready(function(){
	// var semgradeid = $('#semester_grade').va();
	var termexam = $('#attendance-details-table').DataTable({
		processing: true,
		serverSide: true,
		"order":[[0,'asc']],
		ajax: 'getattendance',
		columns: [
			{data:'term', name:'term'},
			{data:'title', name:'title'},
			{data:'totalscore', name:'totalscore'},
			{data:'score', name:'score'},
		],

	});
	var termexam = $('#term-exam-details-table').DataTable({
		processing: true,
		serverSide: true,
		"order":[[0,'asc']],
		ajax: 'getterm',
		columns: [
			{data:'term', name:'term'},
			{data:'title', name:'title'},
			{data:'totalscore', name:'totalscore'},
			{data:'score', name:'score'},
		],

	});
	var quiz = $('#quiz-details-table').DataTable({
		processing: true,
		serverSide: true,
		"order":[[0,'asc']],
		ajax: 'getquiz',
		columns: [
			{data:'term', name:'term'},
			{data:'title', name:'title'},
			{data:'totalscore', name:'totalscore'},
			{data:'score', name:'score'},
		],

	});
	var assignment = $('#assignment-details-table').DataTable({
		processing: true,
		serverSide: true,
		"order":[[0,'asc']],
		ajax: 'getass',
		columns: [
			{data:'term', name:'term'},
			{data:'title', name:'title'},
			{data:'totalscore', name:'totalscore'},
			{data:'score', name:'score'},
		],

	});
	// $('#confirm-button').click(function(){
	// 	$.ajax({
	// 		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
 //            method:'DELETE',
	// 		url:'students/'+deleteId,
	// 		beforeSend:function(){
	// 			$('#confirm-button').text('Deleting...');
	// 		},
	// 		success:function(data){
	// 			setTimeout(function(){
	// 				$('#confirm-button').text('Yes');
	// 				$('#confirm-modal').modal('hide');
	// 				$('#students-table').DataTable().ajax.reload();
	// 			},500);

	// 		}
	// 	});
	// });
	
	
});	