$(document).ready(function(){
	var deleteId = 0;
	var students = $('#students-table').DataTable({
		processing: true,
		serverSide: true,
		"order":[[0,'asc']],
		ajax: 'students/get',
		columns: [
			{data:'last_name', name:'last_name'},
			{data:'first_name', name:'first_name'},
			{data:'middle_name', name:'middle_name'},
			{data:'suffix', name:'suffix'},
			{data:'action', name:'action'},
		],

	});
	$('#students-table').on('click','#delete',function(){
		deleteId = $(this).attr('data-id');
		$('#confirm-modal').modal('show');
	});
	$('#confirm-button').click(function(){
		$.ajax({
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            method:'DELETE',
			url:'students/'+deleteId,
			beforeSend:function(){
				$('#confirm-button').text('Deleting...');
			},
			success:function(data){
				setTimeout(function(){
					$('#confirm-button').text('Yes');
					$('#confirm-modal').modal('hide');
					$('#students-table').DataTable().ajax.reload();
				},500);

			}
		});
	});
	
	$("#uploadBtn").click(function(){
		$('#uploadModal').toggle();
	    $(".loader").css('display','block');
		setTimeout(function(){ 
			location.reload(); 
		}, 30000);
	    
	});
});	