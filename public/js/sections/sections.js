$(document).ready(function(){

	var deleteId = 0;
	var section = $('#section-table').DataTable({
		processing: true,
		serverSide: true,
		ajax: 'sections/get',
		"order":[[0,'desc']],
		columns: [
			{data: 'id', name:'id'},
			{data: 'code', name:'code'},
			{data: 'name', name:'name'},
	        {data: 'action', name: 'action', orderable: false, searchable: false},

		],
		"columnDefs":[
			{
				"width":"35%",
				"targets":3
			},
			{
				"targets":[0],
				"visible": false,
				"searchable": false
			}
		],

	});
	$('#section-table').on('click','#delete',function(){
		deleteId = $(this).attr('data-id');
		$('#confirm-modal').modal('show');
	});
	$('#confirm-button').click(function(){
		$.ajax({
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            method:'DELETE',
			url:'sections/'+deleteId,
			beforeSend:function(){
				$('#confirm-button').text('Deleting...');
			},
			success:function(data){
				setTimeout(function(){
					$('#confirm-button').text('Yes');
					$('#confirm-modal').modal('hide');
					$('#section-table').DataTable().ajax.reload();
				},500);
			}
		});
	});
});