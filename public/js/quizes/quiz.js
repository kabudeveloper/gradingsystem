$(document).ready(function(){

	$('#course').change(function(){
		var courseid = $(this).val();
		$.ajax({
			method: 'GET',
			url:'subject/'+courseid+'/getSubjects',
			success:function(data){
				var html = [];	
				var he = '';		
				for (var i = 0; i < data.length; i++) {
					
					html[i] ='<option data-id="'+data[i].courseid+'" value="'+data[i].id+'" id="subjectData">'+data[i].name+'</option>';
				}
				
				he ='<option value="0"></option>'+html;
				$('#subject').html(he);
				
			}
		});
	});

	$('#subject').change(function(){
		// var courseid = 0;
		var subject = $(this).val();
		var courseid = $('#subjectData').attr('data-id');
		$.ajax({
			method: 'GET',
			url:'year/'+courseid+'/'+subject+'/getYear',
			success:function(data){
				var html = [];	
				var he = '';			
				for (var i = 0; i < data.length; i++) {
					html[i] ='<option value="'+data[i].id+'" id="yearData" data-id="'+data[i].subjectid+'">'+data[i].name+'</option>';
				}
				he ='<option value="0"></option>'+html;
				$('#year').html(he);
				
			}
		});
	});

	$('#year').change(function(){
		// var courseid = 0;
		var yearid = $(this).val();
		var subject = $('#yearData').attr('data-id');
		var courseid = $('#subjectData').attr('data-id');
		$.ajax({
			method: 'GET',
			url:'section/'+courseid+'/'+subject+'/'+yearid+'/getSection',
			success:function(data){
				var html = [];	
				var he = '';
				var html1 = [];	
				var he1 = '';			
				for (var i = 0; i < data.length; i++) {
					html[i] ='<option value="'+data[i].id+'" id="sectionData" data-id="'+data[i].yearid+'">'+data[i].name+'</option>';
				}

				he ='<option value="0"></option>'+html;
				$('#section').html(he);

				// he ='<option value="1">1st Sem</option>';
				for(var i= 0; i<2; i++){

					if (i == 0) {
						html1[i] ='<option value="1">1st Sem</option>';
					}else{
						html1[i] ='<option value="2">2nd Sem</option>';	
					}
					
				}
				$('#semester').html(html1);
				
			}
		});
	});

	$('#quiz-form').submit(function(e){
		e.preventDefault();
		// alert(1);
			var courseid = $('#course').val();
			var subjectid = $('#subject').val();
			var yearid = $('#year').val();
			var sectionid = $('#section').val();
			var semesterid = $('#semester').val();
		
		var classes = $('#quizes-class-table').DataTable({
			destroy: true,
			processing: true,
			serverSide: true,
			searching: false,
			"paging": false,
			ajax: 'studentquiz/'+courseid+'/'+subjectid+'/'+yearid+'/'+sectionid+'/'+semesterid+'/get',
			columns:[
				{data:'title',name:'title'},
				{data:'date',name:'date'},
				{data:'totalscore',name:'totalscore'},
				{data:'action',name:'action'},
			],

		});
			

	});

	// $('#score-form').submit(function(e){
	// 	e.preventDefault();
	// 	// var data = [];
	// 	var  cnt = 0;
	// 	$("input[name=sc]").each(function(){
	// 		var total = $("input[name=totalscore]").val();
	// 		var stud_id = $(this).attr('id');
	// 		var sem_id = $(this).attr('data-sem');
	// 		var sub_id = $(this).attr('data-sub');
	// 		var score = $(this).val();
	// 		var data = {total:total, score:score, student:stud_id, semester:sem_id, subject:sub_id };
	// 		if (score > total) {
	// 			alert("You enter a value of "+score+" which is above perfect score, update your scores");
	// 		}else{
	// 			$.ajax({
	// 				headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
	// 				url: 'quizes/store',
	// 				data: data,
	// 				success:function(data){
	// 					$('#student-class-table').DataTable().ajax.reload();
	// 				}
	// 			});
	// 		}
			
			
	// 	});

		// var data = $(this).serialize();
		// console.log(data);
		


	// });

});