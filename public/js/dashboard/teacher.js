$(document).ready(function(){
	var term = 0; 

	$('#course').change(function(){
		var courseid = $(this).val();
		$.ajax({
			method: 'GET',
			url:'subject/'+courseid+'/getSubjects',
			success:function(data){
				var html = [];	
				var he = '';		
				for (var i = 0; i < data.length; i++) {
					
					html[i] ='<option data-id="'+data[i].courseid+'" value="'+data[i].id+'" id="subjectData">'+data[i].name+'</option>';
				}
				
				he ='<option value="0"></option>'+html;
				$('#subject').html(he);
				
			}
		});
	});

	$('#subject').change(function(){
		// var courseid = 0;
		var subject = $(this).val();
		var courseid = $('#subjectData').attr('data-id');
		$.ajax({
			method: 'GET',
			url:'year/'+courseid+'/'+subject+'/getYear',
			success:function(data){
				var html = [];	
				var he = '';			
				for (var i = 0; i < data.length; i++) {
					html[i] ='<option value="'+data[i].id+'" id="yearData" data-id="'+data[i].subjectid+'">'+data[i].name+'</option>';
				}
				he ='<option value="0"></option>'+html;
				$('#year').html(he);
				
			}
		});
	});

	$('#year').change(function(){
		// var courseid = 0;
		var yearid = $(this).val();
		var subject = $('#yearData').attr('data-id');
		var courseid = $('#subjectData').attr('data-id');
		$.ajax({
			method: 'GET',
			url:'section/'+courseid+'/'+subject+'/'+yearid+'/getSection',
			success:function(data){
				var html = [];	
				var he = '';
				var html1 = [];	
				var he1 = '';			
				for (var i = 0; i < data.length; i++) {
					html[i] ='<option value="'+data[i].id+'" id="sectionData" data-id="'+data[i].yearid+'">'+data[i].name+'</option>';
				}

				he ='<option value="0"></option>'+html;
				$('#section').html(he);

				// he ='<option value="1">1st Sem</option>';
				for(var i= 0; i<2; i++){

					if (i == 0) {
						html1[i] ='<option value="1">1st Sem</option>';
					}else{
						html1[i] ='<option value="2">2nd Sem</option>';	
					}
					
				}
				$('#semester').html(html1);
				
			}
		});
	});

	var sc_year = $('#sc_year').val();
	$('#class-form').submit(function(e){
		e.preventDefault();
			var courseid = $('#course').val();
			var subjectid = $('#subject').val();
			var yearid = $('#year').val();
			var sectionid = $('#section').val();
			var semesterid = $('#semester').val();
			
			term = $('#term').val();
		var classes = $('#student-class-table').DataTable({
			destroy: true,
			processing: true,
			serverSide: true,
			ajax: 'classes/'+courseid+'/'+subjectid+'/'+yearid+'/'+sectionid+'/'+semesterid+'/'+sc_year+'/get',
			columns:[
				{data:'last_name',name:'s.last_name'},
				{data:'first_name',name:'s.first_name'},
				{data:'middle_name',name:'s.middle_name'},
				{data:'action',name:'action'},
			],

		});
			

	});

	$('#student-class-table').on('click','#present',function(){
		var studentid = $(this).attr('data-id');
		var semid = $(this).attr('data-sec');
		$.ajax({
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			// method:'POST',
			url:'attendance/'+studentid+'/'+semid+'/'+term+'/'+sc_year+'/in',
			success:function(data){
				if (data != 0) {
					$('#present').addClass('disabled');
					$('#student-class-table').DataTable().ajax.reload();

				}else{
					
					$('#student-class-table').DataTable().ajax.reload();
				}
			}

		});
	});

	$('#student-class-table').on('click','#absent',function(){
		var studentid = $(this).attr('data-id');
		var semid = $(this).attr('data-sec');
		$.ajax({
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			url:'attendance/'+studentid+'/'+semid+'/'+term+'/'+sc_year+'/absent',
			success:function(data){
				if (data != 0) {
					$('#absent').addClass('disabled');
					$('#student-class-table').DataTable().ajax.reload();
					
				}else{
					
					$('#student-class-table').DataTable().ajax.reload();
				}
			}

		});
	});

	$('#student-class-table').on('click','#timeout',function(){
		var studentid = $(this).attr('data-id');
		var semid = $(this).attr('data-sec');
		$.ajax({
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			url:'attendance/'+studentid+'/'+semid+'/'+term+'/'+sc_year+'/out',
			success:function(data){
				if (data != 0) {
					// $('#absent').addClass('disabled');
					$('#student-class-table').DataTable().ajax.reload();
					
				}else{
					
					$('#student-class-table').DataTable().ajax.reload();
				}
			}

		});
	});
	

});