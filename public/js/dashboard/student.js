$(document).ready(function(){

	var students = $('#student-grade-details-table').DataTable({
		destroy: true,
		processing: true,
		serverSide: true,
		ajax: 'studentgrade/get',
		columns:[
			{data:'subject',name:'subject'},
			{data:'year',name:'year'},
			{data:'semester',name:'semester'},
			// {data:'term',name:'term'},
			{data:'final_equivalent',name:'final_equivalent'},
			{data:'action',name:'action', orderable: false, searchable: false },
		],

	});

	var students = $('#student-term-grade-details-table').DataTable({
		destroy: true,
		processing: true,
		serverSide: true,
		ajax: 'studentgrade/getterm',
		columns:[
			{data:'subject',name:'subject'},
			{data:'year',name:'year'},
			{data:'semester',name:'semester'},
			{data:'term',name:'term'},
			{data:'final_equivalent',name:'final_equivalent'},
		],

	});

	

});