$(document).ready(function(){

	$('#course').change(function(){
		var courseid = $(this).val();
		$.ajax({
			method: 'GET',
			url:'subject/'+courseid+'/getSubjects',
			success:function(data){
				var html = [];	
				var he = '';		
				for (var i = 0; i < data.length; i++) {
					
					html[i] ='<option data-id="'+data[i].courseid+'" value="'+data[i].id+'" id="subjectData">'+data[i].name+'</option>';
				}
				
				he ='<option value="0"></option>'+html;
				$('#subject').html(he);
				
			}
		});
	});

	$('#subject').change(function(){
		// var courseid = 0;
		var subject = $(this).val();
		var courseid = $('#subjectData').attr('data-id');
		$.ajax({
			method: 'GET',
			url:'/year/'+courseid+'/'+subject+'/getYear',
			success:function(data){
				var html = [];	
				var he = '';			
				for (var i = 0; i < data.length; i++) {
					html[i] ='<option value="'+data[i].id+'" id="yearData" data-id="'+data[i].subjectid+'">'+data[i].name+'</option>';
				}
				he ='<option value="0"></option>'+html;
				$('#year').html(he);
				
			}
		});
	});

	$('#year').change(function(){
		// var courseid = 0;
		var yearid = $(this).val();
		var subject = $('#yearData').attr('data-id');
		var courseid = $('#subjectData').attr('data-id');
		$.ajax({
			method: 'GET',
			url:'/section/'+courseid+'/'+subject+'/'+yearid+'/getSection',
			success:function(data){
				var html = [];	
				var he = '';
				var html1 = [];	
				var he1 = '';			
				for (var i = 0; i < data.length; i++) {
					html[i] ='<option value="'+data[i].id+'" id="sectionData" data-id="'+data[i].yearid+'">'+data[i].name+'</option>';
				}

				he ='<option value="0"></option>'+html;
				$('#section').html(he);

				// he ='<option value="1">1st Sem</option>';
				for(var i= 0; i<2; i++){

					if (i == 0) {
						html1[i] ='<option value="1">1st Sem</option>';
					}else{
						html1[i] ='<option value="2">2nd Sem</option>';	
					}
					
				}
				$('#semester').html(html1);
				
			}
		});
	});
	var sc_year = $('#sc_year').val();
	$('#quiz-form').submit(function(e){
		e.preventDefault();
		// alert(1);
			var courseid = $('#course').val();
			var subjectid = $('#subject').val();
			var yearid = $('#year').val();
			var sectionid = $('#section').val();
			var semesterid = $('#semester').val();
		
		var classes = $('#quizes-class-table').DataTable({
			destroy: true,
			processing: true,
			serverSide: true,
			searching: true,
			ajax: 'studentquiz/'+courseid+'/'+subjectid+'/'+yearid+'/'+sectionid+'/'+semesterid+'/'+sc_year+'/get',
			columns:[
				{data:'title',name:'title'},
				{data:'date',name:'date'},
				{data:'term_id',name:'term_id'},
				{data:'totalscore',name:'totalscore'},
				{data:'action',name:'action'},
			],

		});
			

	});

	var id = 0;
	$('#quizes-class-table').on('click','#delete',function(){
		id = $(this).attr('data-id');
		$('#confirm-modal').modal('show');

	});
	$('#confirm-button').click(function(){
		$.ajax({
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            method:'DELETE',
			url:'quizes/'+id,
			beforeSend:function(){
				$('#confirm-button').text('Deleting...');
			},
			success:function(data){
				$('#confirm-button').text('Yes');
				$('#confirm-modal').modal('hide');
				$('#quizes-class-table').DataTable().ajax.reload();

			}
		})
	});

	var students_quiz = $('#quiz-details-table').DataTable({
		processing: true,
		serverSide: true,
		searching: true,
		"paging": false,
		ajax: 'studentquiz/get',
		columns:[
			{data:'last_name',name:'last_name'},
			{data:'first_name',name:'first_name'},
			{data:'action',name:'action'},
		],

	});
	$('#student-score-form').submit(function(e){
		e.preventDefault();
		var t = $("input[name=totalscore]").val();
		var total = parseFloat(t);
		var quizid = $("#quizid").val();
		$("input[name=score]").each(function(){
			var s = $(this).val();
			var studid = $(this).attr('id');
			var score = parseFloat(s);
			if (score > total) {
				alert("Score "+score+" is above total score. Please Edit");
			}else{
				$.ajax({
					headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
					method:'POST',
					url:'savescore',
					data:{score:score,studentid:studid},
					success:function(data){

					}
				});
				

			}
		});
		// $('#quiz-details-table').DataTable().ajax.reload();
		location.reload();
	});

});