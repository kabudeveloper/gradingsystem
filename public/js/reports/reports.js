$(document).ready(function(){
	// alert(1);
	var course = 0;
	var subject = 0;
	var sem = 0;
	var year = 0;
	var section = 0;
	$('#report-form').submit(function(e){
		e.preventDefault();
		course = $('#course').val();
		subject = $('#subject').val();
		sem = $('#semester').val();
		year = $('#year').val();
		section = $('#section').val();
		$('.report-button').html('<button class="btn btn-success btn-round" id="generateButton">Generate</button>')
		var reports = $('#report-table').DataTable({
			destroy: true,
			processing: true,
			serverSide: true,
			"order":[[0,'dec']],
			ajax: 'filterreport/'+course+'/'+subject+'/'+year+'/'+section+'/'+sem+'/get',
			columns: [
				{data:'name',name:'name'},
				{data: 'course', name:'course'},
				{data: 'subject', name:'subject'},
				{data: 'year', name:'year'},
		        {data: 'section', name: 'section'},
		        {data: '_semester', name: '_semester'},

			],
			
		});


	});
	$(document).on('click','#generateButton',function(e){
		
		var query = {
			course:course,
			subject:subject,
			year:year,
			section:section,
			sem:sem
	    }
    	var url = "generatenow?" + $.param(query);
   		window.location = url;
	});
	$('#course').change(function(){
		var courseid = $(this).val();
		if (courseid == 0) {
			$.ajax({
				method:'GET',
				url: 'getallcoursedetailsreport',
				success:function(data){
					var subject = [];
					var section = [];
					for (var i = 0; i < data[0].length; i++) {
						subject[i] = '<option value="'+data[0][i].subjectid+'">'+data[0][i].subjectname+'</option>';
					}
					for (var i = 0; i < data[1].length; i++) {
						section[i] = '<option value="'+data[1][i].sectionid+'">'+data[1][i].sectioncode+'</option>';
					}

					$('#subject').html('<option value="0">All</option>'+subject);
					$('#section').html('<option value="0">All</option>'+section);
				}
			});
		}else{
			$.ajax({
				method:'GET',
				url: 'getcoursedetailsreport/'+courseid,
				success:function(data){
					var subject = [];
					var section = [];
					for (var i = 0; i < data[0].length; i++) {
						subject[i] = '<option value="'+data[0][i].subjectid+'">'+data[0][i].subjectname+'</option>';
					}
					for (var i = 0; i < data[1].length; i++) {
						section[i] = '<option value="'+data[1][i].sectionid+'">'+data[1][i].sectioncode+'</option>';
					}

					$('#subject').html('<option value="0">All</option>'+subject);
					$('#section').html('<option value="0">All</option>'+section);
				}
			});
		}
	});
});