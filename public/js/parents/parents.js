$(document).ready(function(){
	var parents = $('#parents-table').DataTable({
		processing: true,
		serverSide: true,
		ajax: 'parents/get',
		"order":[[0,'asc']],
		columns:[
			{data:'last_name',name:'last_name'},
			{data:'first_name',name:'first_name'},
			{data:'middle_name',name:'middle_name'},
			{data:'suffix',name:'suffix'},
		],

	});

});