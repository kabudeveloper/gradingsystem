<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Route::POST('/registerstudent','RegisterController@registerStudents');
Route::get('relogout','VerificationController@relogout');
Route::group([ 'middleware' => ['auth'] ], function() {
	Route::get('/verify','CoursesController@verify');
	Route::get('/verified','VerificationController@verified');
	Route::group([ 'middleware' => ['checklogin'] ], function() {
		
		Route::get('/home', 'DashboardController@index')->name('home');
		Route::get('adminuserprofile','UsersController@adminProfile');
		Route::POST('updateadminprofile','UsersController@updateAdminProfile');

		//students
		Route::get('students/get','StudentsController@getStudents');
		Route::get('students/{id}/edit/details','StudentsController@getStudentsDetails');
		Route::get('students/{id}/details/create','StudentsController@createDetails');
		Route::POST('students/details/{id}','StudentsController@storeDetails');
		Route::get('students/{stud_id}/details/{id}/view','StudentsController@showDetails');
		Route::get('students/{studid}/details/{id}/edit','StudentsController@editDetails');
		Route::POST('students/details/{stud_id}/{id}/update','StudentsController@updateDetails');
		Route::get('students/{studid}/details/{id}/get','StudentsController@getStudentsDetails');
		Route::DELETE('students/{studid}/details/{id}/delete','StudentsController@destroyDetails');
		Route::get('students/{id}/account/create','StudentsController@createAccount');
		Route::POST('students/{id}/account/store','StudentsController@storeAccount');
		Route::get('students/{id}/account/{accountid}/edit','StudentsController@editAccount');
		Route::POST('students/{id}/account/{accountid}/update','StudentsController@updateAccount');
		Route::DELETE('students/{id}/account/{accountid}/destroy','StudentsController@destroyAccount');
		//endstudents

		//courses details
		Route::get('courses/{courseid}/details/create','CoursesController@createCourseDetails');
		Route::POST('courses/{courseid}/details/store','CoursesController@storeCourseDetails');
		Route::get('courses/{courseid}/details/{id}/edit','CoursesController@editCourseDetails');
		Route::POST('courses/{courseid}/details/{id}/update','CoursesController@updateCourseDetails');
		Route::DELETE('courses/{courseid}/details/{id}/delete','CoursesController@destroyDetails');
		//end courses datails

		//adding subjects,class and account to teachers
		Route::PUT('teachers/{id}/details/{d_id}/assigned','TeachersController@assigned');
		Route::PUT('teachers/{id}/details/{d_id}/cancel','TeachersController@cancel');
		Route::get('teachers/{id}/account/create','TeachersController@createAccount');
		Route::POST('teachers/{id}/account/store','TeachersController@storeAccount');
		Route::get('teachers/{id}/account/{accountid}/edit','TeachersController@editAccount');
		Route::POST('teachers/{id}/account/{accountid}/update','TeachersController@updateAccount');
		Route::DELETE('teachers/{id}/account/{accountid}/destroy','TeachersController@destroyAccount');
		//end adding subjects,class and account to teachers

		//T E A C H E R  A C C E S S 
		Route::get('teacheruserprofile','UsersController@teachersProfile');
		Route::get('subject/{courseid}/getSubjects','DashboardController@getSubjects');
		Route::get('year/{courseid}/{subjectid}/getYear','DashboardController@getYear');
		Route::get('section/{courseid}/{subjectid}/{yearid}/getSection','DashboardController@getSection');

		Route::get('attendance/{studentid}/{semid}/{term}/{sc_id}/in','AttendanceController@timeIn');
		Route::get('attendance/{studentid}/{semid}/{term}/{sc_id}/out','AttendanceController@timeOut');
		Route::get('attendance/{studentid}/{semid}/{term}/{sc_id}/absent','AttendanceController@absent');

		Route::get('semestergrades','GradeDetailsController@semestergrade');
		Route::POST('semestergrades/storesemgrades','GradeDetailsController@storesemgrades');

		Route::get('teachersubjects','SubjectsController@teachersubjects');
		

		//ADD QUIZES
		Route::get('quizes/subject/{courseid}/getSubjects','QuizesController@getSubjects');
		Route::get('quizes/year/{courseid}/{subjectid}/getYear','QuizesController@getYear');
		Route::get('quizes/section/{courseid}/{subjectid}/{yearid}/getSection','QuizesController@getSection');
		Route::POST('quizes/{id}/savescore','QuizesController@scoreUpload');
		Route::POST('assignment/{id}/savescore','AssignmentController@scoreUpload1');
		Route::POST('termexam/{id}/savescore','TermExamController@scoreUpload');
		Route::POST('attendancegrade/{id}/savescore','AttendanceController@scoreUpload');
		Route::DELETE('attendancegrade/{id}/delete','AttendanceController@destroy');
		//END ADD QUIZES

		// A T T E N D A N C E   M O D U L E
		Route::get('attendancegrade','AttendanceController@index');
		Route::get('attendancegrade/create','AttendanceController@create');
		Route::POST('attendancegrade/store','AttendanceController@store');
		Route::get('attendancegrade/{id}/edit','AttendanceController@edit');
		Route::get('attendancegrade/{id}/studentattendace/get','AttendanceController@getStudentsAttendance');
		Route::PATCH('attendancegrade/{id}/update','AttendanceController@update');

		// on change on course , yeari
		Route::get('assignment/subject/{courseid}/getSubjects','AssignmentController@getSubjects');
		Route::get('assignment/year/{courseid}/{subjectid}/getYear','AssignmentController@getYear');

		Route::get('attendancegrade/section/{courseid}/{subjectid}/{yearid}/getSection','AttendanceController@getSection');

		Route::get('attendancegrade/subject/{courseid}/getSubjects','AttendanceController@getSubjects');
		Route::get('attendancegrade/year/{courseid}/{subjectid}/getYear','AttendanceController@getYear');

		Route::get('assignment/section/{courseid}/{subjectid}/{yearid}/getSection','AssignmentController@getSection');

		Route::get('termexam/subject/{courseid}/getSubjects','TermExamController@getSubjects');
		Route::get('termexam/year/{courseid}/{subjectid}/getYear','TermExamController@getYear');
		Route::get('termexam/section/{courseid}/{subjectid}/{yearid}/getSection','TermExamController@getSection');
		Route::PATCH('termexam/{id}/update','TermExamController@update');

		Route::POST('gradedetails/storegrades','GradeDetailsController@gstore');
		Route::get('gradedetails/student/{studentid}/{subjectgradeid}/view','GradeDetailsController@viewStudentsGrade');
		//end teacher login

		//S T U D E N T   A C C E S S 
		Route::get('studentgrade/get','GradeDetailsController@individualStudentGrade');
		Route::get('studentgrade/getterm','GradeDetailsController@individualStudentGradeTerm');
		Route::get('coursedetails/getcoursesubjects','CourseDetailsController@getCourseSubjects');
		Route::get('studentuserprofile','UsersController@studentsProfile');
		Route::get('studentsgrade/{gradeid}/view','GradeDetailsController@studentGradeBreakDown');
		Route::get('studentsgrade/{gradeid}/getattendance','GradeDetailsController@getstudentAttendanceGradeBreakDown');
		Route::get('studentsgrade/{gradeid}/getterm','GradeDetailsController@getstudentTermGradeBreakDown');
		Route::get('studentsgrade/{gradeid}/getquiz','GradeDetailsController@getStudentQuizGradeBreakDown');
		Route::get('studentsgrade/{gradeid}/getass','GradeDetailsController@getStudentAssGradeBreakDown');

		// E N D   S T U D E N T   A C C E S S 

		//P A R E N T   A C C E S S 
		

		// E N D   P A R E N T   A C C E S S 

		// R E P O R T S 
		Route::get('getallcoursedetailsreport','ReportsController@getall');
		Route::get('getcoursedetailsreport/{courseid}','ReportsController@getwhere');
		Route::get('filterreport/{course}/{subject}/{year}/{section}/{sem}/get','ReportsController@filterreport');
		Route::get('generatenow','ReportsController@generatenow');
		// E N D  R E P O R T S

		//datatables
		Route::get('parents/get','ParentsController@getParents');
		Route::get('courses/get','CoursesController@getCourses');
		Route::get('courses/{courseid}/details/get','CoursesController@getCoursesDetails');
		Route::get('sections/get','SectionsController@getSections');
		Route::get('subjects/get','SubjectsController@getSubjects');
		Route::get('students/{id}/account/get','StudentsController@getAccount');
		Route::get('users/get','UsersController@getUsers');
		Route::get('teachers/get','TeachersController@getTeachers');
		Route::get('teachers/{id}/details/getassigned','TeachersController@getassigned');
		Route::get('teachers/{id}/details/getnotassigned','TeachersController@getnotassigned');
		Route::get('teachers/{id}/account/getteacheraccount','TeachersController@getteacheraccount');
		Route::get('classes/{courseid}/{subjectid}/{yearid}/{sectionid}/{semesterid}/{sc_year}/get','DashboardController@getClasses');
		Route::get('studentquiz/{courseid}/{subjectid}/{yearid}/{sectionid}/{semesterid}/{sc_year}/get','QuizesController@getQuizes');
		Route::get('studentassignment/{courseid}/{subjectid}/{yearid}/{sectionid}/{semesterid}/{scyear}/get','AssignmentController@getAss');
		Route::get('studentattendance/{courseid}/{subjectid}/{yearid}/{sectionid}/{semesterid}/{scyear}/get','AttendanceController@getAttendance');
		Route::get('studentterm/{courseid}/{subjectid}/{yearid}/{sectionid}/{semesterid}/{sc_year}/get','TermExamController@getTerm');
		Route::get('quizes/{detailsid}/studentquiz/get','QuizesController@getStudentQuiz');

		Route::get('studentass/{courseid}/{subjectid}/{yearid}/{sectionid}/{semesterid}/get','AssignmentController@getAss');
		Route::get('assignment/{detailsid}/studentass/get','AssignmentController@getStudentAss');
		Route::get('termexam/{detailsid}/studentterm/get','TermExamController@getStudentTerm');
		Route::get('gradedetails/{courseid}/{subjectid}/{yearid}/{sectionid}/{semesterid}/{termid}/{sc_year}/get','GradeDetailsController@getStudentSubjectGrade');
		Route::get('gradedetails/{courseid}/{subjectid}/{yearid}/{sectionid}/{semesterid}/{sc_year}/getsem','GradeDetailsController@getStudentSemGrade');
		Route::get('gradedetails/student/{studentid}/{subjectgradeid}/getTerm','GradeDetailsController@getStudentsTermScore');
		Route::get('gradedetails/student/{studentid}/{subjectgradeid}/getQuiz','GradeDetailsController@getStudentsQuizScore');
		Route::get('gradedetails/student/{studentid}/{subjectgradeid}/getAss','GradeDetailsController@getStudentsAssScore');
		Route::get('gradedetails/student/{studentid}/{subjectgradeid}/getAttendance','GradeDetailsController@getStudentsAttendanceScore');
		Route::get('teachersubjects/get','SubjectsController@getteachersubjects');
		Route::get('schoolyear/get','SchoolYearController@getschoolyear');
		//end datatables

		// Resource route
		Route::resource('students','StudentsController');
		Route::resource('courses','CoursesController');
		Route::resource('sections','SectionsController');
		Route::resource('parents','ParentsController');
		Route::resource('subjects','SubjectsController');
		Route::resource('users','UsersController');
		Route::resource('teachers','TeachersController');
		Route::resource('attendance','AttendanceController');
		Route::resource('quizes','QuizesController');
		Route::resource('assignment','AssignmentController');
		Route::resource('termexam','TermExamController');
		Route::resource('gradedetails','GradeDetailsController');
		Route::resource('coursedetails','CourseDetailsController');
		Route::resource('schoolyear','SchoolYearController');
		Route::resource('reports','ReportsController');
		// End Resource route
	});
	
	
});



